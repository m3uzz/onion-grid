/**
 * @category   Javascript
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */

$(function() {
	var checkboxStatus = true;
	var btnConfirmOrigin = '';

	$('[data-toggle="popover"]').popover();
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-bs-toggle="popover"]').popover();
	$('[data-bs-toggle="tooltip"]').tooltip();
	
	$(".onion-grid-search-query").off("keypress").on("keypress", function(e) {
		if (e.which == '13') {
			gridForm = $(this).closest('form');
			gridForm.children("input[name=p]").val('');
			gridForm.children("input[name=btn]").val('search');
			gridForm.attr("action", $(this).data("act"));
			return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));
		}
	});	
	
	$(".onion-grid-search-btn").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.children("input[name=p]").val('');
		gridForm.children("input[name=btn]").val('search');
		gridForm.attr("action", $(this).data("act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
	});

	$(".onion-grid-act-btn").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.children("input[name=id]").val($(this).data("value"));
		
		params = ''; 
		
		if ($(this).data("params") != '' && $(this).data("params") != undefined)
		{
			params = '?' + $(this).data("params");
		}
		
		btnRequestType = $(this).data("request");
		
		if (btnRequestType != undefined)
		{
			gridForm.data("request", btnRequestType);	
		}
		
		gridForm.attr("action", $(this).data("act") + params);
		
		gridId = gridForm.data("grid-id");

		if ($(this).data("confirm") == "true" || $(this).data("confirm") == true) {
			btnConfirmOrigin = $(this).attr('id');
			$('#onion-grid-modal-confirmation-' + gridId + ' div.modal-body').html('<p>' + $(this).data("msg") + '</p>');
			$('#onion-grid-modal-confirmation-' + gridId).modal('show');
		}
		else {
			if ($(this).data("ajax")) {
				return callAction($(this));
			}

			return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));
		}
	});
	
	$(".onion-grid-filter-submit").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.children("input[name=p]").val('');
		gridForm.children("input[name=btn]").val('filter');
		gridForm.attr("action", $(this).data("act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
	});
	
	$(".onion-grid-select-num-rows").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.children("input[name=p]").val('0');
		gridForm.children("input[name=rows]").val($(this).data("rows"));
		gridForm.children("input[name=btn]").val('rows');
		gridForm.attr("action", $(this).data("act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
	});	

	$(".onion-grid-col-order").on('click', function() {
		gridForm = $(this).closest('form');
		colOrder = gridForm.children("input[name='ord']").val();
		colOrder = JSON.parse(colOrder);

		if ($(this).data("ord") != '')
		{
			colOrder[$(this).data("col")] = $(this).data("ord");
		}
		else
		{
			delete colOrder[$(this).data("col")];
			delete colOrder[$(this).data("col-ord")];
		}

		colOrder = JSON.stringify(colOrder)
		gridForm.children("input[name='ord']").val(colOrder);
		gridForm.children("input[name=btn]").val('order');
		gridForm.attr("action", $(this).data("act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
	});
	
	$(".onion-grid-pagination li a, .pagination li a").on('click', function() {
		gridForm = $(this).closest('form');
		$(this).attr("href", "#");
		gridForm.children("input[name=p]").val($(this).data("page"));
		gridForm.children("input[name=btn]").val('page');
		gridForm.attr("action", $(this).data("act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
	});

	$(".onion-grid-mass-act-btn").on('click', function() {
		gridForm = $(this).closest('form');
		rowChecked = new Array();

		$("input[name='ck[]']:checked").each(function(){
			rowChecked.push($(this).val());
		});
	
		$("input[type=hidden][name='ckd']").val(rowChecked);

		btnRequestType = $(this).data("request");
		
		if (btnRequestType != undefined)
		{
			gridForm.data("request", btnRequestType);	
		}
		
		gridForm.attr("action", $(this).data("act"));
		gridId = gridForm.data("grid-id");
		
		if(rowChecked.length > 0) {
			if ($(this).data( "confirm" ) == "true" || $(this).data( "confirm" ) == true)
			{
				$('#onion-grid-modal-confirmation-' + gridId + ' div.modal-body').html('<p>' + $(this).data("msg") + '</p>');
				$('#onion-grid-modal-confirmation-' + gridId).modal('show');
			}
			else
			{
				return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
			}
		}
		else {
			if ($(this).data("no-check") == "true" || $(this).data("no-check") == true)
			{
				if ($(this).data( "confirm" ) == "true" || $(this).data( "confirm" ) == true)
				{
					$('#onion-grid-modal-confirmation-' + gridId + ' div.modal-body').html('<p>' + $(this).data("msg") + '</p>');
					$('#onion-grid-modal-confirmation-' + gridId).modal('show');
				}
				else
				{
					return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area"));	
				}
			}
			else
			{
				msg = $(this).data('check-msg');
				
				if (msg == undefined || msg == '')
				{
					msg = "É necessário marcar ao menos um item para executar a ação!";
				}
				
				$('#onion-grid-alert-' + gridId + ' div.modal-body').html(msg);					
				$('#onion-grid-alert-' + gridId).modal('show');
			}
		}
		
		return false;
	});
	
	$(".onion-grid-mass-act-popup-btn").off('click').on('click', function() {
		gridForm = $(this).closest('form');
		gridId = gridForm.data("grid-id");
		rowChecked = new Array();
		openPopUp = false;			
		
		$("input[name='ck[]']:checked").each(function(){
			rowChecked.push($(this).val());
		});
	
		$("input[type=hidden][name='ckd']").val(rowChecked);

		if(rowChecked.length > 0) {
			if ($(this).hasClass( "noConfirmation" ))
			{
				openPopUp = true;	
			}
			else
			{
				$('#onion-grid-modal-confirmation-' + gridId + ' div.modal-body').html('<p>' + $( this ).data("msg") + '</p>');
				$('#onion-grid-modal-confirmation-' + gridId).modal('show');
			}
		}
		else {
			if ($(this).hasClass( "no-check" ))
			{
				openPopUp = true;
			}
			else
			{
				message = $(this).data('msg');
				
				if (message != '')
				{
					$('#onion-grid-alert-' + gridId + ' div.modal-body').html(message);
				}
				
				$('#onion-grid-alert-' + gridId).modal('show');
			}
		}
		
		if (openPopUp)
		{
			$(this).popUpWindow(
					$(this).data('url'),
					$(this).data('params') + "&ckd=" + rowChecked,
					$(this).data('wname'),
					$(this).data('wwidth'),
					$(this).data('wheight'),
					$(this).data('wtop'),
					$(this).data('wleft'),
					$(this).data('wscroll'),
					$(this).data('wwindow'),
					$(this).data('reload')
				);			
		}
		
		return false;
	});

	$(".onion-grid-modal-confirm-btn").on('click', function() {
		if (btnConfirmOrigin != '') {
			var btnOrigin = $("#" + btnConfirmOrigin);

			if (btnOrigin != undefined && btnOrigin.data("ajax")) {
				return callAction(btnOrigin);
			}
		}

		$(this).showLoader();
		gridForm = $(this).closest('form');
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.data("content-area")); 
	});
	

	$(".onion-grid-modal-btn").off('click').on('click', function() {
		gridForm = $(this).closest('form');
		gridId = gridForm.attr("data-grid-id");
		modal = $('#onion-grid-modal-' + gridId);
		modal.find(".modal-title").html($(this).attr("data-title"));
		okBtn = modal.find(".onion-grid-modal-ok-btn");
		okBtn.html($(this).attr('data-btnName'));
		okBtn.attr('data-origen', $(this).attr("id"));
		okBtn.attr('data-act', $(this).attr('data-act'));
		okBtn.attr('data-form', $(this).attr("data-form"));
		okBtn.attr('data-method', $(this).attr("data-method"));
		okBtn.attr('data-request', $(this).attr('data-request'));
		okBtn.attr('data-no-check', $(this).attr("data-no-check"));
		okBtn.attr('data-check-msg', $(this).attr("data-check-msg"));
		okBtn.attr('data-container', $(this).attr("data-container"));
		okBtn.attr('data-fnSubmit', $(this).attr("data-fnSubmit"));
		okBtn.attr('data-fnDone', $(this).attr("data-fnDone"));

		if ($(this).attr('data-btnDismiss') != 'false')
		{
			okBtn.attr('data-bs-dismiss', 'modal');	
		}

		if ($(this).attr('data-cancelBtn') == 'no')
		{
			modal.find(".onion-grid-modal-cancel-btn").hide();
		}

		if ($(this).attr('data-header') == 'no')
		{
			modal.find(".modal-header").hide();
		}

		if ($(this).attr('data-footer') == 'no')
		{
			modal.find(".modal-footer").hide();
		}

		modalDimension = $(this).attr("data-modal-dimension");
	
		if (modalDimension != undefined && modalDimension != "")
		{
			dialog = modal.find('.modal-dialog');
			dialog.removeClass(dialog.data('original-dimension'));
			dialog.addClass(modalDimension);
			dialog.attr('data-dimension', modalDimension);
		}

		act = $(this).attr("data-act");
		console.debug(act);

		//TODO: inserir confirmação

		if (act != undefined)
		{			
			body = modal.find(".modal-body");
			body.html($("#loading").html());

			if ($(this).attr("data-params") != undefined && $(this).attr("data-params") != '')
			{
				act = act + '?' + $(this).attr("data-params");
			}
		
			requestMethod = $(this).attr('data-method');			
			console.debug(requestMethod);
			
			params = {'w':'modal'};
		
			$.ajaxSetup({async:true});
			
			if (requestMethod == 'post')
			{
				$.post(act, params, function( data ) {
					body.html(data);
					}, "html" 
				);			
			}
			else
			{
				$.get(act, params, function( data ) {
					body.html(data);
					}, "html" 
				);
			}
		}

		modal.modal('show');
	});
	
	$(".onion-grid-modal-ok-btn").off('click').on('click', function() {
		var fnSubmit = $(this).attr('data-fnSubmit');
		console.debug(fnSubmit);

		if (fnSubmit != undefined && fnSubmit != '')
		{
			try{
				var userFunc = {'fnSubmit': eval(fnSubmit)};
				userFunc['fnSubmit']($(this));
			}catch (e){
				console.debug(e);
			}
		}

		formId = $(this).attr('data-form');

		if (formId != undefined && formId != "")
		{
			gridForm = $(formId);
		}
		else
		{
			gridForm = $(this).closest('form');
		}

		btnRequestType = $(this).attr("data-request");
		
		if (btnRequestType != undefined)
		{
			gridForm.attr("data-request", btnRequestType);	
		}

		btnMethod = $(this).attr("data-method");
		
		if (btnMethod != undefined)
		{
			gridForm.attr("method", btnMethod);	
		}
		
		btnAct = $(this).attr("data-act");

		if (btnAct != undefined)
		{
			gridForm.attr("action", btnAct);	
		}

		gridId = gridForm.attr("data-grid-id");

		submit = true;

		if ($(this).attr('data-bs-dismiss') == 'modal' && $(this).attr("data-no-check") == undefined)
		{
			submit = false;
		}

		if ($(this).attr("data-no-check") != "true" && $(this).attr("data-no-check") != true && submit)
		{
			rowChecked = new Array();
	
			$("input[name='ck[]']:checked").each(function(){
				rowChecked.push($(this).val());
			});
		
			$("input[type=hidden][name='ckd']").val(rowChecked);
			
			if (rowChecked.length < 1)
			{
				msg = $(this).attr('data-check-msg');
				
				if (msg == undefined || msg == '')
				{
					msg = "É necessário marcar ao menos um item para executar a ação!";
				}
					
				$('#onion-grid-alert-' + gridId + ' div.modal-body').html(msg);					
				$('#onion-grid-alert-' + gridId).modal('show');

				submit = false;
			}
		}

		if (submit)
		{
			container = $(this).attr("data-container");
			response = gridForm.actFormSubmit("#"+gridForm.attr('id'), container);	
		}

		var fnDone = $(this).attr('data-fnDone');
		console.debug(fnDone);
		
		if (fnDone != undefined && fnDone != '')
		{
			try{
				var userFunc = {'fnDone': eval(fnDone)};
				userFunc['fnDone']($(this));
			}catch (e){
				console.debug(e);
			}
		}
	});

	$('.onion-grid-modal').off('hidden.bs.modal').on('hidden.bs.modal', function (){
		modal = $(this);
		modal.find('.modal-body').html("");
		dialog = modal.find('.modal-dialog');
		dialog.removeClass(dialog.data('dimension'));
		dialog.addClass(dialog.data('original-dimension'));
		dialog.removeAttr('data-dimension');
		btnOk = modal.find(".onion-grid-modal-ok-btn");
		btnOk.html(btnOk.data('btn-confirm-name'));
		btnOk.removeAttr('data-origen');
		btnOk.removeAttr('data-form');
		btnOk.removeAttr('data-act');
		btnOk.removeAttr('data-method');
		btnOk.removeAttr('data-request');		
		btnOk.removeAttr('data-no-check');		
		btnOk.removeAttr('data-check-msg');		
		btnOk.removeAttr('data-container');		
		btnOk.removeAttr('data-dismiss');		
		btnOk.removeAttr('data-fnSubmit');	
		btnOk.removeAttr('data-fnDone');
		modal.find(".onion-grid-modal-cancel-btn").show();
		modal.find(".modal-header").show();
		modal.find(".modal-footer").show();
	})
	
	$(".onion-grid-reload-btn").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.children("input[name=p]").attr('disabled', 'disabled');
		gridForm.find("input[name='vc[]']").each(function(){
			$(this).attr('disabled', 'disabled');
		});
		gridForm.find("input[name='f[]']").each(function(){
			$(this).attr('disabled', 'disabled');
		});
		gridForm.find("input[name=q]").attr('disabled', 'disabled');
		gridForm.children("input[name=rows]").attr('disabled', 'disabled');
		gridForm.children("input[name=ord]").attr('disabled', 'disabled');
		gridForm.children("input[name=btn]").val('reload');
		gridForm.attr("action", $(this).attr("data-act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.attr("data-content-area"));	
	});
	
	$(".onion-grid-change-row-check-all").on('click', function() {
		checkboxChange = false;
		
		$("input[name='ck[]']").each(function(){
			$(this).prop('checked', checkboxStatus);
			checkboxChange = true;
		});
		
		title = $(this).attr('title');
		inv = $(this).attr('data-inv');
		
		if (checkboxChange) {
			$(this).attr('title', inv);
			$(this).attr('data-inv', title);
			$(this).children("i").html(Boolean(checkboxStatus) ? "check_box" : "check_box_outline_blank");
			checkboxStatus = !checkboxStatus;
		}
	});
	
	$(".onion-grid-tr").on('click', function() {
		ck = $(this).find("input[name='ck[]']");
		ck.prop('checked', !ck.prop('checked'));
	});	
	
	$("input[name='ck[]']").on('click', function() {
		$(this).prop('checked', !$(this).prop('checked'));
	});	
	
	$(".onion-grid-table-responsive .onion-grid-td-actions .dropdown-toggle").off('click').on('click', function() {
		$(".table-responsive").css('overflow-x', 'visible');
	});
	
	var stGridFieldValue = null;
	
	$(".onion-grid-value-area").off("dblclick").on("dblclick", function(e) {
		editArea = $(this).parent().find(".onion-grid-edit-area");
		ig = editArea.find(".input-group");
		temp = ig.html();
		ig.html(temp.replace('<p ', '<input '));
		temp = ig.html();
		ig.html(temp.replace('<s ', '<select '));
		temp = ig.html();
		ig.html(temp.replace('</s>', '</select>'));
		
		$(this).hide();
		editArea.show();

		$(".onion-grid-edit-field").off("focus").on("focus", function(e) {
			stGridFieldValue = $(this).val();
		});
		
		$(".onion-grid-edit-field").off("keypress").on("keypress", function(e) {
			if (e.which == '13') {
				if (stGridFieldValue != $(this).val())
				{
					saveGridField($(this), gridEditSaved);
				}
				else
				{
					gridEditSaved(null, $(this));
				}
			}
		});
		
		$(".onion-grid-edit-field").off("keyup").on("keyup", function(e) {
			if (e.keyCode == '27') {
				$(this).val(stGridFieldValue);
				gridEditSaved(null, $(this));
			}
		});
		
		$(".onion-grid-edit-field").off("blur").on("blur", function(e) {
			$(this).val(stGridFieldValue);
			gridEditSaved(null, $(this));
		});

		editArea.find('input,select').focus();
	});

	function gridEditSaved (data, e)
	{
		valueArea = e.parent().parent().parent().find(".onion-grid-value-area");
		valueArea.html(e.val());
		e.attr('value', e.val());
		valueArea.show();
		ig = e.parent();
		ig.parent().hide();
		temp = ig.html();
		ig.html(temp.replace('<input ', '<p '));
		temp = ig.html();
		ig.html(temp.replace('<select ', '<s '));
		temp = ig.html();
		ig.html(temp.replace('</select>', '</s>'));

		var fnFormat = e.attr('data-fnFormat');
		var func = {'done':eval(fnFormat)};

		if (fnFormat != undefined)
		{
			func['done'](e.val(), valueArea);
		}
	}

	function saveGridField (e, fnDone, fnFail){
		var func = {'done':eval(fnDone), 'fail':eval(fnFail)};
		var url = e.attr("data-url");
		var id = e.attr("data-id");
		var field = e.attr("name");
		var value = e.val();
		var param = "id=" + id + "&field=" + field + "&value=" + value;
		
		console.debug(url + param);
		
		$.ajaxSetup({async:true});
		
		$.get(
			url,
			param,
			function( data ) {
				stGridFieldValue = value;
			},
			"html"
		).done (function( data ) {
			if (fnDone != undefined)
			{
				func['done'](data, e);
			}
		}).fail(function( data ) {
			if (fnFail != undefined)
			{
				func['fail'](data, e);
			}
		});
			
		return false;
	}

	function callAction (btn) {
		$.ajaxSetup({async:true});
		$(this).showLoader();

		$.get(
			btn.data('act'),
		).done (function( response ) {
			$(this).stopLoader();
			console.log(response);
			btn.trigger('getDone', [response]);
		}).fail(function( response ) {
			$(this).stopLoader();
			console.log(response);
			btn.trigger('getFail', [response]);
		});

		return false;
	}

	$("a[data-request=ajax").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.attr("action", $(this).attr("href"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.attr("data-content-area"));
	});
	
	$(".onion-grid-apply-fields-to-view").on('click', function() {
		gridForm = $(this).closest('form');
		gridForm.attr("action", $(this).attr("data-act"));
		return gridForm.actFormSubmit("#"+gridForm.attr('id'), gridForm.attr("data-content-area"));
	});
	
	$(".onion-grid-field-view").on('click', function() {
		$(this).prop('checked', $(this).prop('checked'));
		$(".onion-grid-select-field-to-view").off('click');
	});	
	
	$(".onion-grid-select-field-to-view").off('mouseenter').on('mouseenter', function() {
		$(".onion-grid-select-field-to-view").off('click').on('click', function() {
			ck = $(this).find("input");
			ck.prop('checked', !ck.prop('checked'));
			return false;
		});
	});
	
	$(".onion-grid-search-field").on('click', function() {
		$(this).prop('checked', $(this).prop('checked'));
		$(".onion-grid-fields-to-search").off('click');
	});	
	
	$(".onion-grid-fields-to-search").off('mouseenter').on('mouseenter', function() {
		$(".onion-grid-fields-to-search").off('click').on('click', function() {
			ck = $(this).find("input");
			ck.prop('checked', !ck.prop('checked'));
			return false;
		});
	});

	$(".onion-grid-form-filter-area").off().on("keydown", event => {
		if (event.key === "Enter") {
			event.stopPropagation();
		  	$(".onion-grid-filter-submit").trigger('click');
		}
	});

	$('.onion-grid-act-btn, .onion-grid-link-btn, .onion-grid-col-order, .page-link').on('click', function () {	
		if ($(this).data('confirm') == true) return;
		if ($(this).data('loader') == false) return;
		if ($(this).attr('target') == '_blank') return;
		$(this).showLoader();
	});

	$('.modal .onion-grid-act-btn, .modal .onion-grid-link-btn, .modal .onion-grid-col-order, .modal .page-link').on('click', function () {	
		$(this).stopLoader();
	});
});