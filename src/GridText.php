<?php 
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;


class GridText
{
	
	/**
	 *
	 * @var string
	 */
	protected static $sMarkAll = 'Marcar todos';
	
	/**
	 *
	 * @var string
	 */
	protected static $sUnmarkAll = 'Desmarcar todos';
	
	/**
	 *
	 * @var string
	 */
	protected static $sClickToEdit = 'Clique duas vezes para editar';
	
	/**
	 *
	 * @var string
	 */
	protected static $sEnterToSave = 'Tecle ENTER para salvar. Tecle ESC ou clique fora para cancelar';
	
	/**
	 *
	 * @var string
	 */
	protected static $sReload = 'Limpar filtros';
	
	/**
	 *
	 * @var string
	 */
	protected static $sExport = 'Exportar para';
	
	/**
	 *
	 * @var string
	 */
	protected static $sExportCSV = 'Exportar para CSV';
	
	/**
	 *
	 * @var string
	 */
	protected static $sExportPDF = 'Exportar para PDF';
	
	/**
	 *
	 * @var string
	 */
	protected static $sExportXLS = 'Exportar para XLS';
	
	/**
	 *
	 * @var string
	 */
	protected static $sPrint = 'Imprimir tela';
	
	/**
	 *
	 * @var string
	 */
	protected static $sOpenFilterForm = 'Abrir formulário de filtros de busca';
	
	/**
	 *
	 * @var string
	 */
	protected static $sSearchForColum = 'Buscar por coluna';
	
	/**
	 *
	 * @var string
	 */
	protected static $sShowColums = 'Exibir colunas';
	
	/**
	 *
	 * @var string
	 */
	protected static $sApply = 'Aplicar';
	
	/**
	 *
	 * @var string
	 */
	protected static $sNumberOfRecords = 'Quantidade de registros por página';
	
	/**
	 *
	 * @var string
	 */
	protected static $sShow = 'Exibir: ';
	
	/**
	 *
	 * @var string
	 */
	protected static $sSearchFor = 'Buscar por';
	
	/**
	 *
	 * @var string
	 */
	protected static $sNotFound = 'Nenhum registro encontrado!';
	
	/**
	 *
	 * @var string
	 */
	protected static $sPagination = 'Exibindo de %d à %d de %d';
	
	/**
	 *
	 * @var string
	 */
	protected static $sPaginationShort = '%d à %d de %d';
	
	/**
	 *
	 * @var string
	 */
	protected static $sAlert = 'Alerta';
	
	/**
	 *
	 * @var string
	 */
	protected static $sClose = 'Fechar';
	
	/**
	 *
	 * @var string
	 */
	protected static $sConfirmation = 'Confirmação';
	
	/**
	 *
	 * @var string
	 */
	protected static $sCancel = 'Cancelar';
	
	/**
	 *
	 * @var string
	 */
	protected static $sConfirm = 'Confirmar';
	
	/**
	 *
	 * @var string
	 */
	protected static $sMarkLessOne = 'Você deve marcar pelo menos uma linha de registro!';
	
	
	/**
	 * 
	 * @param string $psText
	 * @return string
	 */
	public static function text (string $psText) : string
	{
		return self::$$psText;
		//return Translator::i18n(self::$$psText);
	}
}
