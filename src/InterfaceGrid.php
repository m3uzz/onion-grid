<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;


interface InterfaceGrid
{
	/**
	 * Check if a property exist into the object
	 *
	 * @param string $psVar
	 * @return bool
	 */
	public function isVar (string $psVar) : bool;
	
	
	/**
	 * Set the object properties.
	 *
	 * The method "set" can be used as:
	 * An array with the exists properties and its values;
	 * ```
	 * set(array('property_name'=>'value'));
	 * ```
	 * or 
	 * ```
	 * set('property_name', 'value');
	 * ```
	 * setPropertyName('value') - An specific method, where the exist
	 * property is the name postfix, using camelCase style.
	 *
	 * If the property doesn't exist, the return will be a exception
	 *
	 * @param string|array $pmVar
	 * @param mixed $pmValue
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function set ($pmVar, $pmValue = null) : InterfaceGrid;
	
	
	/**
	 * Define if the application is enable to run
	 *
	 * @param bool $pbEnable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setEnable (bool $pbEnable) : InterfaceGrid;
	
	
	/**
	 *
	 * @param object|null $poParent
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setParent (?object $poParent = null) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string $psResource
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setResource (string $psResource) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string $psId
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setId (string $psId) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string|null $psName
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setName (?string $psName = null) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string|null $psTitle
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setTitle (?string $psTitle = null) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string|null $psDescription
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setDescription (?string $psDescription = null) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string|null $psHelp
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setHelp (?string $psHelp = null) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string|null $psIcon
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function setIcon (?string $psIcon = null) : InterfaceGrid;
	
	
	/**
	 *
	 * @param string $psVar
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public function getProperties (string $psVar);
	
	
	/**
	 *
	 * @return object|null
	 */
	public function getParent () : ?object;
	
	
	/**
	 *
	 * @return bool
	 */
	public function isEnable () : bool;
	
	
	/**
	 *
	 * @return bool
	 */
	public function isClone () : bool;
	
	
	/**
	 * Return the whole object and its children as an array
	 *
	 * @return array
	 */
	public function toArray () : array;
	
	
	/**
	 * Return the value of the object and its children
	 *
	 * The method "get" could be used as:
	 * without pass any specific var, it will return the whole object
	 * ```
	 * get();
	 * ```
	 * or passing a property name, return just the object property required
	 * ```
	 * get('property_name');
	 * ```
	 * or passing an array whith the path to the deeped property.
	 * ```
	 * get(array('module'=>'value1','controller'=>'value2','button'=>'value3'));
	 * ```
	 * In this case it will return the button element identified by id = value3
	 *
	 * @param string|null $pmVar
	 * @return mixed
	 */
	public function get ($pmVar = null);
	
	
	/**
	 * Return a clone of the object.
	 * The change maked in this object clone will not affect the original one.
	 *
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function copy () : InterfaceGrid;
	
	
	/**
	 *
	 * @param string $psItem
	 * @param array $paOrder
	 * @param \OnionGrid\InterfaceGrid
	 */
	public function order (string $psItem, array $paOrder) : InterfaceGrid;
	
	
	/**
	 * Insere um item de array na posição desejada, deslocando os demais itens.
	 *
	 * @param array $paList Array original onde será inserido o novo elemento.
	 * @param \OnionGrid\InterfaceGrid $poValue Elemento a ser inserido no array original.
	 * @param string|null $psIndex Identificador do indice do elemento, podendo ser null quando for igual ao id do elemento.
	 * @param int|null $pnPosition Posição em que o elemento $poValue será inserido na nova lista.
	 * @param bool $pbNumericIndex Informa quando o indice do array for numérico.
	 * @return array - Nova lista com o elemento inserido na posição desejada e com os demais elementos deslocados.
	 */
	public function position (array $paList, InterfaceGrid $poValue, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : array;
	
	
	/**
	 * Create a new object into the array object
	 * and setting its id and name
	 *
	 * @param string $psArrayObj
	 * @param string $psElementId
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function create (string $psArrayObj, string $psElementId) : InterfaceGrid;
	
	
	/**
	 * Add an existent object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param string $psArrayObj
	 * @param \OnionGrid\InterfaceGrid $poElement
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @param bool $pbNumericIndex
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : InterfaceGrid;
	
	
	/**
	 * Remove the element from the array object
	 *
	 * @param string $psArrayObj
	 * @param string $psElementId
	 * @return \OnionGrid\InterfaceGrid
	 */
	public function remove (string $psArrayObj, string $psElementId) : InterfaceGrid;
	
	
	/**
	 * Load the element from array object
	 * or the entire array if $psElementId = null
	 *
	 * @param string $psArrayObj
	 * @param string|null $psElementId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\InterfaceGrid|array|null
	 */
	public function getElement (string $psArrayObj, ?string $psElementId = null, bool $pbValid = true);
}