<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;
use OnionGrid\AbstractGrid;
use OnionGrid\InterfaceGrid;


class Dropdown extends AbstractGrid implements InterfaceGrid
{
	const TYPE = 'dropdown';
	
	/**
	 * List type: `btnmenu`, `submenu` or `inline`
	 * 
	 * @var string
	 */
	protected $sType = 'btnmenu';
	
	/**
	 * Iten exibition type: `icon`, `title`, `both`
	 *
	 * @var string
	 */
	protected $sExibition = 'both';
	
	/**
	 * @var int
	 */
	protected $nToInline = 1;
	
	/**
	 * Action itens
	 * 
	 * @var array of \OnionGrid\Button
	 */
	protected $aItens = [];
	
	/**
	 * 
	 * @var bool
	 */
	protected $bShowCaret = true;
	
	/**
	 * Iten exibition type: `left` or `right`
	 * 
	 * @var string
	 */
	protected $sOrientation = '';

	/**
	 * Area: `toolbar` or `row` 
	 * 
	 * @var string
	 */
	protected $sArea = 'toolbar';

	/**
	 * @var string
	 */
	protected $sClass = '';
	
	/**
	 * @var string
	 */
	protected $sStyle = '';

	/**
	 * Position: `left` or `right` 
	 * 
	 * @var string
	 */
	protected $sPosition = 'left';
	
	/**
	 * Params to pass for action
	 *
	 * @var array
	 */
	protected $aParams = [];

	
	// Settings
	
	
	/**
	 * Construct an object setting the id, name and resource properties
	 * if the id is not given the construct will return an exception
	 *
	 * @param string $psId Instance identifier.
	 * @param string|null $psResource
	 * @param \OnionGrid\InterfaceGrid|null $poParent
	 * @throws \InvalidArgumentException
	 */	
	public function __construct (string $psId, ?string $psResource = null, ?InterfaceGrid $poParent = null)
	{
		parent::__construct($psId, $psResource, $poParent);
	}


	/**
	 * 
	 * @param array $paDropdownProp
	 * @return \OnionGrid\Dropdown
	 */
	public function factory (array $paDropdownProp) : Dropdown
	{
		if (is_array($paDropdownProp))
		{
			foreach ($paDropdownProp as $lsProperty => $lmValue)
			{
				$lsMethod = "set{$lsProperty}";

				if (method_exists($this, $lsMethod) && !is_null($lmValue))
				{
					$this->$lsMethod($lmValue);
				}
			}
		}

		return $this;
	}

	
	/**
	 * 
	 * @param array $paItensProp
	 * @return \OnionGrid\Dropdown
	 */
	public function setItens (array $paItensProp) : Dropdown
	{
		if (is_array($paItensProp))
		{
			$laItens = $this->getIten();

			foreach ($laItens as $loIten)
			{
				if (isset($paItensProp[$loIten->getId()]))
				{
					$laProperties = $paItensProp[$loIten->getId()];

					if (is_array($laProperties))
					{
						$loIten->factory($laProperties);
					}
				}
			}
		}

		return $this;
	}

	
	/**
	 * List type: `btnmenu`, `submenu` or `inline`
	 *  
	 * @param string $psType
	 * @return \OnionGrid\Dropdown
	 */
	public function setType (string $psType = 'btnmenu') : Dropdown
	{
		$laOptions = [
			'btnmenu' => 1,
			'submenu' => 1,
			'inline' => 1,
		];
		
		if(isset($laOptions[strtolower($psType)]))
		{
			$this->sType = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Menu type option error, try: btnmenu, submenu or inline');
		}
		
		return $this;
	}
	
	
	/**
	 * Iten exibition type: `icon`, `title`, `both`
	 * 
	 * @param string $psExibition
	 * @return \OnionGrid\Dropdown
	 */
	public function setExibition (string $psExibition = 'both') : Dropdown
	{
		$laOptions = [
			'icon' => 1,
			'title' => 1,
			'both' => 1,
		];
		
		if(isset($laOptions[strtolower($psExibition)]))
		{
			$this->sExibition = $psExibition;
		}
		else
		{
			throw new \InvalidArgumentException('Menu exibition option error, try: icon, title, both');
		}
		
		return $this;
	}
	
	
	/**
	 * Iten exibition type: `left` or `right`
	 *
	 * @param string|null $psOrientation
	 * @return \OnionGrid\Dropdown
	 */
	public function setOrientation (?string $psOrientation = 'null') : Dropdown
	{
		$laOptions = [
				'left' => 1,
				'right' => 1,
		];
		
		if(is_null($psOrientation) || isset($laOptions[strtolower($psOrientation)]))
		{
			$this->sOrientation = $psOrientation;
		}
		else
		{
			throw new \InvalidArgumentException('Menu orientation option error, try: left or right');
		}
		
		return $this;
	}

	
	/**
	 * Area: `toolbar` or `row`
	 *
	 * @param string $psArea
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Dropdown
	 */
	public function setArea (string $psArea = 'toolbar') : Dropdown
	{
		switch(strtolower($psArea))
		{
			case 'toolbar':
			case 'row':
				$this->sArea = $psArea;
				break;
				
			default:
				throw new \InvalidArgumentException('The area value should be toolbar or row!');
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param int $pnToInline
	 * @return \OnionGrid\Dropdown
	 */
	public function setToInline (int $pnToInline = 1) : Dropdown
	{
		if (is_int((int)$pnToInline))
		{
			$this->nToInline = $pnToInline;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowCaret
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Dropdown
	 */
	public function setShowCaret (bool $pbShowCaret = true) : Dropdown
	{
		if (is_bool($pbShowCaret))
		{
			$this->bShowCaret= $pbShowCaret;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "pbShowCaret" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psClass
	 * @return \OnionGrid\Dropdown
	 */
	public function setClass (string $psClass = '') : Dropdown
	{
		if (!empty($psClass))
		{
			$this->sClass = $psClass;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psStyle
	 * @return \OnionGrid\Dropdown
	 */
	public function setStyle (string $psStyle = '') : Dropdown
	{
		if (!empty($psStyle))
		{
			$this->sStyle = $psStyle;
		}
		
		return $this;
	}

	
	/**
	 * Iten position: `left` or `right`
	 *
	 * @param string|null $psPosition
	 * @return \OnionGrid\Dropdown
	 */
	public function setPosition (?string $psPosition = null) : Dropdown
	{
		$laOptions = [
				'left' => 1,
				'right' => 1,
		];
		
		if(isset($laOptions[strtolower($psPosition)]))
		{
			$this->sPosition = $psPosition;
		}
		else
		{
			throw new \InvalidArgumentException('Option position error, try: left or right');
		}
		
		return $this;
	}
	
	
	/**
	 * Params to pass for action
	 *
	 * @param array $paParams
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Dropdown
	 */
	public function setParams (array $paParams) : Dropdown
	{
		if (is_array($paParams))
		{
			$this->aParams = $paParams;
		}
		else
		{
			throw new \InvalidArgumentException('The params value should be an array!');
		}
		
		return $this;
	}
	
	
	// Action methods
	
	
	/**
	 * Create a new Button object into the array object
	 * and setting its id and name
	 *
	 * @param string $psButtonId        	
	 * @return \OnionGrid\Button
	 */
	public function createIten (string $psButtonId) : Button
	{
		$loButton = new Button($psButtonId, $this->sResource, $this);
		$this->aItens[] = $loButton;
		
		return $loButton;
	}

	
	/**
	 * Add an existent Button object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Button $poButton        	
	 * @param string|null $psIndex        	
	 * @param int|null $pnPosition        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Dropdown
	 */
	public function addIten (Button $poButton, ?string $psIndex = null, ?int $pnPosition = null) : Dropdown
	{
		if ($poButton instanceof Button)
		{
			return parent::add('aItens', $poButton, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poButton should be a instance of \OnionGrid\Button!');
		}
	}
	

	/**
	 * Remove a Button from the array object
	 *
	 * @param string $psButtonId
	 * @return \OnionGrid\Dropdown
	 */
	public function removeIten (string $psButtonId) : Dropdown
	{
		return parent::remove('aItens', $psButtonId);
	}

	
	/**
	 * Load the Button object from array object
	 * or the entire array if $psButtonId = null
	 *
	 * @param string|null $psButtonId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Button|array|null
	 */
	public function getIten (?string $psButtonId = null, bool $pbValid = true)
	{
		return parent::getElement('aItens', $psButtonId, $pbValid);
	}	
	
		
	/**
	 * Create a new Dropdown object into the array object
	 * and setting its id and name
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Dropdown
	 */
	public function createOptions (string $psDropdownId) : Dropdown
	{
		$loDropdown = new Dropdown($psDropdownId, $this->sResource, $this);
		$this->aItens[] = $loDropdown;
		
		return $loDropdown;
	}
	
	
	/**
	 * Add an existent Dropdown object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Dropdown $poDropdown
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Dropdown
	 */
	public function addOptions (Dropdown $poDropdown, ?string $psIndex = null, ?int $pnPosition = null) : Dropdown
	{
		if ($poDropdown instanceof Dropdown)
		{
			return parent::add('aItens', $poDropdown, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poDropdown should be a instance of \OnionGrid\Dropdown!');
		}
	}
	
	
	/**
	 * Remove an Dropdown from the array object
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Dropdown
	 */
	public function removeOptions (string $psDropdownId) : Dropdown
	{
		return parent::remove('aItens', $psDropdownId);
	}
	
	
	/**
	 * Load the Dropdown object from array object
	 * or the entire array if $psDropdownId = null
	 *
	 * @param string|null $psDropdownId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Dropdown|array|null
	 */
	public function getOptions (?string $psDropdownId = null, bool $pbValid = true)
	{
		return parent::getElement('aItens', $psDropdownId, $pbValid);
	}
	
	
	/**
	 * Create a new separator object into the array object
	 * and setting its id and name
	 *
	 * @param string|null $psSeparatorId
	 * @return \OnionGrid\Button
	 */
	public function createSeparator (?string $psSeparatorId = null) : Button
	{
	    if (is_null($psSeparatorId))
	    {
	       $psSeparatorId = "-/separator/-" . microtime();
	    }
	    
	    $loSeparator = new Button($psSeparatorId, $this->sResource, $this);
	    $loSeparator->setType('separator');
	    $loSeparator->setTitle(null);
	    $this->aItens[] = $loSeparator;
	    
	    return $loSeparator;
	}
	
	
	/**
	 * Add a separator to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @return \OnionGrid\Dropdown
	 */	
	public function addSeparator (?string $psIndex = null, ?int $pnPosition = null) : Dropdown
	{
		$lsId = "-/separator/-" . microtime();
		$loSeparator = new Button($lsId, $this->sResource, $this);
		$loSeparator->setType('separator');
		$loSeparator->setTitle(null);
		
		return parent::add('aItens', $loSeparator, $psIndex, $pnPosition, true);	
	}
	
	
	/**
	 * Remove a separator from the array object
	 *
	 * @param string $psIndex
	 * @return \OnionGrid\Dropdown
	 */
	public function removeSeparator (string $psIndex) : Dropdown
	{
		return parent::remove('aItens', $psIndex);
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function getResponseType () : string
	{
		return $this->oParent->getResponseType();
	}
	
	
	/**
	 *
	 * @param array $paParams
	 * @return \OnionGrid\Dropdown
	 */
	public function prepare (array $paParams = []) : Dropdown
	{
		return $this;
	}
	
	
	/**
	 *
	 * @param array $paParams
	 * @return string|null
	 */
	public function render (array $paParams = []) : ?string
	{
		$lsResponse = $this->getResponseType();
		
		switch ($lsResponse)
		{
			case 'CSV':
			case 'XLS':
			case 'PDF':
				return '';
				break;
			case 'OBJ':
				$this->renderObj($paParams);
				return null;
				break;
			default:
				return $this->renderHtml($paParams);
		}
	}
	
	
	/**
	 *
	 * @param array $paParams
	 */
	public function renderObj (array $paParams = []) : void
	{
		$laItens = $this->getIten();
		
		foreach($laItens as $loIten)
		{
			if ($loIten instanceof Button || $loIten instanceof Dropdown)
			{
				$loIten->setArea($this->sArea);
				$laParams = $loIten->get('aParams');
				
				if (isset($laParams['data-folder']) && !empty(trim($laParams['data-folder'])) && $loIten->isEnable())
				{
					$loIten->setEnable(false);
					$laFolders = explode(",", $laParams['data-folder']);
					
					foreach ($laFolders as $lsFolder)
					{
						if (!isset($paParams['action']) || trim($lsFolder) == $paParams['action'])
						{
							$loIten->setEnable(true);
							continue;
						}
					}
				}
				
				if ($loIten->isEnable())
				{
					$lsParentId = '';
					
					if (isset($paParams['row']['id']))
					{
						$lsParentId = "-{$paParams['row']['id']}";
					}
					
					$loIten->setId($loIten->get('sId') . $lsParentId);
					$loIten->setArea('dropdown');
					$loIten->render($paParams);
				}
			}
		}
		
		if (isset($paParams['row']) && is_array($paParams['row']))
		{
			foreach ($paParams['row'] as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}
			
			$this->sId = preg_replace($laPattern, $paParams['row'], $this->sId);
			$this->sName = preg_replace($laPattern, $paParams['row'], $this->sName);
			$this->sIcon = preg_replace($laPattern, $paParams['row'], $this->sIcon);
			$this->sTitle = preg_replace($laPattern, $paParams['row'], $this->sTitle);
			$this->sDescription = preg_replace($laPattern, $paParams['row'], $this->sDescription);
			$this->sClass = preg_replace($laPattern, $paParams['row'], $this->sClass);
			$this->sStyle = preg_replace($laPattern, $paParams['row'], $this->sStyle);
			$this->aParams = preg_replace($laPattern, $paParams['row'], $this->aParams);
		}
	}
	
	
	/**
	 * 
	 * @param array $paParams
	 * @return string
	 */
	public function renderHtml (array $paParams = []) : string
	{
		$laItens = $this->getIten();
		$laDropdown = [];
		$lnDropdownQt = 0;
		$lnItensBeforeSeparate = 0;
		$loLastIten = null;
		
		foreach($laItens as $lnPos => $loIten)
		{
			if ($loIten instanceof Button || $loIten instanceof Dropdown)
			{
				$loIten->setArea($this->sArea);
				$laParams = $loIten->get('aParams');
				
				if (isset($laParams['data-folder']) && !empty(trim($laParams['data-folder'])) && $loIten->isEnable())
				{
					$loIten->setEnable(false);
					$laFolders = explode(",", $laParams['data-folder']);
				
					foreach ($laFolders as $lsFolder)
					{
						if (!isset($paParams['action']) || trim($lsFolder) == $paParams['action'])
						{
							$loIten->setEnable(true);
							continue;
						}
					}
				}
				
				if ($loIten->isEnable())
				{
					$loIten->prepare($paParams);
					
					if ($loIten->isEnable())
					{
						$laDropdown[] = $loIten;
						
						if ($loIten->get('sType') != 'separator')
						{
							$lnDropdownQt++;
							$lnItensBeforeSeparate++;
							$loLastIten = $loIten;
						}
						elseif ($lnItensBeforeSeparate == 0)
						{
							$loIten->setEnable(false);
						}
						else
						{
							$lnItensBeforeSeparate = 0;
							$loLastIten = $loIten;
						}
					}
				}
			}
		}
		
		if (!is_null($loLastIten) && $loLastIten->get('sType') == 'separator')
		{
			$loLastIten->setEnable(false);
		}
		
		$loParent = $this->getParent();

		if ($loParent::TYPE == 'column' && $loParent->get('sType') == 'actions')
		{
			$loParent = $loParent->getParent();

			if ($loParent::TYPE == 'grid' && $loParent->getDeviceType() == 'mobile')
			{
				//$this->sType = 'inline';
			}
		}
		
		$lsGroupDropdown = '';

		if ($lnDropdownQt > $this->nToInline && $this->sType != 'inline')
		{
			$lsDropdown = '';
			
			foreach ($laDropdown as $loIten)
			{
				$loIten->setArea('dropdown');

				if ($this->sType != 'submenu')
				{
					$loIten->setArea('row-dropdown');
				}

				$lsDropdown .= $this->parseTemplate(
					$this->getGridObject()->getTemplate('dropdown', 'dropdown-iten'), 
					['button' => $loIten->render($paParams)]
				);
			}
			
			$lsCaret = '';
			
			if ($this->bShowCaret)
			{
				$lsCaret = $this->getGridObject()->getTemplate('dropdown', 'caret');
			}
			
			$lsIcon = $this->renderIcon($this->sIcon);
			
			$lsParams = '';
			
			if (is_array($this->aParams))
			{
				foreach ($this->aParams as $lsName => $lmValue)
				{
					if (is_array($lmValue))
					{
						$lsValue = json_encode($lmValue);
						$lsParams .= "{$lsName}='{$lsValue}'";
					}
					else
					{
						$lsParams .= "{$lsName}=\"{$lmValue}\" ";
					}
				}
			}
			
			$lsOrientation = '';

			if (!empty($this->sOrientation))
			{
				$lsOrientation = $this->getGridObject()->getTemplate('class', "orientation-{$this->sOrientation}");
			}
			
			if ($this->sType == 'submenu')
			{
				$lsGroupDropdown = $this->parseTemplate(
					$this->getGridObject()->getTemplate('dropdown', 'on-toolbar'),
					[
						'class' => $this->sClass, 
						'style' => $this->sStyle, 
						'desc' => $this->sDescription, 
						'params' => $lsParams, 
						'icon' => trim((string)$lsIcon),
						'label' => trim((string)$this->sTitle),
						'caret' => $lsCaret, 
						'orientation' => $lsOrientation, 
						'dropdown' => $lsDropdown
					]
				);
			}
			else
			{
				$lsGroupDropdown = $this->parseTemplate(
					$this->getGridObject()->getTemplate('dropdown', 'on-row'),
					[
						'class' => $this->sClass, 
						'style' => $this->sStyle, 
						'desc' => $this->sDescription, 
						'params' => $lsParams, 
						'icon' => trim((string)$lsIcon),
						'label' => trim((string)$this->sTitle),
						'caret' => $lsCaret, 
						'orientation' => $lsOrientation, 
						'dropdown' => $lsDropdown
					]
				);
			}
		}
		else 
		{
			$lsArea = 'toolbar';

			if ($this->oParent instanceof Column)
			{
				$lmColWidth = $this->oParent->get('mOriginalWidth');
				$lnColWidth = $lnDropdownQt * (int)$lmColWidth;
				$lsColWidth = preg_replace('/([0-9]+)/', (string)$lnColWidth, (string)$lmColWidth);
				$this->oParent->setWidth($lsColWidth);
				$lsArea = 'row';
			}
			
			$lsSpace = '';
			
			foreach ($laDropdown as $loIten)
			{
				if ($loIten->get('sType') != 'separator')
				{
					$this->exibition($loIten);
					$loIten->setArea($lsArea);

					$lsGroupDropdown .= $lsSpace . $loIten->render($paParams);
					$lsSpace = '&nbsp;';
				}
			}
		}

		if (!empty($lsGroupDropdown) && isset($paParams['row']) && is_array($paParams['row']))
		{
			foreach ($paParams['row'] as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}
			
			$lsGroupDropdown = preg_replace($laPattern, $paParams['row'], $lsGroupDropdown);
		}
		
		return $lsGroupDropdown;
	}
	
	
	/**
	 * 
	 * @param \OnionGrid\Button $poBtn
	 */
	public function exibition (Button $poBtn) : void
	{
		if ($this->sExibition == 'icon')
		{
			$poBtn->setTitle(null);
		}
		elseif ($this->sExibition == 'title')
		{
			$poBtn->setIcon(null);
		}
	}


	public function set ($pmVar, $pmValue = null) : Dropdown
	{
		parent::set($pmVar, $pmValue);
		return $this;
	}


	public function setEnable (bool $pbEnable) : Dropdown
	{
		parent::setEnable($pbEnable);
		return $this;
	}


	public function setParent (?object $poParent = null) : Dropdown
	{
		$this->oParent = $poParent;
		return $this;
	}
	
	
	public function setResource (string $psResource) : Dropdown
	{
		parent::setResource($psResource);
		return $this;
	}

	
	public function setId (string $psId) : Dropdown
	{
		parent::setId($psId);
		return $this;
	}


	public function setName (?string $psName = null) : Dropdown
	{
		parent::setName($psName);
		return $this;
	}


	public function setTitle (?string $psTitle = null) : Dropdown
	{
		parent::setTitle($psTitle);	
		return $this;
	}


	public function setDescription (?string $psDescription = null) : Dropdown
	{
		parent::setDescription($psDescription);
		return $this;
	}


	public function setHelp (?string $psHelp = null) : Dropdown
	{
		$this->sHelp = $psHelp;
		
		return $this;
	}


	public function setIcon (?string $psIcon = null) : Dropdown
	{
		parent::setIcon($psIcon);
		return $this;
	}


	public function setPrepared (bool $pbPrepared) : Dropdown
	{
		parent::setPrepared($pbPrepared);
		return $this;
	}


	public function order (string $psItem, array $paOrder) : Dropdown
	{
		parent::order($psItem, $paOrder);
		return $this;
	}


	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : Dropdown
	{
		parent::add($psArrayObj, $poElement, $psIndex, $pnPosition, $pbNumericIndex);
		return $this;
	}


	public function remove (string $psArrayObj, string $psElementId) : Dropdown
	{
		parent::remove($psArrayObj, $psElementId);
		return $this;
	}	
}