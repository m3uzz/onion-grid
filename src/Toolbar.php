<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;
use OnionGrid\AbstractGrid;
use OnionGrid\Button;
use OnionGrid\GridText;
use OnionGrid\InterfaceGrid;
use OnionGrid\Dropdown;


class Toolbar extends AbstractGrid implements InterfaceGrid
{
	const TYPE = 'toolbar';

	/**
	 * Can be: `horizontal`, `vertical` or `closed`
	 * 
	 * @var string
	 */
	protected $sType = 'horizontal';
	
	/**
	 * 
	 * @var bool
	 */
	protected $bShowReload = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowPrint = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowExport = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowFilter = false;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowSearch = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowViewCol = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowNumRows = true;
	
	/**
	 * 
	 * @var bool
	 */
	protected $bShowDefaultToolbar = true;
	
	/**
	 * Action itens
	 *
	 * @var array of \OnionGrid\Button or \OnionGrid\Dropdown
	 */
	protected $aItens = [];
	
	/**
	 * 
	 * @var array
	 */
	protected $aItensDefault = [];
	
	/**
	 * 
	 * @var array
	 */
	protected $aSearchRender = [];
	
	/**
	 * 
	 * @var array
	 */
	protected $aFilter = [];
	
	/**
	 *
	 * @var callable
	 */
	protected $cFilter = null;

	
	// Settings
	
	
	/**
	 * Construct an object setting the id, name and resource properties
	 * if the id is not given the construct will return an exception
	 *
	 * @param string $psId Instance identifier.
	 * @param string|null $psResource
	 * @param object|null $poParent
	 * @throws \InvalidArgumentException
	 */
	public function __construct (string $psId, ?string $psResource = null, ?object $poParent = null)
	{
		parent::__construct($psId, $psResource, $poParent);
	}

	
	/**
	 * 
	 * @param array $paToolbarProp
	 * @return \OnionGrid\Toolbar
	 */
	public function factory (array $paToolbarProp) : Toolbar
	{
		if (is_array($paToolbarProp))
		{
			foreach ($paToolbarProp as $lsProperty => $lmValue)
			{
				$lsMethod = "set{$lsProperty}";

				if (method_exists($this, $lsMethod) && !is_null($lmValue))
				{
					$this->$lsMethod($lmValue);
				}
			}
		}

		return $this;
	}

	
	/**
	 * 
	 * @param array $paButtonProp
	 * @return \OnionGrid\Toolbar
	 */
	public function setButtons (array $paButtonProp) : Toolbar
	{
		if (is_array($paButtonProp))
		{
			$laButtons = $this->getButton();

			foreach ($laButtons as $loButton)
			{
				if (isset($paButtonProp[$loButton->getId()]))
				{
					$laProperties = $paButtonProp[$loButton->getId()];

					if (is_array($laProperties))
					{
						$loButton->factory($laProperties);
					}
				}
			}
		}

		return $this;
	}	


	/**
	 * 
	 * @param array $paDropdownsProp
	 * @return \OnionGrid\Toolbar
	 */
	public function setDropdowns (array $paDropdownsProp) : Toolbar
	{
		if (is_array($paDropdownsProp))
		{
			$laDropdowns = $this->getDropdown();

			foreach ($laDropdowns as $loDropdown)
			{
				if (isset($paOptionsProp[$loDropdown->getId()]))
				{
					$laProperties = $paOptionsProp[$loDropdown->getId()];

					if (is_array($laProperties))
					{
						$loDropdown->factory($laProperties);
					}
				}
			}
		}

		return $this;
	}

	
	/**
	 * List type: `horizontal`, `vertical` or `closed`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setType (string $psType = 'horizontal') : Toolbar
	{
		$laOptions = [
				'horizontal' => 1,
				'vertical' => 1,
				'closed' => 1,
		];
		
		if(isset($laOptions[strtolower($psType)]))
		{
			$this->sType = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Menu type option error, try: horizontal, vertical or closed');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowDefaultToolbar
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowDefaultToolbar (bool $pbShowDefaultToolbar = true) : Toolbar
	{
		if (is_bool($pbShowDefaultToolbar))
		{
			$this->bShowDefaultToolbar = $pbShowDefaultToolbar;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "pbShowReloadBtn" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowReloadBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowReloadBtn (bool $pbShowReloadBtn = true) : Toolbar
	{
		if (is_bool($pbShowReloadBtn))
		{
			$this->bShowReload = $pbShowReloadBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "pbShowReloadBtn" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowPrintBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowPrintBtn (bool $pbShowPrintBtn = true) : Toolbar
	{
		if (is_bool($pbShowPrintBtn))
		{
			$this->bShowPrint = $pbShowPrintBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "pbShowPrintBtn" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowExportBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowExportBtn (bool $pbShowExportBtn = true) : Toolbar
	{
		if (is_bool($pbShowExportBtn))
		{
			$this->bShowExport = $pbShowExportBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "bShowExport" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowSearchBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowSearchBtn (bool $pbShowSearchBtn = true) : Toolbar
	{
		if (is_bool($pbShowSearchBtn))
		{
			$this->bShowSearch = $pbShowSearchBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "pbShowSearchBtn" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param bool $pbShowFilterBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowFilterBtn (bool $pbShowFilterBtn = false) : Toolbar
	{
		if (is_bool($pbShowFilterBtn))
		{
			$this->bShowFilter = $pbShowFilterBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "bShowFilter" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowViewColBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowViewColBtn (bool $pbShowViewColBtn = true) : Toolbar
	{
		if (is_bool($pbShowViewColBtn))
		{
			$this->bShowViewCol = $pbShowViewColBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "bShowViewCol" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowNumRowsBtn
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function setShowNumRowsBtn (bool $pbShowNumRowsBtn = true) : Toolbar
	{
		if (is_bool($pbShowNumRowsBtn))
		{
			$this->bShowNumRows = $pbShowNumRowsBtn;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "bShowNumRows" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param array|null $paFilter
	 * @return \OnionGrid\Toolbar
	 */
	public function setFilterRender (?array $paFilter = null) : Toolbar
	{
		$this->aFilter = $paFilter;
		
		return $this;
	}
	
	
	/**
	 * ```
	 * setFilter(function (\OnionGrid\Toolbar $poToolbar) : string);
	 * ```
	 * 
	 * @param callable|null $pcFilter
	 * @return \OnionGrid\Toolbar
	 */
	public function setFilter (?callable $pcFilter = null) : Toolbar
	{
		if (is_callable($pcFilter))
		{
			$this->bShowFilter = true;
			$this->cFilter = $pcFilter;
		}
		
		return $this;
	}
	
	
	// Action methods
	
	
	/**
	 *
	 * @return bool
	 */
	public function isShowSearch () : bool
	{
		return $this->bShowSearch;
	}
	
	
	/**
	 * Create a new Button object into the array object
	 * and setting its id and name
	 *
	 * @param string $psButtonId
	 * @param string $psWhere
	 * @return \OnionGrid\Button
	 */
	public function createButton (string $psButtonId, string $psWhere = 'aItens') : Button
	{
		$loButton = new Button($psButtonId, $this->sResource, $this);
		$this->{$psWhere}[] = $loButton;
		
		return $loButton;
	}
	
	
	/**
	 * Add an existent Button object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Button $poButton
	 * @param string $psIndex
	 * @param int $pnPosition
	 * @param string $psWhere
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function addButton (Button $poButton, string $psIndex = null, int $pnPosition = null, string $psWhere = 'aItens') : Toolbar
	{
		if ($poButton instanceof Button)
		{
			return parent::add($psWhere, $poButton, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poButton should be a instance of OnionGrid\Button!');
		}
	}
	
	
	/**
	 * Remove a Button from the array object
	 *
	 * @param string $psButtonId
	 * @param string $psWhere
	 * @return \OnionGrid\Toolbar
	 */
	public function removeButton (string $psButtonId, string $psWhere = 'aItens') : Toolbar
	{
		return parent::remove($psWhere, $psButtonId);
	}
	
	
	/**
	 * Load the Button object from array object
	 * or the entire array if $psButtonId = null
	 *
	 * @param string|null $psButtonId
	 * @param bool $pbValid
	 * @param string $psWhere
	 * @throws \Exception
	 * @return \OnionGrid\Button|array|null
	 */
	public function getButton (?string $psButtonId = null, bool $pbValid = true, string $psWhere = 'aItens')
	{
		return parent::getElement($psWhere, $psButtonId, $pbValid);
	}

	
	/**
	 * Create a new Button object into the array object
	 * and setting its id and name
	 *
	 * @param string $psButtonId
	 * @return \OnionGrid\Button
	 */
	public function createBtnDef (string $psButtonId) : Button
	{
		return $this->createButton($psButtonId, 'aItensDefault');
	}
	
	
	/**
	 * Add an existent Button object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Button $poButton
	 * @param string $psIndex
	 * @param int $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function addBtnDef (Button $poButton, string $psIndex = null, int $pnPosition = null) : Toolbar
	{
		return $this->addButton($poButton, $psIndex, $pnPosition, 'aItensDefault');
	}
	
	
	/**
	 * Remove a Button from the array object
	 *
	 * @param mixed $psButtonId
	 * @return \OnionGrid\Toolbar
	 */
	public function removeBtnDef ($psButtonId) : Toolbar
	{
		return $this->removeButton($psButtonId, 'aItensDefault');
	}
	
	
	/**
	 * Load the Button object from array object
	 * or the entire array if $psButtonId = null
	 *
	 * @param mixed $psButtonId
	 * @param bool $pbValid
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button|array|null
	 */
	public function getBtnDef ($psButtonId = null, bool $pbValid = true)
	{
		return $this->getButton($psButtonId, $pbValid, 'aItensDefault');
	}


	/**
	 * Create a new Dropdown object into the array object
	 * and setting its id and name
	 *
	 * @param string $psDropdownId
	 * @param string $psWhere
	 * @return \OnionGrid\Dropdown
	 */
	public function createDropdown (string $psDropdownId, string $psWhere = 'aItens') : Dropdown
	{
		$loDropdown = new Dropdown($psDropdownId, $this->sResource, $this);
		$this->{$psWhere}[] = $loDropdown;
		
		return $loDropdown;
	}
	
	
	/**
	 * Add an existent Dropdown object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Dropdown $poDropdown
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @param string $psWhere
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function addDropdown (Dropdown $poDropdown, ?string $psIndex = null, ?int $pnPosition = null, string $psWhere = 'aItens') : Toolbar
	{
		if ($poDropdown instanceof Dropdown)
		{
			return parent::add($psWhere, $poDropdown, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poDropdown should be a instance of \OnionGrid\Dropdown!');
		}
	}
	
	
	/**
	 * Remove an Dropdown from the array object
	 *
	 * @param mixed $psDropdownId
	 * @param string $psWhere
	 * @return \OnionGrid\Toolbar
	 */
	public function removeDropdown ($psDropdownId, string $psWhere = 'aItens') : Toolbar
	{
		return parent::remove($psWhere, $psDropdownId);
	}
	
	
	/**
	 * Load the Dropdown object from array object
	 * or the entire array if $psDropdownId = null
	 *
	 * @param mixed $psDropdownId
	 * @param bool $pbValid
	 * @param string $psWhere
	 * @throws \Exception
	 * @return \OnionGrid\Dropdown|array|null
	 */
	public function getDropdown ($psDropdownId = null, bool $pbValid = true, string $psWhere = 'aItens')
	{
		return parent::getElement($psWhere, $psDropdownId, $pbValid);
	}
	
	
	/**
	 * Create a new Dropdown object into the array object
	 * and setting its id and name
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Dropdown
	 */
	public function createDropdownDef (string $psDropdownId) : Dropdown
	{
		return $this->createDropdown($psDropdownId, 'aItensDefault');
	}
	
	
	/**
	 * Add an existent Dropdown object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Dropdown $poDropdown
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Toolbar
	 */
	public function addDropdownDef (Dropdown $poDropdown, ?string $psIndex = null, ?int $pnPosition = null) : Toolbar
	{
		return $this->addDropdown($poDropdown, $psIndex, $pnPosition, 'aItensDefault');
	}
	
	
	/**
	 * Remove an Dropdown from the array object
	 *
	 * @param mixed $psDropdownId
	 * @return \OnionGrid\Toolbar
	 */
	public function removeDropdownDef ($psDropdownId) : Toolbar
	{
		return $this->removeDropdown($psDropdownId, 'aItensDefault');
	}
	
	
	/**
	 * Load the Dropdown object from array object
	 * or the entire array if $psDropdownId = null
	 *
	 * @param mixed $psDropdownId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Dropdown|array|null
	 */
	public function getDropdownDef ($psDropdownId = null, bool $pbValid = true)
	{
		return $this->getDropdown($psDropdownId, $pbValid, 'aItensDefault');
	}
	
	
	/**
	 * Add a separator to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @param string $psWhere
	 * @return \OnionGrid\Toolbar
	 */
	public function addSeparator (?string $psIndex = null, ?int $pnPosition = null, string $psWhere = 'aItens') : Toolbar
	{
		$lsId = "-/separator/-" . microtime();
		$loSeparator = new Button($lsId, $this->sResource, $this);
		$loSeparator->setType('separator');
		$loSeparator->setTitle(null);
		
		return parent::add($psWhere, $loSeparator, $psIndex, $pnPosition, true);
	}
	
	
	/**
	 * Remove a separator from the array object
	 *
	 * @param string $psIndex
	 * @param string $psWhere
	 * @return \OnionGrid\Toolbar
	 */
	public function removeSeparator (string $psIndex, string $psWhere = 'aItens') : Toolbar
	{
		return parent::remove($psWhere, $psIndex);
	}
	
	
	/**
	 * Add a separator to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @return \OnionGrid\Toolbar
	 */
	public function addSepDef (?string $psIndex = null, ?int $pnPosition = null) : Toolbar
	{
		return $this->addSeparator($psIndex, $pnPosition, 'aItensDefault');
	}
	
	
	/**
	 * Remove a separator from the array object
	 *
	 * @param string $psIndex
	 * @return \OnionGrid\Toolbar
	 */
	public function removeSepDef (string $psIndex) : Toolbar
	{
		return $this->removeSeparator($psIndex, 'aItensDefault');
	}
	
	
	/**
	 *
	 * @return \OnionGrid\Toolbar
	 */
	public function prepare () : Toolbar
	{
		if ($this->bShowDefaultToolbar)
		{
			$this->reloadBtn();
			
			$this->printBtn();

			$this->searchBtn();

			$this->filterBtn();

			$this->exportSelect();		
			
			$this->fieldsViewSelect();
			
			$this->numRowsSelect();
		}
		
		return $this;
	}
	
	
	/**
	 *
	 *  @return void
	 */
	public function reloadBtn () : void
	{
		if ($this->bShowReload)
		{
			$this->createBtnDef('reloadBtn')
				->setEnable($this->bShowReload)
				->setTitle($this->parseTemplate($this->getGridObject()->getTemplate('toolbar', 'btn-title'), ['title' => GridText::text('sReload')]))
				->setDescription(GridText::text('sReload'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'reloadBtn'))
				->setHref($this->oParent->get('sRoute') . $this->oParent->get('sAction') . '/?' . $this->oParent->get('sGetParam'))
				->setTarget('action')
				->setClass('onion-grid-reload-btn');
		}
	}
	
	
	/**
	 *
	 *  @return void
	 */
	public function printBtn () : void
	{
		if ($this->bShowPrint)
		{
			$this->createBtnDef('printBtn')
				->setEnable($this->bShowPrint)
				->setTitle($this->parseTemplate($this->getGridObject()->getTemplate('toolbar', 'btn-title'), ['title' => GridText::text('sPrint')]))
				->setDescription(GridText::text('sPrint'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'printBtn'))
				->setHref('javascript:window.print();')
				->setParams([
					'data-loader' => false
				]);
		}
	}
	
	
	/**
	 *
	 *  @return void
	 */
	public function exportSelect () : void
	{
		if ($this->bShowExport)
		{
			$loDropdown = $this->createDropdownDef('exportSelect')
				->setType('submenu')
				->setTitle($this->parseTemplate($this->getGridObject()->getTemplate('toolbar', 'btn-title'), ['title' => GridText::text('sExport')]))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'exportSelect'))
				->setDescription(GridText::text('sExport'));
			
			$loDropdown->createIten('csvBtn')
				->setTitle(GridText::text('sExportCSV'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'csvBtn'))
				->setDescription(null)
				->setTarget('action')
				->setRequestType('HTTP')
				->setHref($this->oParent->get('sRoute'). 'csv/?act=' . $this->oParent->get('sAction') . '&' . $this->oParent->get('sGetParam'))
				->setAction('csv')
				->setParams([
					'data-loader' => false
				]);

			$loDropdown->createIten('pdfBtn')
				->setTitle(GridText::text('sExportPDF'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'pdfBtn'))
				->setDescription(null)
				->setTarget('action')
				->setRequestType('HTTP')
				->setHref($this->oParent->get('sRoute'). 'pdf/?act=' . $this->oParent->get('sAction') . '&' . $this->oParent->get('sGetParam'))
				->setAction('pdf')
				->setParams([
					'data-loader' => false
				]);
			
			$loDropdown->createIten('xlsBtn')
				->setTitle(GridText::text('sExportXLS'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'xlsBtn'))
				->setDescription(null)			
				->setTarget('action')
				->setRequestType('HTTP')
				->setHref($this->oParent->get('sRoute'). 'xls/?act=' . $this->oParent->get('sAction') . '&' . $this->oParent->get('sGetParam'))
				->setAction('xls')
				->setParams([
					'data-loader' => false
				]);
		}
	}
	
	
	/**
	 *
	 *  @return void
	 */
	public function filterBtn () : void
	{
		if ($this->bShowFilter)
		{
			$laParams = $this->getGridObject()->getTemplate('toolbar', 'collapse-btn-params');

			foreach ($laParams as $lsKey => $lsValue)
			{
				$laParams[$lsKey] = $this->parseTemplate(
					$lsValue, 
					['area-id' => "onion-form-search-area-{$this->oParent->get('sId')}"]
				);
			}

			$laParams['data-loader'] = false;
			
			$this->createBtnDef('filterBtn')
				->setEnable($this->bShowFilter)
				->setTitle($this->parseTemplate($this->getGridObject()->getTemplate('toolbar', 'btn-title'), ['title' => GridText::text('sOpenFilterForm')]))
				->setDescription(GridText::text('sOpenFilterForm'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'filterBtn'))	
				->setWithCaret(true)
				->setHref('#onion-grid-form-filter-area-' . $this->oParent->get('sId'))
				->setParams($laParams);
		}
	}
	
	
	/**
	 *
	 *  @return void
	 */
	public function searchBtn () : void
	{
		if ($this->bShowSearch)
		{
			$laParams = $this->getGridObject()->getTemplate('toolbar', 'collapse-btn-params');

			foreach ($laParams as $lsKey => $lsValue)
			{
				$laParams[$lsKey] = $this->parseTemplate(
					$lsValue, 
					['area-id' => $this->oParent->get('sId')]
				);
			}

			$laParams['data-loader'] = false;
						
			$this->createBtnDef('searchBtn')
				->setEnable($this->bShowSearch)
				->setTitle($this->parseTemplate($this->getGridObject()->getTemplate('toolbar', 'btn-title'), ['title' => GridText::text('sSearchForColum')]))
				->setDescription(GridText::text('sSearchForColum'))
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'searchBtn'))
				->setWithCaret(true)
				->setHref('#onion-grid-form-search-area-' . $this->oParent->get('sId'))
				->setParams($laParams);
		}
	}
	
	
	/**
	 *
	 * @return void
	 */
	public function fieldsViewSelect () : void
	{
		if ($this->bShowViewCol)
		{
			$laColums = $this->oParent->getColumn();
			
			if (is_array($laColums))
			{
				$loDropdown = $this->createDropdownDef('ViewColSelect')
					->setType('submenu')
					->setEnable($this->bShowViewCol)
					->setTitle($this->parseTemplate($this->getGridObject()->getTemplate('toolbar', 'btn-title'), ['title' => GridText::text('sShowColums')]))
					->setIcon($this->getGridObject()->getTemplate('button-icons', 'ViewColSelect'))
					->setDescription(GridText::text('sShowColums'))
					->setOrientation('right');
				
				foreach ($laColums as $loColum)
				{
					if ($loColum->isEnable() && $loColum->get('sType') == 'data' || $loColum->get('sType') == 'img')
					{
						$laVisible = [];
						
						if ($loColum->isVisible())
						{
							$laVisible['checked'] = "checked";
						}
						
						$loDropdown->createIten($loColum->get('sId'))
							->setTitle($loColum->get('sTitle'))
							->setDescription(null)
							->setHref(null)
							->setParams($laVisible)
							->setClass('onion-grid-select-field-to-view')
							->setFormat(function ($poButton, $paParams) {
								$laBtnParams = $poButton->get('aParams');
								$lsVisible = '';
								
								if (isset($laBtnParams['checked']))
								{
									$lsVisible = 'checked="checked"';
								}

								$lsClassCkb = $this->getGridObject()->getTemplate('class', 'checkbox');
								$lsClassLab = $this->getGridObject()->getTemplate('class', 'label');

								$poButton->setTitle('<input class="onion-grid-field-view '. $lsClassCkb .'" type="checkbox" name="vc[]" data-toUpper="false" value="' . $poButton->get('sId') . '" ' . $lsVisible . '/> <label class="'. $lsClassLab .'">' . $poButton->get('sTitle') . '</label>');
							});
					}
				}
				
				$loDropdown->createIten('separator')
					->setType('separator');

				$loDropdown->createIten('applyBtn')
					->setTitle(GridText::text('sApply'))
					->setIcon($this->getGridObject()->getTemplate('button-icons', 'applyBtn'))
					->setDescription(null)
					->setTarget('action')
					->setHref($this->oParent->get('sRoute') . $this->oParent->get('sAction') . '?' . $this->oParent->get('sGetParam'))
					->setClass("onion-grid-apply-fields-to-view {$this->getGridObject()->getTemplate('class', 'align-center')}");
			}
		}
	}
	
	
	/**
	 *
	 * @return void
	 */
	public function numRowsSelect () : void
	{
		if ($this->bShowNumRows)
		{
			$laPaginationNumRows = $this->oParent->get('aPaginationNumRows');
			
			if (is_array($laPaginationNumRows) && count($laPaginationNumRows) > 0)
			{
				$loDropdown = $this->createDropdownDef('NumRowsSelect')
					->setType('submenu')
					->setEnable($this->bShowNumRows)
					->setTitle("{$this->oParent->get('nNumRows')}")
					->setIcon($this->getGridObject()->getTemplate('button-icons', 'NumRowsSelect'))
					->setDescription(GridText::text('sNumberOfRecords'))
					->setOrientation('right');
				
				foreach ($laPaginationNumRows as $lnValue)
				{
					$loDropdown->createIten($lnValue)
						->setTitle(GridText::text('sShow') . $lnValue)
						->setDescription(null)
						->setTarget('action')
						->setClass('onion-grid-select-num-rows')
						->setHref($this->oParent->get('sRoute') . $this->oParent->get('sAction') . '?' . $this->oParent->get('sGetParam'))
						->setParams([
							'data-rows' => $lnValue
						]);
				}
			}
		}
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function getResponseType () : string
	{
		return $this->oParent->getResponseType();
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function render () : ?string
	{
		$lsResponse = $this->getResponseType();
		
		switch ($lsResponse)
		{
			case 'CSV':
			case 'XLS':
			case 'PDF':
				break;
			case 'OBJ':
				$this->renderObj();
				return null;
				break;
			default:
				return $this->renderHtml();
		}
	}
	
	
	/**
	 *
	 */
	public function renderObj () : void
	{
		if ($this->bEnable)
		{
			$this->prepare();
			$this->renderSearch();
			$this->renderFilter();
			
			if (is_array($this->aItens))
			{
				foreach ($this->aItens as $loIten)
				{
					if ($loIten instanceof Button || $loIten instanceof Dropdown)
					{
						$loIten->setArea('toolbar');
						$loIten->render(['action' => $this->oParent->get('sAction')]);
					}
				}
			}
			
			if (is_array($this->aItensDefault))
			{
				foreach ($this->aItensDefault as $loIten)
				{
					if ($loIten instanceof Button || $loIten instanceof Dropdown)
					{
						$loIten->setArea('toolbar');
						$loIten->render(['action' => $this->oParent->get('sAction')]);
					}
				}
			}
		}
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderHtml () : string
	{
		if ($this->bEnable)
		{
			$this->prepare();
			$lsSearch = $this->renderSearch();
			$lsFilter = $this->renderFilter();

			$lsToolbarLeft = '';
			$lsToolbarRight = '';
			
			if (is_array($this->aItens))
			{
				foreach ($this->aItens as $loIten)
				{
					if ($loIten instanceof Button || $loIten instanceof Dropdown)
					{
						$loIten->setArea('toolbar');
						$lsIten = $loIten->render(['action' => $this->oParent->get('sAction')]);
	
						if (!empty($lsIten))
						{
							$lsToolbarLeft .= $this->parseTemplate(
								$this->getGridObject()->getTemplate('toolbar', 'iten'),
								['button' => $lsIten],
							);
						}
					}
				}
			}
			
			if (is_array($this->aItensDefault))
			{
				foreach ($this->aItensDefault as $loIten)
				{
					if ($loIten instanceof Button || $loIten instanceof Dropdown)
					{
						$loIten->setArea('toolbar');
						$lsIten = $loIten->render(['action' => $this->oParent->get('sAction')]);
						
						if (!empty($lsIten))
						{
							$lsToolbarRight .= $this->parseTemplate(
								$this->getGridObject()->getTemplate('toolbar', 'iten'),
								['button' => $lsIten],
							);							
						}
					}
				}
			}

			return $this->parseTemplate(
				$this->getGridObject()->getTemplate('toolbar', 'nav'),
				[
					'id' => $this->sId,
					'left-btns' => $lsToolbarLeft,
					'right-btns' => $lsToolbarRight, 
					'search-form' => $lsSearch, 
					'filter-form' => $lsFilter
				]
			);
		}

		return '';
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function callFilter () : string
	{
		if (is_callable($this->cFilter))
		{
			$lcFilter = $this->cFilter;
			$lsFormFilter = $lcFilter($this);

			if (is_null($lsFormFilter))
			{
				$loFilterBtn = $this->getBtnDef('filterBtn');
				
				if ($loFilterBtn instanceof Button)
				{
					$loFilterBtn->setEnable(false);
				}
			}

			return (string)$lsFormFilter;
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderFilter () : string
	{
		$lsFormFilter = $this->callFilter();

		if ($this->bShowFilter)
		{
			$this->aFilter['submitBtn'] = new Button('filterSubmit', $this->sResource, $this);
			$this->aFilter['submitBtn']->setTitle('Filtrar')
				->setDescription(null)
				->setHref($this->oParent->get('sRoute') . $this->oParent->get('sAction') . '?' . $this->oParent->get('sGetParam'))
				->setClass('onion-grid-filter-submit ' . $this->getGridObject()->getTemplate('class', 'submit-filter'))
				->setTarget('action')
				->setIcon($this->getGridObject()->getTemplate('button-icons', 'submitBtn'));
			
			$lsSubmitBtn = $this->aFilter['submitBtn']->render();

			return $this->parseTemplate(
				$this->getGridObject()->getTemplate('toolbar', 'filter'),
				[
					'id' => "onion-grid-form-filter-area-{$this->oParent->get('sId')}",
					'form' => $lsFormFilter,
					'submit-btn' => $lsSubmitBtn
				]
			);
		}

		return '';
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderSearch () : string
	{
		if ($this->bShowSearch)
		{
			$lsQuery = $this->oParent->get('sSearchQuery');

			$this->aSearchRender['input'] = sprintf(
				'<input id="%1$s" name="q" type="text" data-act="%2$s" class=" %3$s" value="%4$s" />',
				"onion-grid-search-query-{$this->oParent->get('sId')}",
				"{$this->oParent->get('sRoute')}{$this->oParent->get('sAction')}?{$this->oParent->get('sGetParam')}",
				"onion-grid-search-query {$this->getGridObject()->getTemplate('class', 'input')}",
				$lsQuery
			);

			return $this->parseTemplate(
				$this->getGridObject()->getTemplate('toolbar', 'search'),
				[
					'id' => "onion-grid-form-search-area-{$this->oParent->get('sId')}",
					'dropdown' => $this->fieldsSearchSelect(),
					'input' => $this->aSearchRender['input'], 
					'submit-btn' => $this->searchButton()
				]
			);
		}

		return '';
	}
	
	
	/**
	 * 
	 * @return string|null
	 */
	public function searchButton () : ?string
	{
		$this->aSearchRender['submitSearchBtn'] = new Button('onion-grid-search-btn-' . $this->oParent->get('sId'), $this->sResource, $this);	
		$this->aSearchRender['submitSearchBtn']
			->setTitle(null)
			->setDescription(null)
			->setTarget('action')
			->setIcon($this->getGridObject()->getTemplate('button-icons', 'submitSearchBtn'))
			->setClass('onion-grid-search-btn ' . $this->getGridObject()->getTemplate('class', 'input-group'))
			->setHref($this->oParent->get('sRoute') . $this->oParent->get('sAction') . '?' . $this->oParent->get('sGetParam'));
		
		return $this->aSearchRender['submitSearchBtn']->render();
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function fieldsSearchSelect () : ?string
	{
		$laColums = $this->oParent->getColumn();
		
		if (is_array($laColums))
		{
			$loDropdown = new Dropdown('searchSelect', $this->sResource, $this);
			$loDropdown->setType('btnmenu')
				->setEnable($this->bShowSearch)
				->setTitle(GridText::text('sSearchFor'))
				->setDescription(null)
				->setIcon(null)
				->setClass($this->getGridObject()->getTemplate('class', 'input-group'))
				->setShowCaret(true);		
				
			foreach ($laColums as $loColum)
			{
				if ($loColum->isEnable() && $loColum->get('sType') == 'data' && !empty($loColum->get('sSearchField')) && ($loColum->isVisible() || $loColum->isSearchableIfHidden()))
				{
					$laSearchable = [];
					
					if ($loColum->isSearchable())
					{
						$laSearchable['checked'] = "checked";
					}
					
					$loDropdown->createIten($loColum->get('sId'))
						->setTitle($loColum->get('sTitle'))
						->setDescription(null)
						->setHref(null)
						->setParams($laSearchable)
						->setClass('onion-grid-fields-to-search')
						->setFormat(function ($poButton, $paParams) {
							$laBtnParams = $poButton->get('aParams');
							$lsSearchable = '';
							
							if (isset($laBtnParams['checked']))
							{
								$lsSearchable = 'checked="checked"';
							}
						
							$lsInput = $this->parseTemplate(
								$this->getGridObject()->getTemplate('toolbar', 'search-iten'),
								[
									'class' => 'onion-grid-search-field',
									'value' => $poButton->get('sId'),
									'searchable' => $lsSearchable,
									'label' => $poButton->get('sTitle')
								]
							);

							$poButton->setTitle($lsInput);
						});
				}
			}
			
			$this->aSearchRender['fieldSelect'] = $loDropdown;
			
			return $loDropdown->render();
		}

		return '';
	}


	public function set ($pmVar, $pmValue = null) : Toolbar
	{
		parent::set($pmVar, $pmValue);
		return $this;
	}


	public function setEnable (bool $pbEnable) : Toolbar
	{
		parent::setEnable($pbEnable);
		return $this;
	}


	public function setParent (?object $poParent = null) : Toolbar
	{
		$this->oParent = $poParent;
		return $this;
	}
	
	
	public function setResource (string $psResource) : Toolbar
	{
		parent::setResource($psResource);
		return $this;
	}

	
	public function setId (string $psId) : Toolbar
	{
		parent::setId($psId);
		return $this;
	}


	public function setName (?string $psName = null) : Toolbar
	{
		parent::setName($psName);
		return $this;
	}


	public function setTitle (?string $psTitle = null) : Toolbar
	{
		parent::setTitle($psTitle);	
		return $this;
	}


	public function setDescription (?string $psDescription = null) : Toolbar
	{
		parent::setDescription($psDescription);
		return $this;
	}


	public function setHelp (?string $psHelp = null) : Toolbar
	{
		$this->sHelp = $psHelp;
		
		return $this;
	}


	public function setIcon (?string $psIcon = null) : Toolbar
	{
		parent::setIcon($psIcon);
		return $this;
	}


	public function setPrepared (bool $pbPrepared) : Toolbar
	{
		parent::setPrepared($pbPrepared);
		return $this;
	}


	public function order (string $psItem, array $paOrder) : Toolbar
	{
		parent::order($psItem, $paOrder);
		return $this;
	}


	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : Toolbar
	{
		parent::add($psArrayObj, $poElement, $psIndex, $pnPosition, $pbNumericIndex);
		return $this;
	}


	public function remove (string $psArrayObj, string $psElementId) : Toolbar
	{
		parent::remove($psArrayObj, $psElementId);
		return $this;
	}
}