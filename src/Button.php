<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;
use OnionGrid\AbstractGrid;
use OnionLib\Str;


class Button extends AbstractGrid implements InterfaceGrid
{
	const TYPE = 'button';
	
	/**
	 * Request type: `HTTP` or `AJAX`
	 * 
	 * @var string
	 */
	protected $sRequestType = null;
	
	/**
	 * Open target: `_blank`, `_self`, `_top`, `_parent`, `popup`, `action`, `modal`, `mass-action`, `mass-popup`, `[window name]` 
	 *  
	 * @var string
	 */
	protected $sTarget = '';
	
	/**
	 * Url to the action
	 * 
	 * @var string
	 */
	protected $sHref = '#';
	
	/**
	 * 
	 * @var string
	 */
	protected $sRel = '';
	
	/**
	 * 
	 * @var string
	 */
	protected $sAlt = '';
	
	/**
	 * Params to pass for action
	 * 
	 * @var array
	 */
	protected $aParams = [];
	
	/**
	 * Type: `button` or `separator`
	 * 
	 * @var string
	 */
	protected $sType = 'button';

	/**
	 * Area: `toolbar`, `dowpdown`, `row` or `row-dropdown`
	 * 
	 * @var string
	 */
	protected $sArea = 'row';
	
	/**
	 * @var string
	 */
	protected $sClass = '';
	
	/**
	 * @var string
	 */
	protected $sStyle = '';

	/**
	 * Position: `left` or `right` 
	 * 
	 * @var string
	 */
	protected $sPosition = 'left';

	/**
	 * 
	 * @var callable
	 */
	protected $cBeforeRender = null;

	/**
	 * 
	 * @var callable
	 */
	protected $cFormat = null;
	
	/**
	 * 
	 * @var bool
	 */
	protected $bWithCaret = false;

	/**
	 * The button image
	 *
	 * @var string
	 */
	protected $sImg = null;

	/**
	 * Action route
	 * 
	 * @var string
	 */
	protected $sAction = null;

	/**
	 * If true, the button disabled will appear on layout, but whitout action
	 * 
	 * @var bool
	 */
	protected $bShowDisabledBtn = false;
			
	
	// Settings
	
	
	/**
	 * Construct an object setting the id, name and resource properties
	 * if the id is not given the construct will return an exception
	 *
	 * @param string $psId Instance identifier.
	 * @param string|null $psResource
	 * @param \OnionGrid\InterfaceGrid|null $poParent
	 * @throws \InvalidArgumentException
	 */	
	public function __construct (string $psId, ?string $psResource = null, ?InterfaceGrid $poParent = null)
	{
		parent::__construct($psId, $psResource, $poParent);
	}

	
	/**
	 * 
	 * @param array $paButtonProp
	 * @return \OnionGrid\Button
	 */
	public function factory (array $paButtonProp) : Button
	{
		if (is_array($paButtonProp))
		{
			foreach ($paButtonProp as $lsProperty => $lmValue)
			{
				$lsMethod = "set{$lsProperty}";

				if (method_exists($this, $lsMethod) && !is_null($lmValue))
				{
					$this->$lsMethod($lmValue);
				}
			}
		}

		return $this;
	}

	
	/**
	 *
	 * @param string $psAction
	 * @return \OnionGrid\Button
	 */
	public function setAction (string $psAction = null) : Button
	{
		if (!empty($psAction) || is_null($psAction))
		{
			$this->sAction = $psAction;
		}
	
		return $this;
	}
		
	
	/**
	 * Request type: `HTTP` or `AJAX`
	 * 
	 * @param string|null $psRequestType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button
	 */
	public function setRequestType (?string $psRequestType = null) : Button
	{
		if(strtoupper($psRequestType) === 'HTTP' || strtoupper($psRequestType) === 'AJAX' || is_null($psRequestType))
		{
			$this->sRequestType = $psRequestType;
		}
		else
		{
			throw new \InvalidArgumentException('The request value should be HTTP or AJAX!'); 	
		}
		
		return $this;
	}
	
	
	/**
	 * Open target: `_self`, `_blank`, `_top`, `_parent`, `popup`, `action`, `modal`, `mass-action`, `mass-popup`, `[window name]`
	 * 
	 * @param string|null $psTarget
	 * @return \OnionGrid\Button
	 */
	public function setTarget (?string $psTarget = null) : Button
	{
		$this->sTarget = $psTarget;
		
		return $this;
	}
	
	
	/**
	 * Type: `button` or `separator`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button
	 */
	public function setType (string $psType = 'button') : Button
	{
		switch(strtolower($psType))
		{
			case 'button':
			case 'separator':
				$this->sType = $psType;
				break;
				
			default:
				throw new \InvalidArgumentException('The type value should be button or separator!');
		}
		
		return $this;
	}

	
	/**
	 * Area: `toolbar`, `dropdown`, `row` or `row-dropdown`
	 *
	 * @param string $psArea
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button
	 */
	public function setArea (string $psArea = 'row') : Button
	{
		switch(strtolower($psArea))
		{
			case 'toolbar':
			case 'dropdown':
			case 'row':
			case 'row-dropdown':
				$this->sArea = $psArea;
				break;
				
			default:
				throw new \InvalidArgumentException('The area value should be toolbar, dropdown, row or row-dropdown!');
		}
		
		return $this;
	}	
	
	
	/**
	 * 
	 * @param bool $pbWithCaret
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button
	 */
	public function setWithCaret (bool $pbWithCaret) : Button
	{
		if (is_bool($pbWithCaret))
		{
			$this->bWithCaret = $pbWithCaret;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "WithCaret" property need to be a bool!');
		}
		
		return $this;
	}


	/**
	 *
	 * @param string|null $psImg        	
	 * @return \OnionGrid\Button
	 */
	public function setImg (?string $psImg = null) : Button
	{
		if (! empty($psImg))
		{
			$this->sImg = $psImg;
		}
		elseif ($psImg === null)
		{
			$this->sImg = null;
		}
		
		return $this;
	}

	
	/**
	 * Set extra params to action button
	 * ```
	 * array(
	 * 'data-url' => string,
	 * 'data-title' => string,
	 * 'data-btnMame' => string,
	 * 'data-btnDismiss' => string|bool,
	 * 'data-params' => string,
	 * 'data-value' => string,
	 * 'data-confirm' => string|bool,
	 * 'data-no-check' => string,
	 * 'data-msg' => string,
	 * 'data-folder' => string,
	 * 'data-folder-not' => string,
	 * 'data-cancelBtn' => 'no',
	 * 'data-header' => 'no',
	 * 'data-footer' => 'no',
	 * 'data-modal-dimension' => string
	 * );
	 * ```
	 * And much more, as your need
	 * 
	 * @param array $paParams
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button
	 */
	public function setParams (array $paParams) : Button
	{
		if (is_array($paParams))
		{
			$this->aParams = $paParams;
		}
		else 
		{
			throw new \InvalidArgumentException('The params value should be an array!');
		}	
			
		return $this;
	}
	
	
	/**
	 *
	 * @param string|null $psHref        	
	 * @return \OnionGrid\Button
	 */
	public function setHref (?string $psHref = null) : Button
	{
		if (!empty($psHref))
		{
			$this->sHref = $psHref;
			
			$laResources = explode(":", $this->sResource);
			
			if (count($laResources) == 2 || empty($this->sResource))
			{
				$lsResource = str_replace("/", ":", $psHref);
				
				if (substr($lsResource, 0, 1) == ":")
				{
					$lsResource = substr($lsResource, 1);
				}
				
				$this->setResource($lsResource);
			}
		}
		elseif ($psHref === null)
		{
			$this->sHref = null;
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psClass
	 * @return \OnionGrid\Button
	 */
	public function setClass (string $psClass = '') : Button
	{
		if (!empty($psClass))
		{
			$this->sClass = $psClass;
		}
	
		return $this;
	}	
	
	
	/**
	 *
	 * @param string $psStyle
	 * @return \OnionGrid\Button
	 */
	public function setStyle (string $psStyle = '') : Button
	{
		if (!empty($psStyle))
		{
			$this->sStyle = $psStyle;
		}
		
		return $this;
	}

	
	/**
	 * Iten position: `left` or `right`
	 *
	 * @param string|null $psPosition
	 * @return \OnionGrid\Button
	 */
	public function setPosition (?string $psPosition = null) : Button
	{
		$laOptions = [
				'left' => 1,
				'right' => 1,
		];
		
		if(isset($laOptions[strtolower($psPosition)]))
		{
			$this->sPosition = $psPosition;
		}
		else
		{
			throw new \InvalidArgumentException('Button position error, try: left or right');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psRel
	 * @return \OnionGrid\Button
	 */
	public function setRel (string $psRel = '') : Button
	{
		if (!empty($psRel))
		{
			$this->sRel= $psRel;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psAlt
	 * @return \OnionGrid\Button
	 */
	public function setAlt (string $psAlt = '') : Button
	{
		if (!empty($psAlt))
		{
			$this->sAlt= $psAlt;
		}
		
		return $this;
	}
	
	
	/**
	 * Define if the application is enable to run
	 *
	 * @param bool $pbShow        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Button
	 */
	public function setShowDisabledBtn (bool $pbShow) : Button
	{
		if (is_bool($pbShow))
		{
			$this->bShowDisabledBtn = $pbShow;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "showDesabledBtn" property need to be a bool!');
		}
		
		return $this;
	}


	/**
	 * ```
	 * setBeforeRender(function (\OnionGrid\Button $poButton, array $paParams) : void);
	 * ```
	 * 
	 * @param callable|null $pcBeforeRender
	 * @return \OnionGrid\Button
	 */
	public function setBeforeRender (?callable $pcBeforeRender = null) : Button
	{
		$this->cBeforeRender = $pcBeforeRender;
	
		return $this;
	}

	
	/**
	 * ```
	 * setFormat(function (\OnionGrid\Button $poButton, array $paParams) : void);
	 * ```
	 * @param callable|null $pcFormat
	 * @return \OnionGrid\Button
	 */
	public function setFormat (?callable $pcFormat = null) : Button
	{
		$this->cFormat = $pcFormat;
		
		return $this;
	}
	

	/**
	 *
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function getClearProperties (bool $pbClearProperty = false) : array
	{
		$laProperties = parent::getClearProperties($pbClearProperty);
		
		if ($pbClearProperty)
		{
			unset($laProperties['cBeforeRender']);
			unset($laProperties['cFormat']);
		}
		
		return $laProperties;
	}
	
	
	/**
	 * 
	 */
	public function aclIsAllowed () : void
	{
		$loGrid = $this->getGridObject();

		if (!is_null($loGrid) && $loGrid::TYPE == 'grid')
		{
			$lcAclCheckIfIsAllowed = $loGrid->getAclCheck();

			if (is_callable($lcAclCheckIfIsAllowed) && !is_null($this->sResource) && !is_null($this->sAction))
			{
				if (!$lcAclCheckIfIsAllowed($this->sResource, $this->sAction))
				{
					$this->setEnable(false);
				}
			}
		}
	}


	/**
	 * 
	 * @param array $paParams
	 * @return \OnionGrid\Button
	 */
	public function prepare (array $paParams = []) : Button
	{
		if (!$this->isPrepared())
		{
			if (is_callable($this->cBeforeRender))
			{
				$lfCallBack = $this->cBeforeRender;
				$lfCallBack($this, $paParams);
			}

			$laParams = $this->get('aParams');
			
			if (isset($laParams['data-folder']) && !empty(trim($laParams['data-folder'])) && $this->isEnable())
			{
				$this->setEnable(false);
				$laFolders = explode(",", $laParams['data-folder']);
				
				foreach ($laFolders as $lsFolder)
				{
					if (!isset($paParams['action']) || trim($lsFolder) == $paParams['action'])
					{
						$this->setEnable(true);
						continue;
					}
				}
			}
			elseif (isset($laParams['data-folder-not']) && !empty(trim($laParams['data-folder-not'])) && $this->isEnable())
			{
				$laFolders = explode(",", $laParams['data-folder-not']);
				
				foreach ($laFolders as $lsFolder)
				{
					if (!isset($paParams['action']) || trim($lsFolder) == $paParams['action'])
					{
						$this->setEnable(false);
						continue;
					}
				}
			}
			
			if ($this->sTarget == 'grid-modal')
			{
				$this->sClass = Str::appendStr($this->sClass, "onion-grid-modal-btn onion-grid-{$this->sArea}-modal-btn onion-grid-{$this->sArea}-modal-btn-{$this->sId}");
				$this->aParams['data-act'] = $this->sHref;
				$this->setHref('#');
			}
			elseif ($this->sTarget == 'modal')
			{
				$this->sClass = Str::appendStr($this->sClass, "onion-grid-modal-btn onion-grid-{$this->sArea}-modal-btn onion-grid-{$this->sArea}-modal-btn-{$this->sId}");
				$this->aParams['data-act'] = $this->sHref;
				$this->setHref('#');
			}
			elseif ($this->sTarget == 'popup')
			{
				$this->sClass = Str::appendStr($this->sClass, "openPopUpBtn onion-grid-{$this->sArea}-popup-btn onion-grid-{$this->sArea}-popup-btn-{$this->sId}");
				$this->aParams['data-url'] = $this->sHref;
				$this->setHref('javascript:return false;');
				
				if (!isset($this->aParams['data-wwidth']))
				{
					$this->aParams['data-wwidth'] = "80%";
				}
				
				if (!isset($this->aParams['data-wheight']))
				{
					$this->aParams['data-wheight'] = "80%";
				}
			}
			elseif ($this->sTarget == 'action')
			{
				$this->sClass = Str::appendStr($this->sClass, "onion-grid-act-btn onion-grid-{$this->sArea}-act-btn onion-grid-{$this->sArea}-act-btn-{$this->sId}");
				$this->aParams['data-act'] = $this->sHref;
				$this->setHref('#');
			}
			elseif ($this->sTarget == 'mass-action')
			{
				$this->sClass = Str::appendStr($this->sClass, "onion-grid-mass-act-btn onion-grid-{$this->sArea}-mass-act-btn onion-grid-{$this->sArea}-mass-act-btn-{$this->sId}");
				$this->aParams['data-act'] = $this->sHref;
				$this->setHref('#');
			}
			elseif ($this->sTarget == 'mass-popup')
			{
				$this->sClass = Str::appendStr($this->sClass, "onion-grid-mass-act-popup-btn onion-grid-{$this->sArea}-mass-act-popup-btn onion-grid-{$this->sArea}-mass-act-popup-btn-{$this->sId}");
				$this->aParams['data-url'] = $this->sHref;
				$this->setHref('#');
			}
			else
			{
				$this->sClass = Str::appendStr($this->sClass, "onion-grid-link-btn onion-grid-{$this->sArea}-link-btn onion-grid-{$this->sArea}-link-btn-{$this->sId}");
			}
	
			if (is_callable($this->cFormat))
			{
				$lfCallBack = $this->cFormat;
				$lfCallBack($this, $paParams);
			}
			
			$this->aclIsAllowed();

			$this->setPrepared(true);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function getResponseType () : string
	{
		return $this->oParent->getResponseType();
	}
	
	
	/**
	 *
	 * @param array $paParams
	 * @return string|null
	 */
	public function render (array $paParams = []) : ?string
	{
		$lsResponse = $this->getResponseType();
		
		switch ($lsResponse)
		{
			case 'CSV':
			case 'XLS':
			case 'PDF':
				return '';
				break;
			case 'OBJ':
				$this->renderObj($paParams);
				return null;
				break;
			default:
				return $this->renderHtml($paParams);
		}
	}
	
	
	/**
	 *
	 * @param array $paParams
	 */
	public function renderObj (array $paParams = []) : void
	{
		$this->prepare($paParams);
		
		if ($this->bEnable)
		{
			if (isset($paParams['row']) && is_array($paParams['row']))
			{
				$laPattern = [];
				
				foreach ($paParams['row'] as $lsField => $lmValue)
				{
					$laPattern[] = "/#%{$lsField}%#/";
				}
				
				$this->sId = preg_replace($laPattern, $paParams['row'], $this->sId);
				$this->sName = preg_replace($laPattern, $paParams['row'], $this->sName);
				$this->sIcon = preg_replace($laPattern, $paParams['row'], $this->sIcon);
				$this->sImg = preg_replace($laPattern, $paParams['row'], $this->sImg);
				$this->sTitle = preg_replace($laPattern, $paParams['row'], $this->sTitle);
				$this->sDescription = preg_replace($laPattern, $paParams['row'], $this->sDescription);
				$this->sClass = preg_replace($laPattern, $paParams['row'], $this->sClass);
				$this->sStyle = preg_replace($laPattern, $paParams['row'], $this->sStyle);
				$this->sHref = preg_replace($laPattern, $paParams['row'], $this->sHref);
				$this->sAlt = preg_replace($laPattern, $paParams['row'], $this->sAlt);
				$this->sRel = preg_replace($laPattern, $paParams['row'], $this->sRel);
				
				//Debug::display($this->aParams);
				$this->aParams = preg_replace($laPattern, $paParams['row'], $this->aParams);
				
				//Debug::display($this->aParams);
			}
		}
	}
	
	
	/**
	 * 
	 * @param array $paParams
	 * @return string
	 */
	public function renderHtml (array $paParams = []) : string
	{
		$this->prepare($paParams);
		
		$lsParams = '';
		
		if (is_array($this->aParams))
		{
			foreach ($this->aParams as $lsName => $lmValue)
			{
				if (is_array($lmValue))
				{
					$lsValue = json_encode($lmValue);
					$lsParams .= "{$lsName}='{$lsValue}'";
				}
				else
				{
					$lsParams .= "{$lsName}=\"{$lmValue}\" ";
				}
			}
		}
		
		$lsIcon = $this->renderIcon($this->sIcon, $this->bWithCaret);

		if (!empty($this->sImg) && empty($lsIcon))
		{
			if (preg_match("/.*<img .* src=.*>.*/", $this->sImg))
			{
				$lsIcon = $this->sImg;
			}
			else
			{
				$lsIcon = $this->parseTemplate(
					$this->getGridObject()->getTemplate('button', 'img'), 
					['img' => $this->sImg]
				);
			}

			if ($this->bWithCaret)
			{
				$lsIcon .= $this->getGridObject()->getTemplate('button', 'caret');
			}
		}
		
		$lsHref = '';
		
		if ($this->sHref !== null)
		{
			$lsHref = 'href="' . $this->sHref . '"';
		}
		
		$lsRequestType = '';
		
		if ($this->sRequestType !== null)
		{
			$lsRequestType = 'data-request="' . $this->sRequestType . '"';
		}
		
		switch ($this->sTarget)
		{
			case 'action':
			case 'grid-modal':
			case 'modal':
			case 'popup':
			case 'mass-action':
			case 'mass-popup':
				$lsTarget = "";
				break;
			default:
				$lsTarget = 'target="' . $this->sTarget . '"';
		}
		
		$lsButton = '';
		
		if ($this->bEnable || $this->bShowDisabledBtn)
		{
			if ($this->sType == 'button')
			{
				$lsId = $this->sId;

				if ($this->sArea == 'row' || $this->sArea == 'row-dropdown')
				{
					$lsId .= "-#%id%#";
				}

				if (!$this->bEnable)
				{
					$this->sClass .= ' disabled';
				}

				$lsButton = $this->parseTemplate(
					$this->getGridObject()->getTemplate('button', "on-{$this->sArea}"),
					[
						'id' => "onion-grid-btn-{$lsId}",
						'parent-id' => $this->sId,
						'class' => $this->sClass,
						'style' => $this->sStyle,
						'desc' => $this->sDescription,
						'rel' => $this->sRel,
						'alt' => $this->sAlt,
						'reqType' => $lsRequestType,
						'href' => $this->bEnable ? $lsHref : '',
						'target' => $lsTarget,
						'params' => $lsParams,
						'icon' => $lsIcon,
						'label' => $this->sTitle
					]
				);
			}
			elseif ($this->sType == 'separator')
			{
				$lsButton = $this->getGridObject()->getTemplate('button', 'separator');
			}
			
			if (isset($paParams['row']) && is_array($paParams['row']))
			{
				$laPattern = [];
				
				foreach ($paParams['row'] as $lsField => $lmValue)
				{
					$laPattern[] = "/#%{$lsField}%#/";
				}
				
				$lsButton = preg_replace($laPattern, $paParams['row'], $lsButton);
			}
		}
		
		return $lsButton;
	}


	public function set ($pmVar, $pmValue = null) : Button
	{
		parent::set($pmVar, $pmValue);
		return $this;
	}


	public function setEnable (bool $pbEnable) : Button
	{
		parent::setEnable($pbEnable);
		return $this;
	}


	public function setParent (?object $poParent = null) : Button
	{
		$this->oParent = $poParent;
		return $this;
	}
	
	
	public function setResource (string $psResource) : Button
	{
		parent::setResource($psResource);
		return $this;
	}

	
	public function setId (string $psId) : Button
	{
		parent::setId($psId);
		return $this;
	}


	public function setName (?string $psName = null) : Button
	{
		parent::setName($psName);
		return $this;
	}


	public function setTitle (?string $psTitle = null) : Button
	{
		parent::setTitle($psTitle);	
		return $this;
	}


	public function setDescription (?string $psDescription = null) : Button
	{
		parent::setDescription($psDescription);
		return $this;
	}


	public function setHelp (?string $psHelp = null) : Button
	{
		$this->sHelp = $psHelp;
		
		return $this;
	}


	public function setIcon (?string $psIcon = null) : Button
	{
		parent::setIcon($psIcon);
		return $this;
	}


	public function setPrepared (bool $pbPrepared) : Button
	{
		parent::setPrepared($pbPrepared);
		return $this;
	}


	public function order (string $psItem, array $paOrder) : Button
	{
		parent::order($psItem, $paOrder);
		return $this;
	}


	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : Button
	{
		parent::add($psArrayObj, $poElement, $psIndex, $pnPosition, $pbNumericIndex);
		return $this;
	}


	public function remove (string $psArrayObj, string $psElementId) : Button
	{
		parent::remove($psArrayObj, $psElementId);
		return $this;
	}	
}