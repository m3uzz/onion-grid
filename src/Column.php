<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;
use OnionGrid\AbstractGrid;
use OnionGrid\GridText;
use OnionGrid\InterfaceGrid;
use OnionLib\Util;

class Column extends AbstractGrid implements InterfaceGrid
{
	const TYPE = 'column';

	/**
	 * 
	 * @var object
	 */
	protected $oReferer = null;

	/**
	 * Can be: `data`, `img`, `checkbox`, `radio`, `actions`
	 *
	 * @var string
	 */
	protected $sType = 'data';

	/**
	 *
	 * @var string
	 */
	protected $sOrdering = '';
	
	/**
	 *
	 * @var bool
	 */
	protected $bSortable = false;

	/**
	 *
	 * @var bool
	 */
	protected $bSearchable = false;
	
	/**
	 * 
	 * @var string
	 */
	protected $sSearchField = '';
		
	/**
	 *
	 * @var bool
	 */
	protected $bSearchableIfHidden = true;

	/**
	 *
	 * @var bool
	 */
	protected $bVisible = true;

	/**
	 *
	 * @var bool
	 */
	protected $bResizable = true;

	/**
	 * 
	 * @var string
	 */
	protected $sTitleAlign = 'left';

	/**
	 *
	 * @var bool
	 */
	protected $bTitleShow = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bEditable = false;
		
	/**
	 *
	 * @var string
	 */
	protected $sClass = '';
	
	/**
	 *
	 * @var string|int
	 */
	protected $mWidth = null;
	
	/**
	 *
	 * @var string|int
	 */
	protected $mOriginalWidth = null;
	
	/**
	 *
	 * @var string
	 */
	protected $sAlign = 'left';
	
	/**
	 *
	 * @var string
	 */
	protected $sColor = '';

	/**
	 *
	 * @var string
	 */
	protected $sBackground = '';
	
	/**
	 * 
	 * @var bool
	 */
	protected $bShowValue = true;
	
	/**
	 * 
	 * @var array
	 */
	protected $aParams = [];

	/**
	 * 
	 * @var callable
	 */
	protected $cBeforeRender = null;

	/**
	 *
	 * @var callable
	 */
	protected $cFormat = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $nRowNumber;

	/**
	 * 
	 * @var string
	 */
	protected $sValue = null;
	
	/**
	 * Row action
	 *
	 * @var array of \OnionGrid\Dropdown | \OnionGrid\Button
	 */
	protected $aActions = null;
	
	
	// Settings
	
	
	/**
	 * Construct an object setting the id, name and resource properties
	 * if the id is not given the construct will return an exception
	 *
	 * @param string $psId Instance identifier.
	 * @param string|null $psResource
	 * @param \OnionGrid\InterfaceGrid|null $poParent
	 * @throws \InvalidArgumentException
	 */	
	public function __construct (string $psId, ?string $psResource = null, ?InterfaceGrid $poParent = null)
	{
		parent::__construct($psId, $psResource, $poParent);
	}


	/**
	 * 
	 * @param array $paColumnProp
	 * @return \OnionGrid\Column
	 */
	public function factory (array $paColumnProp) : Column
	{
		if (is_array($paColumnProp))
		{
			foreach ($paColumnProp as $lsProperty => $lmValue)
			{
				$lsMethod = "set{$lsProperty}";

				if (method_exists($this, $lsMethod) && !is_null($lmValue))
				{
					$this->$lsMethod($lmValue);
				}
			}
		}

		return $this;
	}


	/**
	 * 
	 * @param array $paDropdownsProp
	 * @return \OnionGrid\Column
	 */
	public function setDropdowns (array $paDropdownsProp) : Column
	{
		if (is_array($paDropdownsProp))
		{
			$laDropdowns = $this->getDropdown();

			foreach ($laDropdowns as $loDropdown)
			{
				if (isset($paOptionsProp[$loDropdown->getId()]))
				{
					$laProperties = $paOptionsProp[$loDropdown->getId()];

					if (is_array($laProperties))
					{
						$loDropdown->factory($laProperties);
					}
				}
			}
		}

		return $this;
	}
	

	/**
	 * 
	 * @param object|null $poReferer
	 * @return \OnionGrid\Column
	 */
	public function setReferer (?object $poReferer = null) : Column
	{
		$this->oReferer = $poReferer;

		return $this;
	}
	

	/**
	 * 
	 * @param int $pnRowNumber
	 * @return \OnionGrid\Column
	 */
	public function setRowNumber (int $pnRowNumber) : Column
	{
		$this->nRowNumber = $pnRowNumber;

		return $this;
	}


	/**
	 *
	 * @param string|int|null $pmValue
	 * @return \OnionGrid\Column
	 */
	public function setValue ($pmValue = null) : Column
	{
		$this->sValue = null;

		if ($pmValue !== null)
		{
			$this->sValue = (string)$pmValue;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psOrdering
	 * @return \OnionGrid\Column
	 */
	public function setOrdering (string $psOrdering = '') : Column
	{
		if (!empty($psOrdering))
		{
			$this->sOrdering = $psOrdering;
		}
	
		return $this;
	}
		
	
	/**
	 *
	 * @param bool $pbSortable        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setSortable (bool $pbSortable = false) : Column
	{
		if (is_bool($pbSortable))
		{
			$this->bSortable = $pbSortable;
		}
		else
		{
			throw new \InvalidArgumentException('The sortable value should be a bool!');
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param bool $pbSearchable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setSearchable (bool $pbSearchable = false) : Column
	{
		if (is_bool($pbSearchable))
		{
			$this->bSearchable = $pbSearchable;
			
			if (empty($this->sSearchField) && $this->bSearchable)
			{
				$this->sSearchField = $this->sId;
			}
		}
		else
		{
			throw new \InvalidArgumentException('The searchable value should be a bool!');
		}
	
		return $this;
	}
	
	
	/**
	 * 
	 * @param string|null $psField
	 * @return \OnionGrid\Column
	 */
	public function setSearchField (?string $psField = null) : Column
	{

		if (!empty($psField))
		{
			$this->bSearchable = true;
			$this->sSearchField = $psField;
		}
		
		return $this;
	}
		
	
	/**
	 *
	 * @param bool $pbSearch        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setSearchableIfHidden (bool $pbSearcheable = true) : Column
	{
		if (is_bool($pbSearcheable))
		{
			$this->bSearchableIfHidden = $pbSearcheable;
		}
		else
		{
			throw new \InvalidArgumentException('The SearchIfHidden value should be a bool!');
		}
		
		return $this;
	}


	/**
	 *
	 * @param bool $pbVisible        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setVisible (bool $pbVisible = true) : Column
	{
		if (is_bool($pbVisible))
		{
			$this->bVisible = $pbVisible;
		}
		else
		{
			throw new \InvalidArgumentException('The visible value should be a bool!');
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param bool $pbResizable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setResizable (bool $pbResizable = false) : Column
	{
		if (is_bool($pbResizable))
		{
			$this->bResizable = $pbResizable;
		}
		else
		{
			throw new \InvalidArgumentException('The resizable value should be a bool!');
		}
	
		return $this;
	}


	/**
	 * Values accepted for $psAlign: `left`, `right` or `center`
	 *
	 * @param string $psAlign
	 * @return \OnionGrid\Column
	 */
	public function setTitleAlign (string $psAlign = 'left') : Column
	{
		$psAlign = strtolower($psAlign);
		
		if ($psAlign == 'left' || $psAlign == 'right' || $psAlign == 'center')
		{
			$this->sTitleAlign = $psAlign;
		}
		else
		{
			throw new \InvalidArgumentException('TitleAlign option error, try: left, right or center!');
		}
		
		return $this;
	}	
	
	
	/**
	 * Specify if a grid value is editable.
	 * Default is false.
	 * If true, it needs to receive extra params:
	 * ```
	 * setParam(array(
	 * 		'data-url' => 'url to save individual field', 
	 * 		'data-toUpper' => 'false' //if true all caracter will turn to upper case. Default is true
	 * ));
	 * ```
	 * 
	 * @param bool $pbEditable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setEditable (bool $pbEditable = false) : Column
	{
		if (is_bool($pbEditable))
		{
			$this->bEditable = $pbEditable;
		}
		else
		{
			throw new \InvalidArgumentException('The editable value should be a bool!');
		}
	
		return $this;
	}	
	
	
	/**
	 *
	 * @param bool $pbShowValue
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setShowValue (bool $pbShowValue = false) : Column
	{
		if (is_bool($pbShowValue))
		{
			$this->bShowValue = $pbShowValue;
		}
		else
		{
			throw new \InvalidArgumentException('The show value value should be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psClass        	
	 * @return \OnionGrid\Column
	 */
	public function setClass (string $psClass = '') : Column
	{
		if (!empty($psClass))
		{
			$this->sClass = $psClass;
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string|int $pmWidth        	
	 * @return \OnionGrid\Column
	 */
	public function setWidth ($pmWidth = '') : Column
	{
		$this->mWidth = $pmWidth;

		if ($this->mOriginalWidth == null)
		{
			$this->mOriginalWidth = $pmWidth;
		}
		
		return $this;
	}

	
	/**
	 * Values accepted for $psAlign: `left`, `right` or `center`
	 *
	 * @param string $psAlign
	 * @return \OnionGrid\Column
	 */
	public function setAlign (string $psAlign = 'left') : Column
	{
		$psAlign = strtolower($psAlign);
		
		if ($psAlign == 'left' || $psAlign == 'right' || $psAlign == 'center')
		{
			$this->sAlign = $psAlign;
		}
		else
		{
			throw new \InvalidArgumentException('Align option error, try: left, right or center!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psColor
	 * @return \OnionGrid\Column
	 */
	public function setColor (string $psColor = '') : Column
	{
		$this->sColor = $psColor;
	
		return $this;
	}

	
	/**
	 *
	 * @param string $psBackground
	 * @return \OnionGrid\Column
	 */
	public function setBackground (string $psBackground = '') : Column
	{
		$this->sBackground = $psBackground;
	
		return $this;
	}	
	
	
	/**
	 * Values accepted for $psType: `checkbox`, `radio`, `data`, `img` or `actions`
	 *
	 * @param string $psType
	 * @return \OnionGrid\Column
	 */
	public function setType (string $psType = 'data') : Column
	{
		$psType = strtolower($psType);
		
		if ($psType == 'checkbox' || $psType == 'radio' || $psType == 'data' || $psType == 'img' || $psType == 'actions')
		{
			$this->sType = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Type option error, try: checkbox, radio, data, img or actions!');
		}
		
		return $this;
	}
	
	
	/**
	 * Set extra params.
	 * ```
	 * array(
	 * 'data-url' => string,
	 * 'data-toUpper' => bool,
	 * 'data-pdfw' => float
	 * );
	 * ```
	 * @param array $paParams
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setParams (array $paParams) : Column
	{
		if (is_array($paParams))
		{
			$this->aParams = $paParams;
		}
		else
		{
			throw new \InvalidArgumentException('The params value should be an array!');
		}
		
		return $this;
	}
	

	/**
	 * ```
	 * setBeforeRender(function (\OnionGrid\Column $poColumn, array $paRow) : string);
	 * ```
	 * 
	 * @param callable|null $pcBeforeRender
	 * @return \OnionGrid\Column
	 */
	public function setBeforeRender (?callable $pcBeforeRender = null) : Column
	{
		$this->cBeforeRender = $pcBeforeRender;
	
		return $this;
	}

	
	/**
	 * ```
	 * setFormat(function (\OnionGrid\Column $poColumn, array $paRow) : string);
	 * ```
	 * 
	 * @param callable|null $pcFormat
	 * @return \OnionGrid\Column
	 */
	public function setFormat (?callable $pcFormat = null) : Column
	{
		$this->cFormat = $pcFormat;
	
		return $this;
	}
	
	
	// Action methods
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isVisible () : bool
	{
		return $this->bVisible;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isResizable () : bool
	{
		return $this->bResizable;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isEditable () : bool
	{
		return $this->bEditable;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isSearchable () : bool
	{
		return $this->bSearchable;
	}
		
	
	/**
	 * 
	 * @return bool
	 */
	public function isSearchableIfHidden () : bool
	{
		return $this->bSearchableIfHidden;
	}

	
	/**
	 * 
	 * @return bool
	 */
	public function isSortable () : bool
	{
		return $this->bSortable;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isShowValue () : bool
	{
		return $this->bShowValue;
	}
	
	
	/**
	 * Remove an action Button or Dropdown from the array object
	 *
	 * @param string $psActionId
	 * @return \OnionGrid\Column
	 */
	public function removeAction (string $psActionId) : Column
	{
		return parent::remove('aActions', $psActionId);
	}
	
	
	/**
	 * Load the Button or Dropdown object from array object
	 * or the entire array if $psActionId = null
	 *
	 * @param string|null $psActionId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Button|\OnionGrid\Dropdown|array|null
	 */
	public function getAction (?string $psActionId = null, bool $pbValid = true)
	{
		return parent::getElement('aActions', $psActionId, $pbValid);
	}
	
	
	/**
	 * Create a new Button object into the array object
	 * and setting its id and name
	 *
	 * @param string $psButtonId
	 * @return \OnionGrid\Button
	 */
	public function createButton (string $psButtonId) : Button
	{
		$loButton = new Button($psButtonId, $this->sResource, $this);
		$this->aActions[] = $loButton;
		
		return $loButton;
	}
	
	
	/**
	 * Add an existent Button object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Button $poButton
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function addButton (Button $poButton, ?string $psIndex = null, ?int $pnPosition = null) : Column
	{
		if ($poButton instanceof Button)
		{
			return parent::add('aActions', $poButton, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poButton should be a instance of \OnionGrid\Button!');
		}
	}
	
	
	/**
	 * Remove a Button from the array object
	 *
	 * @param string $psButtonId
	 * @return \OnionGrid\Column
	 */
	public function removeButton (string $psButtonId) : Column
	{
		return parent::remove('aActions', $psButtonId);
	}
	
	
	/**
	 * Load the Button object from array object
	 * or the entire array if $psButtonId = null
	 *
	 * @param string|null $psButtonId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Button|array|null
	 */
	public function getButton (?string $psButtonId = null, bool $pbValid = true)
	{
		$lmButton = parent::getElement('aActions', $psButtonId, $pbValid);
		
		if ($lmButton instanceof Button)
		{
			return $lmButton;
		}
		elseif (is_array($lmButton))
		{
			$laButtons = null;
			
			foreach ($lmButton as $lnKey => $loButton)
			{
				if ($loButton instanceof Button)
				{
					$laButtons[$lnKey] = $loButton;
				}
			}
			
			return $laButtons;
		}
		
		return null;
	}
	
	
	/**
	 * Create a new Dropdown object into the array object
	 * and setting its id and name
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Dropdown
	 */
	public function createDropdown (string $psDropdownId) : Dropdown
	{
		$loDropdown = new Dropdown($psDropdownId, $this->sResource, $this);
		$this->aActions[] = $loDropdown;
		
		return $loDropdown;
	}
	
	
	/**
	 * Add an existent Dropdown object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Dropdown $poDropdown
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function addDropdown (Dropdown $poDropdown, ?string $psIndex = null, ?int $pnPosition = null) : Column
	{
		if ($poDropdown instanceof Dropdown)
		{
			return parent::add('aActions', $poDropdown, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poDropdown should be a instance of \OnionGrid\Dropdown!');
		}
	}
	
	
	/**
	 * Remove an Dropdown from the array object
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Column
	 */
	public function removeDropdown (string $psDropdownId) : Column
	{
		return parent::remove('aActions', $psDropdownId);
	}
	
	
	/**
	 * Load the Dropdown object from array object
	 * or the entire array if $psDropdownId = null
	 *
	 * @param string $psDropdownId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Dropdown|array|null
	 */
	public function getDropdown (string $psDropdownId = null, bool $pbValid = true)
	{
		$lmDropdown = parent::getElement('aActions', $psDropdownId, $pbValid);
		
		if ($lmDropdown instanceof Dropdown)
		{
			return $lmDropdown;
		}
		elseif (is_array($lmDropdown))
		{
			foreach ($lmDropdown as $lnKey => $loDropdown)
			{
				if ($loDropdown instanceof Dropdown)
				{
					$laDropdowns[$lnKey] = $loDropdown;
				}
			}
			
			return $laDropdowns;
		}
		
		return null;
	}


	/**
	 * 
	 * @param mixed $pmDefault
	 * @param string $psType
	 * @return mixed
	 */
	public function getValue (mixed $pmDefault = '', ?string $psType = null) : mixed
	{
		// não utilizar !empty
		if (!is_null($this->sValue))
		{
			switch ($psType)
			{
				case 'bool':
					return Util::toBoolean($this->sValue);
				case 'int':
					return (int)$this->sValue;			
				case 'float':
					return (float)$this->sValue;
				case 'array':
					return json_decode((string)$this->sValue, true);
				case 'string':
					return (string)$this->sValue;
				default:
					return $this->sValue;
			}
		}

		return $pmDefault;
	}

	
	/**
	 * 
	 * @return string
	 */
	public function getResponseType () : string
	{
		return $this->oParent->getResponseType();
	}
	
	
	/**
	 *
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function getClearProperties (bool $pbClearProperty = false) : array
	{
		$laProperties = parent::getClearProperties($pbClearProperty);
		
		if ($pbClearProperty)
		{
			unset($laProperties['cBeforeRender']);
			unset($laProperties['cFormat']);
		}
		
		return $laProperties;
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function renderHeader () : ?string
	{
		if (is_callable($this->cBeforeRender))
		{
			$lfCallBack = $this->cBeforeRender;
			$lfCallBack($this);
		}
		
		$lsResponse = $this->getResponseType();
		
		switch ($lsResponse)
		{
			case 'CSV':
				return $this->renderHeaderCsv();
				break;
			case 'XLS':
				return $this->renderHeaderXls();
				break;
			case 'PDF':
				return $this->renderHeaderPdf();
				break;
			case 'OBJ':
				$this->renderHeaderObj();
				return null;
				break;
			default:
				return $this->renderHeaderHtml();
		}
	}

	
	/**
	 *
	 * @return string
	 */
	public function renderHeaderCsv () : string
	{
		if ($this->isEnable() && ($this->sType == 'data' || $this->sType == 'img'))
		{
			$lsTitle = !is_null($this->sTitle) ? $this->sTitle : '';
			return $this->oParent->get('sTextDelimiterCSV') . strtoupper($lsTitle) . $this->oParent->get('sTextDelimiterCSV') . $this->oParent->get('sSeparateValueCSV');
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderHeaderXls () : string
	{
		if ($this->isEnable() && ($this->sType == 'data' || $this->sType == 'img'))
		{
			$lsTitle = !is_null($this->sTitle) ? $this->sTitle : '';
			return '<th bgColor="#666666" style="color:white"><strong>' . strtoupper($lsTitle) . '</strong></th>';
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderHeaderPdf () : string
	{
		if ($this->isEnable() && $this->isVisible() && ($this->sType == 'data' || $this->sType == 'img'))
		{
			return is_null($this->sTitle) ? '' : $this->sTitle;
		}
		
		return '';
	}
	
	
	/**
	 *
	 */
	public function renderHeaderObj () : void
	{
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderHeaderHtml () : string
	{
		$lsGridHeader = '';

		if ($this->isEnable() && $this->isVisible())
		{
			$lsCol = '';
			
			$lsHiddenPrint = '';
			
			$lsResizable = ($this->isResizable() ? $this->getGridObject()->getTemplate('class', "resizable") : '');
			$lsAlign = (!empty($this->sTitleAlign) ? $this->getGridObject()->getTemplate('class', "align-{$this->sTitleAlign}") : '');
			
			$lsIcon = $this->renderIcon($this->sIcon);
			
			if ($this->sType == 'data' || $this->sType == 'img')
			{
				if ($this->isSortable())
				{
					$lsOrder = 'ASC';
					$lsClass = '';
					$lsCaret = '';

					$laColOrder = $this->oParent->get('aColOrder');
					
					if (is_array($laColOrder))
					{
						foreach ($laColOrder as $lsCol => $lsOrd)
						{
							if ($lsCol == $this->sId || $lsCol == $this->sOrdering)
							{
								$lsOrder = 'ASC';
								$lsClass = '';
								$lsCaret = '';

								if ($lsOrd == 'ASC')
								{
									$lsOrder = 'DESC';
									$lsCaret = $this->getGridObject()->getTemplate('table', 'thead-caret');

								}
								elseif ($lsOrd == 'DESC')
								{
									$lsOrder = '';
									$lsClass = ' dropup';
									$lsCaret = $this->getGridObject()->getTemplate('table', 'thead-caret');
								}
							}		
						}
					}

					$lsCol = $this->parseTemplate(
						$this->getGridObject()->getTemplate('table', 'thead-link'),
						[
							'title-class' => "onion-grid-col-order",
							'class' => $lsClass,
							'act' => "{$this->oParent->get('sRoute')}{$this->oParent->get('sAction')}?{$this->oParent->get('sGetParam')}",
							'id' => $this->sId,
							'col' => $this->sOrdering,
							'ord' => $lsOrder,
							'desc' => $this->sDescription,
							'title' => $lsIcon . ($this->bTitleShow ? $this->sTitle : ''),
							'caret' => $lsCaret
						]
					);
				}
				else
				{
					$lsCol = $this->parseTemplate(
						$this->getGridObject()->getTemplate('table', 'thead-static'),
						[
							'desc' => $this->sDescription,
							'title' => $lsIcon . ($this->bTitleShow ? $this->sTitle : '')
						]
					);						
				}
			}
			elseif ($this->sType == 'checkbox')
			{
				$lsHiddenPrint = $this->getGridObject()->getTemplate('class', 'hidden-print');
				$lsAlign = $this->getGridObject()->getTemplate('class', 'align-center');
			    
				$lsIcon = $this->renderIcon($this->getGridObject()->getTemplate('button-icons', 'checkbox'));
				
				$lsCol = '
					<a id="onion-grid-change-row-check-all-' . $this->oParent->get('sId') . '" class="onion-grid-change-row-check-all" href="#" title="' . GridText::text('sMarkAll') . '" data-inv="' . GridText::text('sUnmarkAll') . '">
						' . $lsIcon . '
					</a>';
			}
			elseif ($this->sType == 'radio' || $this->sType == 'actions')
			{
			    $lsHiddenPrint = $this->getGridObject()->getTemplate('class', 'hidden-print');
			}

			$lsGridHeader = $this->parseTemplate(
				$this->getGridObject()->getTemplate('table', 'th'),
				[
					'type' => "onion-grid-th-{$this->sType}",
					'resize' => $lsResizable,
					'hide' => $lsHiddenPrint,
					'align' => $lsAlign,
					'col' => $lsCol
				]
			);				
		}
				
		return $lsGridHeader;
	}
	
	
	/**
	 *
	 * @param array $paRow
	 * @param string $psBgColor
	 * @return string|null
	 */
	public function render (array $paRow = [], string $psBgColor = '#ffffff') : ?string
	{
		$lsResponse = $this->getResponseType();
		
		switch ($lsResponse)
		{
			case 'CSV':
				return $this->renderCsv($paRow);
				break;
			case 'XLS':
				return $this->renderXls($paRow, $psBgColor);
				break;
			case 'PDF':
				return $this->renderPdf($paRow);
				break;
			case 'OBJ':
				$this->renderObj($paRow);
				return null;
				break;
			default:
				return $this->renderHtml($paRow);
		}
	}
	
	
	/**
	 *
	 * @param array $paRow
	 * @return string
	 */
	public function renderCsv (array $paRow = []) : string
	{
		$lsColData = '';
		$lsValueFormated = '';
		
		$this->setValue(isset($paRow[$this->sId]) ? $paRow[$this->sId] : '');
		
		if (is_callable($this->cFormat))
		{
			$lcFormat = $this->cFormat;
			$lsValueFormated = (string)$lcFormat($this, $paRow);
		}
		
		if ($this->get('oReferer')->isEnable() && ($this->sType == 'data' || $this->sType == 'img'))
		{
			foreach ($paRow as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}
			
			$this->sValue = preg_replace($laPattern, $paRow, (string)$this->sValue);
			$lsValueFormated = preg_replace($laPattern, $paRow, $lsValueFormated);
			
			$lsColData = $lsValueFormated;
			
			if (empty($lsColData))
			{
				$lsColData = ($this->isShowValue() ? $this->sValue : '""');
			}
			
			return $this->oParent->get('sTextDelimiterCSV') . $lsColData . $this->oParent->get('sTextDelimiterCSV') . $this->oParent->get('sSeparateValueCSV');
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @param array $paRow
	 * @param string $psBgColor
	 * @return string
	 */
	public function renderXls (array $paRow = [], string $psBgColor = '#ffffff') : string
	{
		$lsValueFormated = '';

		$this->setValue(isset($paRow[$this->sId]) ? $paRow[$this->sId] : '');
		
		if (is_callable($this->cFormat))
		{
			$lcFormat = $this->cFormat;
			$lsValueFormated = (string)$lcFormat($this, $paRow);
		}

		$lsWidth = '';
		$lsAlign = (!empty($this->sAlign) ? " align=\"{$this->sAlign}\"" : '');
		$lsBgColor = ' bgColor="' . (!empty($this->sBackground) ? $this->sBackground : $psBgColor) . '"';
		$lsColor = ' style="color:' . (!empty($this->sColor) ? $this->sColor : '') . '"';
		
		if ($this->get('oReferer')->isEnable() && ($this->sType == 'data' || $this->sType == 'img'))
		{
		    $lsParams = $this->paramsToString();
			
			foreach ($paRow as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}
			
			$this->sValue = preg_replace($laPattern, $paRow, (string)$this->sValue);
			$lsValueFormated = preg_replace($laPattern, $paRow, $lsValueFormated);
			$lsParams = preg_replace($laPattern, $paRow, $lsParams);
			
			$lsColData = $lsValueFormated;
			
			if (empty($lsColData))
			{
				if ($this->sType == 'img')
				{
					$this->sValue = '<img ' . $lsParams . ' src="' . $this->sValue . '"/>';
				}
				else 
				{
					$this->sValue = '<span ' . $lsParams . '>' . $this->sValue . '</span>';
				}
				
				$lsColData = ($this->isShowValue() ? $this->sValue : "");
			}

			return '<td' . $lsBgColor . $lsColor . $lsAlign . $lsWidth . '>' . $lsColData . '</td>';
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @param array $paRow
	 * @return string
	 */
	public function renderPdf (array $paRow = []) : string
	{
		$lsValueFormated = '';
		
		$this->setValue(isset($paRow[$this->sId]) ? $paRow[$this->sId] : '');
		
		if (is_callable($this->cFormat) && $this->get('oReferer')->isEnable() && $this->get('oReferer')->isVisible())
		{
			$lcFormat = $this->cFormat;
			$lsValueFormated = (string)$lcFormat($this, $paRow);
		}
		
		if ($this->get('oReferer')->isEnable() && $this->get('oReferer')->isVisible() && ($this->sType == 'data' || $this->sType == 'img'))
		{
			foreach ($paRow as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}
			
			$this->sValue = preg_replace($laPattern, $paRow, (string)$this->sValue);
			$lsValueFormated = preg_replace($laPattern, $paRow, $lsValueFormated);
			
			$lsColData = $lsValueFormated;
			
			if (empty($lsColData))
			{
				$lsColData = ($this->isShowValue() ? $this->sValue : "");
			}
			
			return is_null($lsColData) ? '' : $lsColData;
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @param array $paRow
	 */
	public function renderObj (array $paRow = []) : void
	{
		if ($this->sType == 'checkbox' || $this->sType == 'radio')
		{
			$this->setValue(isset($paRow[$this->sValue]) ? $paRow[$this->sValue] : '');
		}
		else 
		{
			$this->setValue(isset($paRow[$this->sId]) ? $paRow[$this->sId] : '');
		}
		
		if (is_callable($this->cFormat))
		{
			$lcFormat = $this->cFormat;
			$lcFormat($this, $paRow);
		}
		
		if ($this->get('oReferer')->isEnable())
		{
			$laActions = $this->getAction();
			
			if (is_array($laActions))
			{
				foreach ($laActions as $loAction)
				{
					if ($loAction instanceof Button || $loAction instanceof Dropdown)
					{
						$loAction->setArea('row');
						$loAction->setId($loAction->get('sId') . "-{$this->oParent->get('sId')}");
						$loAction->render(['row' => $paRow, 'action' => $this->oParent->get('sAction')]);
					}
				}
			}
			
			$laPattern = [];
			
			foreach ($paRow as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}
			
			$this->sValue = preg_replace($laPattern, $paRow, (string)$this->sValue);
			$this->aParams = preg_replace($laPattern, $paRow, $this->aParams);
		}
	}
	
	
	/**
	 * 
	 * @param array $paRow
	 * @return string
	 */
	public function renderHtml (array $paRow = []) : string
	{
		$lsValueFormated = '';
		
		if ($this->sType == 'checkbox' || $this->sType == 'radio')
		{
			$this->setValue(isset($paRow[$this->sValue]) ? $paRow[$this->sValue] : '');
		}
		else
		{
			$this->setValue(isset($paRow[$this->sId]) ? $paRow[$this->sId] : '');
		}

		if (is_callable($this->cFormat) && $this->get('oReferer')->isEnable() && $this->get('oReferer')->isVisible())
		{
			$lcFormat = $this->cFormat;
			$lsValueFormated = (string)$lcFormat($this, $paRow);
		}

		$lsAlign = (!empty($this->sAlign) ? "text-align:{$this->sAlign};" : '');
		$lsWidth = (!empty($this->mWidth) ? "width:{$this->mWidth};" : '');
		$lsColor = (!empty($this->sColor) ? "color:{$this->sColor};" : '');
		$lsBackground = (!empty($this->sBackground) ? "background:{$this->sBackground};" : '');
		$lsStyle = "{$lsAlign} {$lsWidth} {$lsColor} {$lsBackground}";
		$lsStyle = (!empty(trim($lsStyle)) ? 'style="' . $lsStyle . '"' : '');
				
		$lsGridCel = '';
		
		if ($this->get('oReferer')->isEnable() && $this->get('oReferer')->isVisible())
		{			
		    $lsParams = $this->paramsToString();
			
			$lsActions = '';
			$lsActionLeft = '';
			$lsActionRight = '';
			$laActions = $this->getAction();
			
			if (is_array($laActions))
			{
				foreach ($laActions as $loAction)
				{
					if ($loAction instanceof Button || $loAction instanceof Dropdown)
					{
						$loAction->setArea('row');
						$loAction->setId($loAction->get('sId') . "-{$this->oParent->get('sId')}");
						$lsActions = $loAction->render(['row' => $paRow, 'action' => $this->oParent->get('sAction')]);
						$lsActions .= " ";

						if ($loAction->get('sPosition') == 'right')
						{
							$lsActionRight .= $lsActions;
						}
						else
						{
							$lsActionLeft .= $lsActions;
						}
					}
				}
			}
			
			$laPattern = [];
			
			foreach ($paRow as $lsField => $lmValue)
			{
				$laPattern[] = "/#%{$lsField}%#/";
			}

			$this->sValue = preg_replace($laPattern, $paRow, (string)$this->sValue);
			$lsValueFormated = preg_replace($laPattern, $paRow, $lsValueFormated);
			$lsParams = preg_replace($laPattern, $paRow, $lsParams);
			
			$lsColData = $lsValueFormated;
			$lsHiddenPrint = '';
			
			if (empty($lsColData))
			{
				if ($this->sType == 'checkbox')
				{
					$lsClass = $this->getGridObject()->getTemplate('class', 'checkbox');
					$lsColData = '<input type="checkbox" class="' . $lsClass . '" name="ck[]" value="' . $this->sValue . '" ' . $lsParams . '/>';
					$lsHiddenPrint = $this->getGridObject()->getTemplate('class', 'hidden-print');
				}
				elseif ($this->sType == 'radio')
				{
					$lsClass = $this->getGridObject()->getTemplate('class', 'radio');
					$lsColData = '<input type="radio" class="' . $lsClass . '" name="ck[]" value="' . $this->sValue . '" ' . $lsParams . '/>';
					$lsHiddenPrint = $this->getGridObject()->getTemplate('class', 'hidden-print');
				}
				elseif ($this->sType == 'actions')
				{
					$lsColData = $lsActions;
					$lsHiddenPrint = $this->getGridObject()->getTemplate('class', 'hidden-print');
				}
				elseif ($this->sType == 'img')
				{
					if (preg_match("/.*<img .* src=.*>.*/", $this->sValue))
					{
						$lsImg = $this->sValue;
					}
					else
					{
						$lsImg = '<img loading="lazy" ' . $lsParams . ' src="' . $this->sValue . '"/>';
					}

					$lsColData = $lsActionLeft . ($this->isShowValue() ? $lsImg : "") . $lsActionRight;
				}
				else
				{
					$lsColData = $lsActionLeft . ($this->isShowValue() ? '<span ' . $lsParams . '>' . $this->sValue . '</span>' : "") . $lsActionRight;
					
					if ($this->isEditable())
					{
					    $lsColData = $this->getEditableHtml($lsColData, $paRow['id'], $lsParams);
					}
				}
			}
			
			$lsGridCel = $this->parseTemplate(
				$this->getGridObject()->getTemplate('table', 'td'),
				[
					'cell-id' => "onion-grid-td-{$this->oParent->get('sId')}-{$this->sId}-{$paRow['id']}",
					'type' => "onion-grid-td-{$this->sType}",
					'class' => $this->sClass,
					'hide' => $lsHiddenPrint,
					'style' => $lsStyle,
					'data' => $lsColData
				]
			);					
		}
				
		return $lsGridCel;
	}
	
	
	/**
	 * 
	 * @param array|null $paParams
	 * @return string
	 */
	public function paramsToString (?array $paParams = null) : string
	{
	    $lsParams = '';
	    
	    if (is_null($paParams))
	    {
	        $paParams = $this->aParams;
	    }
	    
	    if (is_array($paParams))
	    {
	        foreach ($paParams as $lsName => $lmValue)
	        {
				if (is_array($lmValue))
				{
					$lsValue = json_encode($lmValue);
					$lsParams .= "{$lsName}='{$lsValue}'";
				}
				else
				{
					$lsParams .= "{$lsName}=\"{$lmValue}\" ";
				}
	        }
	    }
	    
	    return $lsParams;
	}
	
	
	/**
	 * 
	 * @param string $psColData
	 * @param string|int $pmRowId
	 * @param string $psParams
	 * @param string|null $psInput
	 * @return string
	 */
	public function getEditableHtml (string $psColData, $pmRowId, string $psParams, ?string $psInput = null) : string
	{
	    if (is_null($psInput))
	    {
			$psInput = $this->parseTemplate(
				$this->getGridObject()->getTemplate('table', 'col-data-input'),
				[
					'input-id' => "{$this->sId}-{$pmRowId}",
					'class' => "onion-grid-edit-field",
					'row-id' => $pmRowId,
					'id' => $this->sId,
					'value' => $this->sValue,
					'params' => $psParams
				]
			);
	    }

		$lsColDataEditable = $this->parseTemplate(
			$this->getGridObject()->getTemplate('table', 'col-data-editable'),
			[
				'data-class' => "onion-grid-value-area",
				'desc' => GridText::text('sClickToEdit'),
				'data' => $psColData,
				'input-class' => "onion-grid-edit-area",
				'input-id' => "onion-grid-edit-area-{$this->sId}-{$pmRowId}",
				'input-desc' => GridText::text('sEnterToSave'),
				'input' => $psInput
			]
		);		
	    
	    return $lsColDataEditable;
	}


	public function set ($pmVar, $pmValue = null) : Column
	{
		parent::set($pmVar, $pmValue);
		return $this;
	}


	public function setEnable (bool $pbEnable) : Column
	{
		parent::setEnable($pbEnable);
		return $this;
	}


	public function setParent (?object $poParent = null) : Column
	{
		$this->oParent = $poParent;
		return $this;
	}
	
	
	public function setResource (string $psResource) : Column
	{
		parent::setResource($psResource);
		return $this;
	}

	
	public function setId (string $psId) : Column
	{
		parent::setId($psId);
		return $this;
	}


	public function setName (?string $psName = null) : Column
	{
		parent::setName($psName);
		return $this;
	}


	public function setTitle (?string $psTitle = null, bool $pbShow = true) : Column
	{
		$this->bTitleShow = $pbShow;
		parent::setTitle($psTitle);	
		return $this;
	}


	public function setDescription (?string $psDescription = null) : Column
	{
		parent::setDescription($psDescription);
		return $this;
	}


	public function setHelp (?string $psHelp = null) : Column
	{
		$this->sHelp = $psHelp;
		
		return $this;
	}


	public function setIcon (?string $psIcon = null) : Column
	{
		parent::setIcon($psIcon);
		return $this;
	}


	public function setPrepared (bool $pbPrepared) : Column
	{
		parent::setPrepared($pbPrepared);
		return $this;
	}


	public function order (string $psItem, array $paOrder) : Column
	{
		parent::order($psItem, $paOrder);
		return $this;
	}


	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : Column
	{
		parent::add($psArrayObj, $poElement, $psIndex, $pnPosition, $pbNumericIndex);
		return $this;
	}


	public function remove (string $psArrayObj, string $psElementId) : Column
	{
		parent::remove($psArrayObj, $psElementId);
		return $this;
	}	
}