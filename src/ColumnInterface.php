<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;


interface ColumnInterface
{
	/**
	 * 
	 * @param array $paColumnProp
	 * @return \OnionGrid\Column
	 */
	public function factory (array $paColumnProp) : Column;


	/**
	 * 
	 * @param array $paDropdownsProp
	 * @return \OnionGrid\Column
	 */
	public function setDropdowns (array $paDropdownsProp) : Column;
	

	/**
	 * 
	 * @param object|null $poReferer
	 * @return \OnionGrid\Column
	 */
	public function setReferer (?object $poReferer = null) : Column;


	/**
	 *
	 * @param string|int|null $pmValue
	 * @return \OnionGrid\Column
	 */
	public function setValue ($pmValue = null) : Column;
	
	
	/**
	 *
	 * @param string $psOrdering
	 * @return \OnionGrid\Column
	 */
	public function setOrdering (string $psOrdering = '') : Column;
		
	
	/**
	 *
	 * @param bool $pbSortable        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setSortable (bool $pbSortable = false) : Column;

	
	/**
	 *
	 * @param bool $pbSearchable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setSearchable (bool $pbSearchable = false) : Column;
	
	
	/**
	 * 
	 * @param string|null $psField
	 * @return \OnionGrid\Column
	 */
	public function setSearchField (?string $psField = null) : Column;
		
	
	/**
	 *
	 * @param bool $pbSearch        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setSearchableIfHidden (bool $pbSearcheable = true) : Column;

	
	/**
	 *
	 * @param bool $pbVisible        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setVisible (bool $pbVisible = true) : Column;

	
	/**
	 *
	 * @param bool $pbResizable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setResizable (bool $pbResizable = false) : Column;


	/**
	 * Values accepted for $psAlign: left, right or center
	 *
	 * @param string $psAlign
	 * @return \OnionGrid\Column
	 */
	public function setTitleAlign (string $psAlign = 'left') : Column;
	
	
	/**
	 * Specify if a grid value is editable.
	 * Default is false.
	 * If true, it needs to receive extra params:
	 * setParam(array(
	 * 		'data-url' => 'url to save individual field', 
	 * 		'data-toUpper' => 'false' //if true all caracter will turn to upper case. Default is true
	 * ))
	 * 
	 * @param bool $pbEditable
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setEditable (bool $pbEditable = false) : Column;
	
	
	/**
	 *
	 * @param bool $pbShowValue
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setShowValue (bool $pbShowValue = false) : Column;
	
	
	/**
	 *
	 * @param string $psClass        	
	 * @return \OnionGrid\Column
	 */
	public function setClass (string $psClass = '') : Column;

	
	/**
	 *
	 * @param string $psWidth        	
	 * @return \OnionGrid\Column
	 */
	public function setWidth (string $psWidth = '') : Column;

	
	/**
	 * Values accepted for $psAlign: left, right or center
	 *
	 * @param string $psAlign
	 * @return \OnionGrid\Column
	 */
	public function setAlign (string $psAlign = 'left') : Column;
	
	
	/**
	 *
	 * @param string $psColor
	 * @return \OnionGrid\Column
	 */
	public function setColor (string $psColor = '') : Column;

	
	/**
	 *
	 * @param string $psBackground
	 * @return \OnionGrid\Column
	 */
	public function setBackground (string $psBackground = '') : Column;
	
	
	/**
	 * Values accepted for $psType: checkbox, radio, data, img or actions
	 *
	 * @param string $psType
	 * @return \OnionGrid\Column
	 */
	public function setType (string $psType = 'data') : Column;
	
	
	/**
	 * Set extra params.
	 * 'data-url',
	 * 'data-toUpper',
	 * 'data-pdfw',
	 * 
	 * @param array $paParams
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function setParams (array $paParams) : Column;
	
	
	/**
	 * function (\OnionGrid\Column $poColumn, array $paRow) : string
	 * 
	 * @param callable|null $pcFormat
	 * @return \OnionGrid\Column
	 */
	public function setFormat (?callable $pcFormat = null) : Column;
		
	
	// Action methods
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isVisible () : bool;
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isResizable () : bool;
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isEditable () : bool;
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isSearchable () : bool;
		
	
	/**
	 * 
	 * @return bool
	 */
	public function isSearchableIfHidden () : bool;

	
	/**
	 * 
	 * @return bool
	 */
	public function isSortable () : bool;
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isShowValue () : bool;
	
	
	/**
	 * Remove an action Button or Dropdown from the array object
	 *
	 * @param string $psActionId
	 * @return \OnionGrid\Column
	 */
	public function removeAction (string $psActionId) : Column;
	
	
	/**
	 * Load the Button or Dropdown object from array object
	 * or the entire array if $psActionId = null
	 *
	 * @param string|null $psActionId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Button|\OnionGrid\Dropdown|array|null
	 */
	public function getAction (?string $psActionId = null, bool $pbValid = true);
	
	
	/**
	 * Create a new Button object into the array object
	 * and setting its id and name
	 *
	 * @param string $psButtonId
	 * @return \OnionGrid\Button
	 */
	public function createButton (string $psButtonId) : Button;
	
	
	/**
	 * Add an existent Button object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Button $poButton
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function addButton (Button $poButton, ?string $psIndex = null, ?int $pnPosition = null) : Column;
	
	
	/**
	 * Remove a Button from the array object
	 *
	 * @param string $psButtonId
	 * @return \OnionGrid\Column
	 */
	public function removeButton (string $psButtonId) : Column;
	
	
	/**
	 * Load the Button object from array object
	 * or the entire array if $psButtonId = null
	 *
	 * @param string|null $psButtonId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Button|array|null
	 */
	public function getButton (?string $psButtonId = null, bool $pbValid = true);
	
	
	/**
	 * Create a new Dropdown object into the array object
	 * and setting its id and name
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Dropdown
	 */
	public function createDropdown (string $psDropdownId) : Dropdown;
	
	
	/**
	 * Add an existent Dropdown object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Dropdown $poDropdown
	 * @param string|null $psIndex
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Column
	 */
	public function addDropdown (Dropdown $poDropdown, ?string $psIndex = null, ?int $pnPosition = null) : Column;
	
	
	/**
	 * Remove an Dropdown from the array object
	 *
	 * @param string $psDropdownId
	 * @return \OnionGrid\Column
	 */
	public function removeDropdown (string $psDropdownId) : Column;
	
	
	/**
	 * Load the Dropdown object from array object
	 * or the entire array if $psDropdownId = null
	 *
	 * @param string|null $psDropdownId
	 * @param bool $pbValid
	 * @throws \Exception
	 * @return \OnionGrid\Dropdown|array|null
	 */
	public function getDropdown (?string $psDropdownId = null, bool $pbValid = true);

	/**
	 * 
	 * @return string|null
	 */
	public function getValue ();

	
	/**
	 * 
	 * @return string
	 */
	public function getResponseType () : string;
	
	
	/**
	 *
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function getClearProperties (bool $pbClearProperty = false) : array;
	
	
	/**
	 *
	 * @return string|null
	 */
	public function renderHeader () : ?string;

	
	/**
	 *
	 * @return string
	 */
	public function renderHeaderCsv () : string;
	
	
	/**
	 *
	 * @return string
	 */
	public function renderHeaderXls () : string;
	
	
	/**
	 *
	 * @return string
	 */
	public function renderHeaderPdf () : string;
	
	
	/**
	 *
	 */
	public function renderHeaderObj () : void;
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderHeaderHtml () : string;
	
	
	/**
	 *
	 * @param array $paRow
	 * @return string|null
	 */
	public function render (array $paRow = []) : ?string;
	
	
	/**
	 *
	 * @param array $paRow
	 * @return string
	 */
	public function renderCsv (array $paRow = []) : string;
	
	
	/**
	 *
	 * @param array $paRow
	 * @return string
	 */
	public function renderXls (array $paRow = []) : string;
	
	
	/**
	 *
	 * @param array $paRow
	 * @return string
	 */
	public function renderPdf (array $paRow = []) : string;
	
	
	/**
	 *
	 * @param array $paRow
	 */
	public function renderObj (array $paRow = []) : void;
	
	
	/**
	 * 
	 * @param array $paRow
	 * @return string
	 */
	public function renderHtml (array $paRow = []) : string;
	
	
	/**
	 * 
	 * @param array|null $paParams
	 * @return string
	 */
	public function paramsToString (?array $paParams = null) : string;
	
	
	/**
	 * 
	 * @param string $psColData
	 * @param string|int $pmRowId
	 * @param string $psParams
	 * @param string|null $psInput
	 * @return string
	 */
	public function getEditableHtml (string $psColData, $pmRowId, string $psParams, ?string $psInput = null) : string;
}