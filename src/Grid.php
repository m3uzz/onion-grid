<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;
use OnionGrid\AbstractGrid;
use OnionGrid\Column;
use OnionGrid\GridText;
use OnionGrid\InterfaceGrid;
use OnionLib\Pagination;


class Grid extends AbstractGrid implements InterfaceGrid
{
	const TYPE = 'grid';

	/**
	 * 
	 * @var string
	 */
	protected $sLayout = 'default';

	/**
	 * 
	 * @var string
	 */
	protected $sLayoutTemplatePath = __DIR__ . DS . ".." . DS . "public" . DS . "template" . DS; 

	/**
	 *
	 * @var array
	 */
	protected $aTemplate = null;

	/**
	 *
	 * @var string
	 */
	protected $sAction = '';
	
	/**
	 * Can be: `GET` or `POST`
	 * Default: GET
	 * 
	 * @var string
	 */
	protected $sFormMethod = 'GET';
	
	/**
	 * Can be: `HTML` or `AJAX`
	 * Default: HTML
	 * 
	 * @var string
	 */
	protected $sFormRequest = 'HTML';
	
	/**
	 * 
	 * @var string
	 */
	protected $sFormAction = '#';
	
	/**
	 * Can be: `HTML`, `OBJ`, `CSV`, `PDF` or `XLS`
	 * Default: HTML
	 *
	 * @var string
	 */
	protected $sGridResponse = 'HTML';
	
	/**
	 * Values: `default`, `modal` or `popup`
	 * Default: default
	 *
	 * @var sting
	 */
	protected $sWindowType = 'default';
	
	/**
	 *
	 * @var string
	 */
	protected $sRoute = '';

	/**
	 *
	 * @var string
	 */
	protected $sGetParam = '';

	/**
	 * @var array
	 */
	protected $aInputParam = [
		'back' => ['value' => '', 'reload' => true],
		'urlBack' => ['value' => '', 'reload' => true],
		'act' => ['value' => '', 'reload' => true],
		'btn' => ['value' => '', 'reload' => true],
		'id' => ['value' => '', 'reload' => true],
		'ckd' => ['value' => '', 'reload' => true],
		'ord' => ['value' => '', 'reload' => true],
		'rows' => ['value' => '', 'reload' => true],
		'p' => ['value' => '', 'reload' => true],
		'w' => ['value' => '', 'reload' => true],
	];
	
	/**
	 * System messages
	 *
	 * @var array
	 */
	protected $aMessages = [];
	
	/**
	 *
	 * @var string
	 */
	protected $sFolderTitle = '';	

	/**
	 *
	 * @var string
	 */
	protected $sFolderIcon = '';	
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowHeader = true;
	
	/**
	 * Toolbar
	 *
	 * @var \OnionGrid\Toolbar
	 */
	protected $oToolbar = null;

	/**
	 *
	 * @var string
	 */
	protected $sSearchQuery = '';

	/**
	 *
	 * @var bool
	 */
	protected $bShowPagination = true;
	
	/**
	 * Pagination object
	 *
	 * @var \OnionLib\Pagination
	 */
	protected $oPagination = null;
	
	/**
	 *
	 * @var array
	 */
	protected $aPaginationNumRows = ['6', '12', '25', '50', '100'];
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowPaginationInfo = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $bShowPaginationNav = true;
	
	/**
	 * Rows to show in the grid
	 * 
	 * @var int
	 */
	protected $nNumRows = 25;
	
	/**
	 * Max Rows to show in the grid
	 *
	 * @var int
	 */
	protected $nMaxNumRows = 1000;
	
	/**
	 * Grid columns array
	 * 
	 * @var array of \OnionGrid\Column
	 */
	protected $aColums = [];

	/**
	 * 
	 * @var array
	 */
	protected $aColOrder = [];

	/**
	 * Results to be showed into the grid
	 * 
	 * @var array
	 */
	protected $aData = [];
	
	/**
	 * Total of results for the query
	 *
	 * @var int
	 */
	protected $nTotalResults = 0;
	
	/**
	 * Total of results for the query
	 *
	 * @var int
	 */
	protected $nCurrentPage = 0;
	
	/**
	 * 
	 * @var string
	 */
	protected $sSeparateValueCSV = ';';
	
	/**
	 * 
	 * @var string
	 */
	protected $sTextDelimiterCSV = '';

	/**
	 * 
	 * @var callable
	 */
	protected $cAclCheck = null;
		
	/**
	 *
	 * @var callable
	 */
	protected $cHeader = null;
	
	/**
	 * 
	 * @var callable
	 */
	protected $cFooter = null;

	/**
	 *
	 * @var callable
	 */
	protected $cExtraRowData = null;
	
	/**
	 * 
	 * @var callable
	 */
	protected $cPdf = null;
	
	/**
	 * 
	 * @var string
	 */
	protected $sHeaderContent = '';
	
	/**
	 * 
	 * @var string
	 */
	protected $sFooterContent = '';
	
	/**
	 * Can be: `none`, `session`, 'cookie', 'db', 'nosql'
	 * Default: session
	 *  
	 * @var string
	 */
	protected $sPrefCacheType = 'cookie';

	/**
	 * Can be: `desktop`, `mobile`
	 * Default: desktop
	 *  
	 * @var string
	 */
	protected $sDeviceType = 'desktop';	

	/**
     *
     * @var object
     */
    protected $oIdentity = null;


	// Settings

	
	/**
	 * Construct an object setting the id, name and resource properties
	 * if the id is not given the construct will return an exception
	 *
	 * @param string $psId Instance identifier.
	 * @param string|null $psResource
	 * @param \OnionGrid\InterfaceGrid|null $poParent
	 * @throws \InvalidArgumentException
	 */	
	public function __construct (string $psId, ?string $psResource = null, ?InterfaceGrid $poParent = null)
	{
		parent::__construct($psId, $psResource, $poParent);
		
		$this->createToolbar();
		
		$this->createColumn('OnionGridCheck')
			->setValue('id')
			->setType('checkbox')
			->setAlign('center')
			->setWidth('2%')
			->setResizable(false)
			->setBackground("#f0f0f0");
	}
	

	/**
	 * 
	 * @param array $paGridProp
	 * @return \OnionGrid\Grid
	 */
	public function factory (array $paGridProp) : Grid
	{
		if (is_array($paGridProp))
		{
			foreach ($paGridProp as $lsProperty => $lmValue)
			{
				$lsMethod = "set{$lsProperty}";

				if (method_exists($this, $lsMethod) && !is_null($lmValue))
				{
					$this->$lsMethod($lmValue);
				}
			}
		}

		return $this;
	}

	
	/**
	 * 
	 * @param array $paToolbarProp
	 * @return \OnionGrid\Grid
	 */
	public function setToolbar (array $paToolbarProp) : Grid
	{
		$loToolbar = $this->getToolbar();
		$loToolbar->factory($paToolbarProp);

		return $this;
	}


	/**
	 * 
	 * @param array $paGridToolbars
	 * @return \OnionGrid\Grid
	 */
	public function setToolbars (array $paGridToolbars) : Grid
	{
		if (is_array($paGridToolbars))
		{
			$laToolbars = $this->getToolbar();

			foreach ($laToolbars as $loToolbar)
			{
				if (isset($paGridToolbars[$loToolbar->getId()]))
				{
					$laProperties = $paGridToolbars[$loToolbar->getId()];

					if (is_array($laProperties))
					{
						foreach ($laProperties as $lsProperty => $lmValue)
						{
							$lsMethod = "set{$lsProperty}";

							if (method_exists($loToolbar, $lsMethod) && !is_null($lmValue))
							{
								$loToolbar->$lsMethod($lmValue);
							}
						}
					}
				}
			}
		}

		return $this;
	}

	
	/**
	 * 
	 * @param array $paColumnsProp
	 * @return \OnionGrid\Grid
	 */
	public function setColumns (array $paColumnsProp) : Grid
	{
		if (is_array($paColumnsProp))
		{
			$laColumns = $this->getColumn();

			foreach ($laColumns as $loColumn)
			{
				if (isset($paColumnsProp[$loColumn->getId()]))
				{
					$laProperties = $paColumnsProp[$loColumn->getId()];

					if (is_array($laProperties))
					{
						$loColumn->factory($laProperties);
					}
				}
			}
		}

		return $this;
	}


	/**
	 *
	 * @param string|null $psLayout
	 * @return \OnionGrid\Grid
	 */
	public function setLayout (?string $psLayout = null) : Grid
	{
		if (!empty($psLayout) || is_null($psLayout))
		{
			$this->sLayout = $psLayout;
		}
	
		return $this;
	}


	/**
	 *
	 * @param string|null $psLayoutTemplatePath
	 * @return \OnionGrid\Grid
	 */
	public function setLayoutTemplatePath (?string $psLayoutTemplatePath = null) : Grid
	{
		if (!empty($psLayoutTemplatePath) || is_null($psLayoutTemplatePath))
		{
			$this->sLayoutTemplatePath = $psLayoutTemplatePath;
		}
	
		return $this;
	}	


	/**
	 *
	 * @param string|null $psAction
	 * @return \OnionGrid\Grid
	 */
	public function setAction (?string $psAction = null) : Grid
	{
		if (!empty($psAction) || is_null($psAction))
		{
			$this->sAction = $psAction;
		}
	
		return $this;
	}
	
	
	/**
	 *
	 * @param string|null $psRoute
	 * @return \OnionGrid\Grid
	 */
	public function setRoute (?string $psRoute = null) : Grid
	{
		if (!empty($psRoute))
		{
			$this->sRoute = $psRoute;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string|null $psFolderTitle
	 * @return \OnionGrid\Grid
	 */
	public function setFolderTitle (?string $psFolderTitle = null) : Grid
	{
		if (!empty($psFolderTitle))
		{
			$this->sFolderTitle = $psFolderTitle;
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string|null $psFolderIcon
	 * @return \OnionGrid\Grid
	 */
	public function setFolderIcon (?string $psFolderIcon = null) : Grid
	{
		if (!empty($psFolderIcon))
		{
			$this->sFolderIcon = $psFolderIcon;
		}
		else
		{
			$this->sFolderIcon = 'material-icons folder';
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowToolbar
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setShowToolbar (bool $pbShowToolbar = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setEnable($pbShowToolbar);
		}
	
		return $this;
	}	

	
	/**
	 *
	 * @param string|null $psSearchQuery
	 * @return \OnionGrid\Grid
	 */
	public function setSearchQuery (?string $psSearchQuery = null) : Grid
	{
		if (!empty($psSearchQuery))
		{
			$this->sSearchQuery = $psSearchQuery;
		}
	
		return $this;
	}


	/**
	 *
	 * @param array $paColOrder
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setColOrder (array $paColOrder, bool $pbChange = false) : Grid
	{
		$this->aColOrder = [];

		if (is_array($paColOrder))
		{
			foreach ($paColOrder as $lsCol => $lsOrd)
			{
				if ($pbChange)
				{
					$loCol = $this->getColOrder($lsCol, false);

					if ($loCol == null || !$loCol->isVisible() || !$loCol->isSortable())
					{
						//throw new \InvalidArgumentException('Field order not allowed;');
						continue;
					}
					
					$lsCol = $loCol->get('sOrdering');
					$lsCol = (!empty($lsCol) ?  $lsCol : $loCol->sId);
				}

				$lsOrd = strtoupper($lsOrd);
			
				if ($lsOrd == 'ASC' || $lsOrd == 'DESC' || $lsOrd == 'RAND')
				{
					$this->aColOrder[$lsCol] = $lsOrd;
				}
				else
				{
					throw new \InvalidArgumentException('Order option error, try: ASC, DESC or RAND;');
				}
			}
		}
		else
		{
			throw new \InvalidArgumentException('The value of "aColOrder" property need to be an array!');
		}
	
		return $this;
	}
	

	/**
	 *
	 * @param bool $pbShowPaginationInfo        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setShowPaginationInfo (bool $pbShowPaginationInfo = true) : Grid
	{
		if (is_bool($pbShowPaginationInfo))
		{
			$this->bShowPaginationInfo = $pbShowPaginationInfo;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "showPaginationInfo" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowHeader
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setShowHeader (bool $pbShowHeader = true) : Grid
	{
		if (is_bool($pbShowHeader))
		{
			$this->bShowHeader= $pbShowHeader;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "bShowHeader" property need to be a bool!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowPagination
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setShowPagination (bool $pbShowPagination = true) : Grid
	{
		if (is_bool($pbShowPagination))
		{
			$this->bShowPagination = $pbShowPagination;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "showPagination" property need to be a bool!');
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param bool $pbShowPaginationNav
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setShowPaginationNav (bool $pbShowPaginationNav = true) : Grid
	{
		if (is_bool($pbShowPaginationNav))
		{
			$this->bShowPaginationNav = $pbShowPaginationNav;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "showPaginationNav" property need to be a bool!');
		}
	
		return $this;
	}
	
	
	/**
	 *
	 * @param array $paPaginationNumRows
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setPaginationNumRows (array $paPaginationNumRows) : Grid
	{
		if (is_array($paPaginationNumRows))
		{
			$this->aPaginationNumRows = $paPaginationNumRows;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "aPaginationNumRows" property need to be an array!');
		}
		
		return $this;
	}	

	
	/**
	 *
	 * @param int $pnNumRows
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setNumRows (int $pnNumRows) : Grid
	{
		if (is_int((int)$pnNumRows))
		{
			$pnNumRows = ($pnNumRows == 0 ? $this->nMaxNumRows : $pnNumRows);
			$pnNumRows = ($pnNumRows > $this->nMaxNumRows ? $this->nMaxNumRows : $pnNumRows);
			
			$this->nNumRows = $pnNumRows;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "nNumRows" property need to be an int!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param int $pnMaxNumRows
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setMaxNumRows (int $pnMaxNumRows) : Grid
	{
		if (is_int((int)$pnMaxNumRows))
		{
			$this->nMaxNumRows = $pnMaxNumRows;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "nMaxNumRows" property need to be an int!');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param array|null $paData
	 * @return \OnionGrid\Grid
	 */
	public function setData (?array $paData = null) : Grid
	{
		$this->aData = $paData;
	
		return $this;
	}	

	
	/**
	 *
	 * @param int $pnTotalResults
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setTotalResults (int $pnTotalResults = 0) : Grid
	{
		if (is_int((int)$pnTotalResults))
		{
			$this->nTotalResults = $pnTotalResults;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "totalResults" property need to be an int!');
		}
	
		return $this;
	}

	
	/**
	 *
	 * @param int $pnCurrentPage
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setCurrentPage (int $pnCurrentPage = 0) : Grid
	{
		if (is_int((int)$pnCurrentPage))
		{
			$this->nCurrentPage = $pnCurrentPage;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "currentPage" property need to be an int!');
		}
	
		return $this;
	}
		
	
	/**
	 *
	 * @param \OnionLib\Pagination|null $poPagination
	 * @throws \InvalidArgumentException
	 * @return OnionLib\Pagination
	 */
	public function setPagination (?Pagination $poPagination = null) : Pagination
	{
		if ($poPagination instanceof Pagination)
		{
			$this->oPagination = $poPagination;
		}
		elseif ($poPagination !== null)
		{
			throw new \InvalidArgumentException('The value of "pagination" property need to be an instance of OnionLib\Pagination!');		
		}
		else
		{
			$loPagination = new Pagination();
			$loPagination->set('sUri', $this->sRoute . $this->sAction . '?');
			$loPagination->set('nResPerPage', $this->nNumRows);
			$loPagination->setPaginator(
					(int)$this->nTotalResults,
					(int)$this->nCurrentPage
			);
			
			$this->oPagination = $loPagination;
		}
	
		return $this->oPagination;
	}
		
	
	/**
	 *
	 * @param array|string $pmParams
	 * @param string $psValue
	 * @param bool $pbReload
	 * @return \OnionGrid\Grid
	 */
	public function setInputParam ($pmParams, string $psValue = '', bool $pbReload = true) : Grid
	{
		if (is_array($pmParams))
		{
			$this->aInputParam = $pmParams;
		}
		elseif (is_string($pmParams) && isset($this->aInputParam[$pmParams]))
		{
			$this->aInputParam[$pmParams] = [
				'value' => $psValue,
				'reload' => $pbReload
			];
		}
	
		return $this;
	}


	/**
	 *
	 * @param string $psInput
	 * @param string $psValue
	 * @param bool $pbReload
	 * @return \OnionGrid\Grid
	 */
	public function addInputParam (string $psInput, string $psValue = '', bool $pbReload = true) : Grid
	{
		$this->aInputParam[$psInput] = [
			'value' => $psValue,
			'reload' => $pbReload
		];
	
		return $this;
	}


	/**
	 *
	 * @param string $psInput
	 * @return \OnionGrid\Grid
	 */
	public function removeInputParam (string $psInput) : Grid
	{
		if (isset($this->aInputParam[$psInput]))
		{
			unset($this->aInputParam[$psInput]);
		}
	
		return $this;
	}


	/**
	 * 
	 * @param string|null $psInput
	 * @return array
	 */
	public function getInputParam (?string $psInput = null) : array
	{
		if (is_null($psInput))
		{
			return $this->aInputParam;
		}
		elseif (isset($this->aInputParam[$psInput]))
		{
			return $this->aInputParam[$psInput];
		}
	
		return null;
	}


	/**
	 * 
	 * @param string $psInput
	 * @return string|null
	 */
	public function getInputParamValue (string $psInput)
	{
		if (isset($this->aInputParam[$psInput]))
		{
			return $this->aInputParam[$psInput]['value'];
		}
	
		return null;
	}

	
	/**
	 *
	 * @param array $paMessages
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setMessages (array $paMessages = []) : Grid
	{
		if (is_array($paMessages))
		{
			foreach ($paMessages as $laMsg)
			{
				$this->aMessages[] = $laMsg;
			}
		}
		else
		{
			throw new \InvalidArgumentException('The value of "messages" property need to be an array!');
		}
	
		return $this;
	}
	
	
	/**
	 * Values accepted for $psType: `default`, `modal` or `popup`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setWindowType (string $psType = 'default') : Grid
	{
		$psType = strtolower($psType);
		
	    if ($psType == "default" || $psType == "popup" || $psType == "modal")
		{
			$this->sWindowType = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Window type option error, try: default, modal or popup;');
		}
	
		return $this;
	}
	
	
	/**
	 * Values accepted for $psMethod: `GET` or `POST`
	 *
	 * @param string $psMethod
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setFormMethod (string $psMethod = 'GET') : Grid
	{
		$psMethod = strtoupper($psMethod);
		
		if ($psMethod == "GET" || $psMethod == "POST")
		{
			$this->sFormMethod = $psMethod;
		}
		else
		{
			throw new \InvalidArgumentException('Grid form method option error, try: GET or POST;');
		}
		
		return $this;
	}
	
	
	/**
	 * Values accepted for $psType: `HTML` or `AJAX`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setFormRequest (string $psType = 'HTML') : Grid
	{
		$psType = strtoupper($psType);
		
		if ($psType == "HTML" || $psType == "AJAX")
		{
			$this->sFormRequest = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Grid form request option error, try: HTML or AJAX;');
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string|null $psAction
	 * @return \OnionGrid\Grid
	 */
	public function setFormAction (?string $psAction = null) : Grid
	{
		if (!empty($psAction) || is_null($psAction))
		{
			$this->sFormAction = $psAction;
		}
		
		return $this;
	}
	
	
	/**
	 * Values accepted for $psType: `HTML`, `OBJ`, `CSV`, `PDF` or `XLS`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setGridResponse (string $psType = 'HTML') : Grid
	{
		$psType = strtoupper($psType);
		
		if ($psType == "HTML" || $psType == "OBJ" || $psType == "CSV" || $psType == "PDF" || $psType == "XLS")
		{
			$this->sGridResponse = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Grid request option error, try: HTML, OBJ, CSV, PDF or XLS;');
		}
		
		return $this;
	}
	
	
	/**
	 * Values accepted for $psType: `none`, `session`, `cookie`, `db` or `nosql`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setPrefCacheType (string $psType = 'session') : Grid
	{
		$psType = strtolower($psType);
		
		if ($psType == "none" || $psType == "session" || $psType == "cookie" || $psType == "db" || $psType == "nosql")
		{
			$this->sPrefCacheType = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Grid request option error, try: none, session, cookie, db or nosql;');
		}
		
		return $this;
	}
	
	
	/**
	 * Values accepted for $psType: `desktop`, `mobile`
	 *
	 * @param string $psType
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function setDeviceType (string $psType = 'desktop') : Grid
	{
		$psType = strtolower($psType);
		
		if ($psType == "desktop" || $psType == "mobile")
		{
			$this->sDeviceType = $psType;
		}
		else
		{
			throw new \InvalidArgumentException('Grid request option error, try: desktop or mobile;');
		}
		
		return $this;
	}


	/**
	 *
	 * @param string|null $psSeparate
	 * @return \OnionGrid\Grid
	 */
	public function setSeparateValueCSV (?string $psSeparate = null) : Grid
	{
		if (!empty($psSeparate))
		{
			$this->sSeparateValueCSV = $psSeparate;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string? $psDelimiter
	 * @return \OnionGrid\Grid
	 */
	public function setTextDelimiterCSV (?string $psDelimiter = null) : Grid
	{
		if (!empty($psDelimiter))
		{
			$this->sTextDelimiterCSV = $psDelimiter;
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string|null $psParams
	 * @return \OnionGrid\Grid
	 */
	public function setGetParam (?string $psParams = null) : Grid
	{
		$this->sGetParam = $psParams;
		
		return $this;
	}
	
	
	/**
	 * ```
	 * setAclCheck(function (\OnionGrid\Grid $poGrid) : string);
	 * ```
	 *  
	 * @param callable|null $pcAclCheck 
	 * @return \OnionGrid\Grid
	 */
	public function setAclCheck (?callable $pcAclCheck = null) : Grid
	{
		$this->cAclCheck = $pcAclCheck;
		
		return $this;
	}

	
	/**
	 * ```
	 * setHeader(function (\OnionGrid\Grid $poGrid) : string);
	 * ```
	 *
	 * @param callable|null $pcHeader 
	 * @return \OnionGrid\Grid
	 */
	public function setHeader (?callable $pcHeader = null) : Grid
	{
		$this->cHeader = $pcHeader;
		
		return $this;
	}
	
	
	/**
	 *
	 * @param string $psContent
	 * @return \OnionGrid\Grid
	 */
	public function setHeaderContent (string $psContent = '') : Grid
	{
		$this->sHeaderContent = $psContent;
		
		return $this;
	}
	
	
	/**
	 * ```
	 * setFooter(function (\OnionGrid\Grid $poGrid) : string);
	 * ```
	 *
	 * @param callable|null $pcFooter 
	 * @return \OnionGrid\Grid
	 */
	public function setFooter (?callable $pcFooter = null) : Grid
	{
		$this->cFooter = $pcFooter;
		
		return $this;
	}

	
	/**
	 * ```
	 * setFormat(function (array $paRow) : string);
	 * ```
	 * 
	 * @param callable|null $pcFormat
	 * @return \OnionGrid\Grid
	 */
	public function setExtraRowData (?callable $pcFormat = null) : Grid
	{
		$this->cExtraRowData = $pcFormat;
	
		return $this;
	}

	
	/**
	 *
	 * @param string $psContent
	 * @return \OnionGrid\Grid
	 */
	public function setFooterContent (string $psContent = '') : Grid
	{
		$this->sFooterContent = $psContent;
		
		return $this;
	}
	

	/**
	 * ```
	 * setPdf(function (\OnionGrid\Grid $poGrid) : string);
	 * ```
	 * 
	 * @param callable|null $pcPdf 
	 * @return \OnionGrid\Grid
	 */
	public function setPdf (?callable $pcPdf = null) : Grid
	{
		$this->cPdf = $pcPdf;
		
		return $this;
	}

	
	/**
	 * ```
	 * setFilter(function (\OnionGrid\Toolbar $poToolbar) : string);
	 * ```
	 *
	 * @param callable|null $pcFilter 
	 * @return \OnionGrid\Grid
	 */
	public function setFilter (?callable $pcFilter = null) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setFilter($pcFilter);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowDefaultToolbar
	 * @return \OnionGrid\Grid
	 */
	public function setShowDefaultToolbar (bool $pbShowDefaultToolbar = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowDefaultToolbar($pbShowDefaultToolbar);
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param bool $pbShowFilterBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowFilterBtn (bool $pbShowFilterBtn = false) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowFilterBtn($pbShowFilterBtn);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowSearchBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowSearchBtn (bool $pbShowSearchBtn = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowSearchBtn($pbShowSearchBtn);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowReloadBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowReloadBtn (bool $pbShowReloadBtn = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowReloadBtn($pbShowReloadBtn);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowPrintBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowPrintBtn (bool $pbShowPrintBtn= true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowPrintBtn($pbShowPrintBtn);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowExportBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowExportBtn (bool $pbShowExportBtn = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowExportBtn($pbShowExportBtn);
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param bool $pbShowVielColBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowViewColBtn (bool $pbShowVielColBtn = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowViewColBtn($pbShowVielColBtn);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbShowNumRowsBtn
	 * @return \OnionGrid\Grid
	 */
	public function setShowNumRowsBtn (bool $pbShowNumRowsBtn = true) : Grid
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			$this->oToolbar->setShowNumRowsBtn($pbShowNumRowsBtn);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function isShowSearch () : bool
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			return $this->oToolbar->get('bShowSearch');
		}
		
		return false;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isShowToolbar () : bool
	{
		if ($this->oToolbar instanceof Toolbar)
		{
			return $this->oToolbar->isEnable();
		}
		
		return false;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function isShowHeader () : bool
	{
		return $this->bShowHeader;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function isShowPagination () : bool
	{
		return $this->bShowPagination;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function isShowPaginationInfo () : bool
	{
		return $this->bShowPaginationInfo;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function isShowPaginationNav () : bool
	{
		return $this->bShowPaginationNav;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function getResponseType () : string
	{
		return $this->sGridResponse;	
	}
	
	
	/**
	 *
	 * @param string $psField
	 * @return string
	 */
	public function getSearchField (string $psField) : string
	{
		return $this->getColumn($psField)->get('sSearchField');
	}
	
	
	/**
	 *
	 * @return callable|null
	 */
	public function getAclCheck () : ?callable
	{
		return $this->cAclCheck;
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function getPrefCacheType () : ?string
	{
		return $this->sPrefCacheType;
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function getDeviceType () : ?string
	{
		return $this->sDeviceType;
	}	
	

	/**
	 * 
	 * @param string $psArea
	 * @param string $psTemplate
	 * @return string|array
	 */
	public function getTemplate (string $psArea, string $psTemplate)
	{
		if ($this->aTemplate == null)
		{
			$this->aTemplate = include ($this->sLayoutTemplatePath . "templates.phtml");
		}

		return $this->aTemplate[$this->sLayout]['grid'][$psArea][$psTemplate]; 
	}

	
    /**
     *
     * @param object|null $poIdentity
     */
    public function setIdentity (?object $poIdentity) : void
    {
        $this->oIdentity = $poIdentity;
    }


    /**
     *
     * @return object|null
     */
    public function getIdentity () : ?object
    {
        return $this->oIdentity;
    }

	
	// Action methods
	
	
	/**
	 * Create a new Column object into the array object
	 * and setting its id and name
	 *
	 * @param string $psColumnId        	
	 * @return \OnionGrid\Column
	 */
	public function createColumn (string $psColumnId) : Column
	{
		return $this->aColums[] = new Column($psColumnId, $this->sResource, $this);
	}

	
	/**
	 * Add an existent Column object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param \OnionGrid\Column $poColumn        	
	 * @param string|null $psIndex        	
	 * @param int|null $pnPosition
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\Grid
	 */
	public function addColumn (Column $poColumn, ?string $psIndex = null, ?int $pnPosition = null) : Grid
	{
		if ($poColumn instanceof Column)
		{
			return parent::add('aColums', $poColumn, $psIndex, $pnPosition, true);
		}
		else
		{
			throw new \InvalidArgumentException('$poColumn should be a instance of \OnionGrid\Column!');
		}
	}

	
	/**
	 * Remove a Column from the array object
	 *
	 * @param string $psColumnId        	
	 * @return \OnionGrid\Grid
	 */
	public function removeColumn (string $psColumnId) : Grid
	{
		return parent::remove('aColums', $psColumnId);
	}

	
	/**
	 * Load the Column object from array object
	 * or the entire array if $psColumnId = null
	 *
	 * @param string|null $psColumnId        	
	 * @param bool $pbValid        	
	 * @throws \Exception
	 * @return \OnionGrid\Column|array|null
	 */
	public function getColumn (?string $psColumnId = null, bool $pbValid = true)
	{
		return parent::getElement('aColums', $psColumnId, $pbValid);
	}
	

	/**
	 * Load the Column object from array object
	 * or the entire array if $psColumnId = null
	 *
	 * @param string|null $psColumn        	
	 * @param bool $pbValid        	
	 * @throws \Exception
	 * @return \OnionGrid\Column|array|null
	 */
	public function getColOrder (?string $psColumn = null, bool $pbValid = true)
	{
		$loColumn = parent::getElement('aColums', $psColumn, $pbValid);

		if (is_null($loColumn))
		{
			foreach ($this->aColums as $loColumn)
			{
				if ($loColumn->get('sOrdering') == $psColumn)
				{
					return $loColumn;
				}
			}
		}

		return $loColumn;
	}

	
	/**
	 * Create a new Toolbar object and setting its id and name
	 *
	 * @return \OnionGrid\Toolbar
	 */
	public function createToolbar () : Toolbar
	{
		return $this->oToolbar = new Toolbar('Toolbar', $this->sResource, $this);
	}
	
	
	/**
	 * Load the Toolbar object
	 *
	 * @param string|null $psToolbarId
	 * @param bool $pbValid
	 * @return \OnionGrid\Toolbar|array|null
	 */
	public function getToolbar (?string $psToolbarId = null, bool $pbValid = true)
	{
		return parent::getElement('oToolbar', $psToolbarId, $pbValid);
	}
	
	
	/**
	 * 
	 * @return \OnionGrid\Column|array|null
	 */
	public function getGridCheckColum ()
	{
		return $this->getColumn('OnionGridCheck');
	}
	

	/**
	 * @param string $psType
	 * @return \OnionGrid\Grid
	 */
	public function setCheckType (string $psType) : Grid
	{
		$this->getGridCheckColum()->setType($psType);
		
		return $this;
	}
	
	
	/**
	 * @param bool $pbShowGridCheck
	 * @return \OnionGrid\Grid
	 */
	public function setShowGridCheck (bool $pbShowGridCheck) : Grid
	{
		$loColumn = $this->getGridCheckColum();
		
		if ($loColumn instanceof Column)
		{
			$loColumn->setVisible($pbShowGridCheck);
		}
		
		return $this;
	}
	
	
	/**
	 * @return string|null
	 */
	public function getGridCheck ()
	{
		$loColumn = $this->getGridCheckColum();
		
		if ($loColumn instanceof Column)
		{
			return $loColumn->get('sType');
		}
		
		return null;
	}

	/**
	 * 
	 * @return \OnionGrid\Column
	 */
	public function getGridColAct () : Column
	{
		return $this->getColumn('OnionGridColAct');
	}
	
	
	/**
	 * @param bool $pbShowGridColAct
	 * @return \OnionGrid\Grid
	 */
	public function setShowGridColAct (bool $pbShowGridColAct) : Grid
	{
		$loColumn = $this->getGridColAct();
		
		if ($loColumn instanceof Column)
		{
			$loColumn->setVisible($pbShowGridColAct);
		}
		
		return $this;
	}


	/**
	 * 
	 * @param string $psField
	 * @return bool
	 */
	public function isSearchField (string $psField) : bool
	{
		if (!empty($psField))
		{
			$loColumn = $this->getColumn($psField, false);

			if ($loColumn instanceof Column)
			{
				return $loColumn->isSearchable();
			}
		}
		
		return false;
	}
	
	
	/**
	 * 
	 * @param string|array $pmFieldsSearchable
	 * @return array
	 */
	public function getSearchableFields ($pmFieldsSearchable) : array
	{
		$laSearchField = [];

		if (is_array($pmFieldsSearchable))
		{
			foreach ($pmFieldsSearchable as $lsField)
			{
				if ($this->isSearchField($lsField))
				{
					$lsSearchField = $this->getSearchField($lsField);

					$laSearchList = explode(',', $lsSearchField);

					if (is_array($laSearchList))
					{
						$laSearchField = array_merge($laSearchField, $laSearchList);
					}
				}
			}
		}
		else
		{
			if ($this->isSearchField($pmFieldsSearchable))
			{
				$laSearchField[] = $this->getSearchField($pmFieldsSearchable);
			}
		}

		return $laSearchField;
	}
	
	
	/**
	 *
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function getClearProperties (bool $pbClearProperty = false) : array
	{
		$laProperties = parent::getClearProperties($pbClearProperty);
		
		if ($pbClearProperty)
		{
			unset($laProperties['cFooter']);
			unset($laProperties['cHeader']);
			unset($laProperties['nMaxNumRows']);
			unset($laProperties['cPdf']);
		}
		
		return $laProperties;
	}
	
	
	/**
	 *
	 */
	public function prepare () : void
	{
		$this->setPagination();
	}
	
	
	/**
	 * 
	 * @param bool $pbToArray
	 * @return \OnionGrid\Grid|array
	 */
	public function render (bool $pbToArray = false)
	{
		$this->setGridResponse('OBJ');
		
		$this->prepare();
		
		$this->renderGridHeader();
		
		$this->oToolbar->render();
		
		$this->renderRows();
		
		$this->renderTableFooter();
		
		if ($pbToArray)
		{
			return $this->toArray(true, true);
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderJson () : string
	{
		$laGrid = $this->render(true);
		
		if (is_array($laGrid))
		{
			return json_encode($laGrid);
		}
		
		return '';
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderCsv () : string
	{
		$this->setGridResponse('CSV');
		
		$lsCSV = $this->renderGridHeader();
		$lsCSV .= $this->renderTableHeader();
		$lsCSV .= $this->renderRows();
		$lsCSV .= $this->renderTableFooter();
			
		return $lsCSV;
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderXls () : string
	{
		$this->setGridResponse('XLS');
		
		$lsGridHeader = $this->renderGridHeader();
		$lsTableHeader = $this->renderTableHeader();
		$lsRows = $this->renderRows();
		$lsTableFooter = $this->renderTableFooter();
		$lnCols = $this->countColums();
		
		$lsXLS = '<table>';
		$lsXLS .= '<tr><td colspan="' . $lnCols . '" height="30" style="font-size:20;"><strong>' . $this->sTitle . '</strong></td></tr>';
		$lsXLS .= $lsGridHeader;
		$lsXLS .= '<thead>' . $lsTableHeader . '</thead>';
		$lsXLS .= '<tbody>' . $lsRows . '</tbody>';
		$lsXLS .= '<tfoot>' . $lsTableFooter . '</tfoot>';
		$lsXLS .= '</table>';
		
		return mb_convert_encoding($lsXLS, 'iso8859-1', 'UTF-8');
	}
	
	
	/**
	 *
	 * @return object|null
	 */
	public function renderPdf () : ?object
	{
		$this->setGridResponse('PDF');

		if (is_callable($this->cPdf))
		{
			$lcPdf = $this->cPdf;
			return $lcPdf($this);
		}
		
		return null;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderHtml () : string
	{
		$this->setGridResponse('HTML');
		
		$this->prepare();

		$lsGridCss = '';
		$lsGridJs = '';
		$lsBasePath = '/vendor/m3uzz/onion-grid/public';
		
		if ($this->sWindowType == 'modal' || $this->sFormRequest == 'AJAX')
		{
			$lsGridCss .= '<link href="' . $lsBasePath . '/onion-grid.css?v=' . CSSRELOAD . '" media="all" rel="stylesheet" type="text/css">'; //remover
			$lsGridJs .= '<script type="text/javascript" src="' . $lsBasePath . '/onion-grid.js?v=' . JSRELOAD . '"></script>';
			$lsGridJs .= '<script type="text/javascript" src="/vendor/m3uzz/onion-zf/public/common.js?v=' . JSRELOAD . '"></script>';
		}

		$lsTable = $this->parseTemplate(
			$this->getTemplate('table', 'table'),
			[
				'table-id' => "onion-grid-table-{$this->sId}",
				'header' => $this->renderTableHeader(),
				'rows' => $this->renderRows(),
				'footer' => $this->renderTableFooter()
			]
		);

		$lsGrid = $this->parseTemplate(
			$this->getTemplate('page', 'content'),
			[
				'id' => "{$this->sId}",
				'container-id' => "onion-grid-{$this->sId}",
				'form-id' => "onion-grid-form-{$this->sId}",
				'action' => $this->sFormAction,
				'method' => $this->sFormMethod,
				'request' => $this->sFormRequest,
				'title' => $this->renderGridTitle(),
				'msgs' => $this->renderMessages(),
				'header' => $this->renderGridHeader(),
				'form' => $this->renderInputForm(),
				'toolbar' => $this->oToolbar->render(),
				'table' => $lsTable,
				'pagination' => $this->renderNavPagination(),
				'modal' => $this->renderModal(),
				'confirm' => $this->renderConfirmation(),
				'alert' => $this->renderAlert(),
				'css' => $lsGridCss,
				'js' => $lsGridJs
			]
		);
		
		return $lsGrid;
	}	
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderGridTitle () : string
	{
		if ($this->isShowHeader())
		{
			$lsDescrtiption = '';
			$lsFolderTitle = '';
			
			if ($this->sFolderTitle !== null)
			{
				$lsIcon = $this->renderIcon($this->sFolderIcon);

				$lsFolderTitle = $this->parseTemplate(
					$this->getTemplate('page', 'header-folder'),
					[
						'icon' => $lsIcon,
						'title' => $this->sFolderTitle
					]
				);
			}
			
			$lsIcon = $this->renderIcon($this->sIcon, false, 'md-48');
	
			if (!empty($this->sDescription))
			{
				$lsDescrtiption = $this->parseTemplate(
					$this->getTemplate('page', 'header-description'),
					[
						'desc' => $this->sDescription
					]
				);
			}
			
			if (!empty($this->sTitle) || !empty($lsIcon) || !empty($lsFolderTitle) || !empty($lsDescrtiption))
			{
				return $this->parseTemplate(
					$this->getTemplate('page', 'header'),
					[
						'icon' => $lsIcon,
						'title' => $this->sTitle,
						'folder' => $lsFolderTitle,
						'desc' => $lsDescrtiption
					]
				);
			}
		}
		
		return '';	
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderInputForm () : string
	{		
		$laData = $this->oPagination->get('aData');
		
		$lnCurrent = ($laData['total'] == 0 ? '0' : $laData['current']);

		$lsColOrder = '{}';

		if (is_array($this->aColOrder) && count($this->aColOrder) > 0)
		{
			$lsColOrder = json_encode($this->aColOrder);
		}

		$this->aInputParam['back']['value'] = $this->sAction;
		$this->aInputParam['urlBack']['value'] = '';
		$this->aInputParam['act']['value'] = $this->sAction;
		$this->aInputParam['ord']['value'] = $lsColOrder;
		$this->aInputParam['rows']['value'] = $this->nNumRows;
		$this->aInputParam['p']['value'] = $lnCurrent;
		$this->aInputParam['w']['value'] = $this->sWindowType;

		$lsForm = '';

		foreach ($this->aInputParam as $lsName => $laValue)
		{
			$lsForm .= '<input type="hidden" id="' . $lsName . '" name="' . $lsName . '" value=\'' . $laValue['value'] . '\' data-reload="' . $laValue['reload'] . '" />';
		}

		return $lsForm;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderMessages () : string
	{
		$lsMessage = '';
		$laMessageType = [];
		
		if (is_array($this->aMessages))
		{
			foreach ($this->aMessages as $lnKey => $laMessage)
			{
    		    $laMessageType[$laMessage['type']][] = '<li>' . $laMessage['msg'] . '</li>';
			}
			
            foreach ($laMessageType as $lsType => $laMsgs)
            {
				$lsMessage .= $this->parseTemplate(
					$this->getTemplate('page', 'message'),
					[
						'msg' => implode("", $laMsgs),
						'type' => $lsType
					]
				);
            }
		}
		
		return $lsMessage;
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function renderTableHeader () : string
	{
		$laColums = $this->getColumn();
		$lsTitles = '';
	
		if (is_array($laColums))
		{
			foreach($laColums as $loColumn)
			{
				$lsTitles .= $loColumn->renderHeader();
			}
		}
	
		switch ($this->sGridResponse)
		{
			case 'CSV':
				return trim($lsTitles, ',') . "\n";
				break;
			case 'XLS':
				return '<tr>' . $lsTitles . '</tr>';
				break;
			case 'PDF':
			case 'OBJ':
				return '';
				break;
			default:
				return $this->parseTemplate(
					$this->getTemplate('table', 'thead'),
					['colums' => $lsTitles]
				);
		}
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderRows () : string
	{
		$lsRows = '';
		
		if (is_array($this->aData) && count($this->aData) > 0)
		{
			$laData = $this->aData;
			$this->aData = [];
			$lsBgColor = '#ffffff';
			
			foreach ($laData as $lnRowNumber => $laRow)
			{
				$lsColums = '';
		
				$laColums = $this->getColumn();
		
				if (is_array($laColums))
				{
					foreach($laColums as $loColumn)
					{
						$loCol = $loColumn->copy();
						$loCol->setReferer($loColumn);
						$loCol->setRowNumber($lnRowNumber);
						$this->aData[$lnRowNumber][$loColumn->get('sId')] = $loCol;
						$lsColums .= $loCol->render($laRow, $lsBgColor);
					}
				}
		
				switch ($this->sGridResponse)
				{
					case 'CSV':
						$lsRows .= trim($lsColums, ',') . "\n";
						break;
					case 'XLS':
						$lsRows .= '<tr>' . $lsColums . '</tr>';
						$lsBgColor = ($lsBgColor == '#ffffff' ? '#eeeeee' : '#ffffff');
						break;
					case 'PDF':
					case 'OBJ':
						break;
					default:				
						$lsRows .= $this->parseTemplate(
							$this->getTemplate('table', 'row'),
							[
								'row-id' => "onion-grid-tr-{$this->sId}-{$laRow['id']}",
								'class' => "onion-grid-tr",
								'colums' => $lsColums
							]
						);
				}

				$lsRows .= $this->renderExtraRowData($laRow, $laColums);
			}
		}
		else
		{
			$lsRows = $this->renderNotFound();
		}
		
		return $lsRows;
	}


	/**
	 * 
	 * @param array $paRow
	 * @param array $paColums
	 * @return string
	 */
	public function renderExtraRowData (array $paRow, array $paColums) : string
	{
		$lnCols = $this->countColums();
		$lsContent = '';

		if (is_callable($this->cExtraRowData))
		{
			$lcExtraRowData = $this->cExtraRowData;
			$lsContent = $lcExtraRowData($this->sId, $paRow, $paColums);
				
			switch ($this->sGridResponse)
			{
				case 'CSV':
				case 'XLS':
				case 'PDF':
				case 'OBJ':
					break;
				default:
					$lsContent = $this->parseTemplate(
						$this->getTemplate('table', 'extra-row-data'),
						[
							'row-extra-id' => "onion-grid-extra-row-data-{$this->sId}-{$paRow['id']}",
							'row-id' => "onion-grid-tr-{$this->sId}-{$paRow['id']}",
							'cols' => $lnCols,
							'content' => $lsContent
						]
					);
			}
		}	

		return $lsContent;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderNotFound () : string
	{
		$lnCols = $this->countColums();
		
		switch ($this->sGridResponse)
		{
			case 'CSV':
				return "\n";
				break;
			case 'XLS':
				return '<tr><td colspan="' . $lnCols . '">' . GridText::text('sNotFound') . '</td></tr>';
				break;
			case 'PDF':
			case 'OBJ':
				return '';
				break;
			default:
				return $this->parseTemplate(
					$this->getTemplate('table', 'not-found'),
					[
						'msg' => GridText::text('sNotFound'),
						'cols' => $lnCols
					]
				);
		}
	}
	
	
	/**
	 *
	 * @return int
	 */
	public function countColums () : int
	{
		$lnCount = 0;
		$paColums = $this->getColumn();
	
		if (is_array($paColums))
		{
			foreach ($paColums as $loColumn)
			{
				if ($loColumn->isEnable())
				{
					switch ($this->sGridResponse)
					{
						case 'CSV':
						case 'XLS':
							if ($loColumn->get('sType') == 'data' || $loColumn->get('sType') == 'img')
							{
								$lnCount ++;
							}
							break;
						case 'PDF':
							if ($loColumn->isVisible() && ($loColumn->get('sType') == 'data' || $loColumn->get('sType') == 'img'))
							{
								$lnCount ++;
							}
							
							break;
						default:
							if ($loColumn->isVisible())
							{
								$lnCount ++;
							}
					}
				}
			}
		}
	
		return $lnCount;
	}

	
	/**
	 *
	 * @return string|null
	 */
	public function renderGridHeader () : ?string
	{
		if (is_callable($this->cHeader))
		{
			$lcHeader = $this->cHeader;
			$this->sHeaderContent = $lcHeader($this);
		}
		
		return $this->sHeaderContent;
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function renderTableFooter () : ?string
	{
		if (is_callable($this->cFooter))
		{
			$lcFooter = $this->cFooter;
			$this->sFooterContent = $lcFooter($this);
		}
		
		return $this->sFooterContent;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderNavPagination () : string
	{
		if ($this->isShowPagination() && ($this->isShowPaginationInfo() || $this->isShowPaginationNav()))
		{
			$lsPagination = '';
			
			if ($this->isShowPaginationInfo())
			{
				$laData = $this->oPagination->get('aData');
				$lnCurrent = ($laData['total'] == 0 ? '0' : $laData['current'] + 1);
				
				$lsInfo1 = sprintf(GridText::text('sPagination'), $lnCurrent, $laData['until'], $laData['total']);
				$lsInfo2 = sprintf(GridText::text('sPaginationShort'), $lnCurrent, $laData['until'], $laData['total']);
			}
			
			if ($this->isShowPaginationNav())
			{
				$lsPagination = $this->oPagination->renderPagination($this->sGetParam);
			}

			return $this->parseTemplate(
				$this->getTemplate('page', 'pagination'),
				[
					'id' => $this->sId,
					'info1' => $lsInfo1, 
					'info2' => $lsInfo2, 
					'pagination' => $lsPagination
				]
			);
		}
		
		return '';
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function renderAlert () : string
	{
		return $this->parseTemplate(
			$this->getTemplate('page', 'alert'),
			[
				'id' => "onion-grid-alert-{$this->sId}", 
				'modal-class' => "onion-grid-modal",
				'title' => GridText::text('sAlert'), 
				'close-btn' => GridText::text('sClose'), 
				'msg' => GridText::text('sMarkLessOne')
			]
		);		
	}

	
	/**
	 * 
	 * @return string
	 */
	public function renderConfirmation () : string
	{
		return $this->parseTemplate(
			$this->getTemplate('page', 'confirmation'),
			[
				'id' => "onion-grid-modal-confirmation-{$this->sId}", 
				'modal-class' => "onion-grid-modal",
				'title' => GridText::text('sConfirmation'), 
				'confirm-id' => "onion-grid-modal-confirm-btn-{$this->sId}",
				'confirm-btn' => GridText::text('sConfirm'), 
				'btn-confirm-class' =>  "onion-grid-modal-confirm-btn",
				'cancel-btn' => GridText::text('sCancel')
			]
		);
	}

	
	/**
	 * 
	 * @return string
	 */
	public function renderModal () : string
	{
		return $this->parseTemplate(
			$this->getTemplate('page', 'modal'),
			[
				'id' => "onion-grid-modal-{$this->sId}", 
				'modal-class' => "onion-grid-modal",
				'title' => $this->sTitle, 
				'ok-id' => "onion-grid-modal-ok-btn-{$this->sId}",
				'ok-btn' => GridText::text('sConfirm'), 
				'btn-ok-class' => "onion-grid-modal-ok-btn",
				'cancel-id' => "onion-grid-modal-cancel-btn-{$this->sId}",
				'cancel-btn' => GridText::text('sCancel'),
				'btn-cancel-class' => "onion-grid-modal-cancel-btn"
			]
		);
	}


	/**
	 *
	 * @return array
	 */
	public function getVisibleColums () : array
	{
		$laVisibleColums = [];
		$paColums = $this->getColumn();
	
		if (is_array($paColums))
		{
			foreach ($paColums as $loColumn)
			{
				if ($loColumn->isEnable() && $loColumn->isVisible())
				{
					$laVisibleColums[$loColumn->get('sName')] = true;
				}
			}
		}
	
		return $laVisibleColums;
	}

	
	/**
	 *
	 * @param array|null $paParams
	 * @return \OnionGrid\Grid
	 */
	public function init (?array $paParams) : Grid
	{ 
		return $this; 
	}


	public function set ($pmVar, $pmValue = null) : Grid
	{
		parent::set($pmVar, $pmValue);
		return $this;
	}


	public function setEnable (bool $pbEnable) : Grid
	{
		parent::setEnable($pbEnable);
		return $this;
	}


	public function setParent (?object $poParent = null) : Grid
	{
		$this->oParent = $poParent;
		return $this;
	}
	
	
	public function setResource (string $psResource) : Grid
	{
		parent::setResource($psResource);
		return $this;
	}

	
	public function setId (string $psId) : Grid
	{
		parent::setId($psId);
		return $this;
	}


	public function setName (?string $psName = null) : Grid
	{
		parent::setName($psName);
		return $this;
	}


	public function setTitle (?string $psTitle = null) : Grid
	{
		parent::setTitle($psTitle);	
		return $this;
	}


	public function setDescription (?string $psDescription = null) : Grid
	{
		parent::setDescription($psDescription);
		return $this;
	}


	public function setHelp (?string $psHelp = null) : Grid
	{
		$this->sHelp = $psHelp;
		
		return $this;
	}


	public function setIcon (?string $psIcon = null) : Grid
	{
		parent::setIcon($psIcon);
		return $this;
	}


	public function setPrepared (bool $pbPrepared) : Grid
	{
		parent::setPrepared($pbPrepared);
		return $this;
	}


	public function order (string $psItem, array $paOrder) : Grid
	{
		parent::order($psItem, $paOrder);
		return $this;
	}


	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : Grid
	{
		parent::add($psArrayObj, $poElement, $psIndex, $pnPosition, $pbNumericIndex);
		return $this;
	}


	public function remove (string $psArrayObj, string $psElementId) : Grid
	{
		parent::remove($psArrayObj, $psElementId);
		return $this;
	}
}