<?php
/**
 * This file is part of Onion Grid
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion Grid
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-grid
 */
declare (strict_types = 1);

namespace OnionGrid;
use OnionGrid\InterfaceGrid;
use OnionIcon\IconFont;


abstract class AbstractGrid extends IconFont implements InterfaceGrid
{

	/**
	 * Define if the object is enable
	 * @default true
	 *
	 * @var bool
	 */
	protected $bEnable = true;

	/**
	 * Define if the object instance is a clone of the original object
	 * In this case every change will not affect the original object
	 * @default false
	 *
	 * @var bool
	 */
	protected $bClone = false;

	/**
	 * The parent object
	 *  
	 * @var object
	 */
	protected $oParent = null;
	
	/**
	 * Define the client folder
	 *
	 * @example onion.com
	 * @var string
	 */
	protected $sResource = '';

	/**
	 * The object id
	 *
	 * @var string
	 */
	protected $sId = '';

	/**
	 * The project client name
	 *
	 * @var string
	 */
	protected $sName = '';

	/**
	 * The project title
	 *
	 * @var string
	 */
	protected $sTitle = '';

	/**
	 * The project description
	 *
	 * @var string
	 */
	protected $sDescription = null;
	/**
	 * An url to the application help
	 *
	 * @var string
	 */
	protected $sHelp = '';

	/**
	 * The application default icon
	 * @default icon-th
	 *
	 * @var string
	 */
	protected $sIcon = '';
	
	/**
	 * @default false
	 *
	 * @var bool
	 */
	protected $bPrepared = false;
			
	
	// methods
	
	
	/**
	 *
	 * @param string $psVarName
	 * @param mixed $pmVarValue
	 * @throws \Exception
	 */
	public function __set (string $psVarName, $pmVarValue) : void
	{
		throw new \Exception("Unable to set vars dynamically");
	}

	
	/**
	 * Construct an object setting the id, name and resource properties
	 * if the id is not given the construct will return an exception
	 *
	 * @param string $psId Instance identifier.
	 * @param string $psResource
	 * @param \OnionGrid\InterfaceGrid|null $poParent        	
	 * @throws \InvalidArgumentException
	 */
	protected function __construct (string $psId, string $psResource, ?InterfaceGrid $poParent = null)
	{
		if (! empty($psId))
		{
			$this->setId($psId);
			
			if (empty($this->sName))
			{
				$this->setName(ucfirst($psId));
			}
			
			if (empty($this->sResource) && $psResource != null)
			{
				$this->setResource($psResource);
			}
			
			if ($poParent !== null)
			{
				$this->setParent($poParent);
			}
		}
		else
		{
			throw new \InvalidArgumentException('The object ID need to be informed!');
		}
	}

	
	/**
	 * Check if a property exist into the object
	 *
	 * @param string $psVar        	
	 * @return bool
	 */
	public function isVar (string $psVar) : bool
	{
		if (property_exists($this, $psVar))
		{
			return true;
		}
		
		return false;
	}

	
	/**
	 * Set the object properties.
	 *
	 * The method "set" can be used as:
	 * An array with the exists properties and its values;
	 * ```
	 * set(array('property_name'=>'value'));
	 * ```
	 * or 
	 * ```
	 * set('property_name', 'value');
	 * ```
	 * setPropertyName('value') - An specific method, where the exist
	 * property is the name postfix, using camelCase style.
	 *
	 * If the property doesn't exist, the return will be a exception
	 *
	 * @param string|array $pmVar
	 * @param mixed $pmValue
	 * @throws \InvalidArgumentException	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function set ($pmVar, $pmValue = null) : InterfaceGrid
	{
		if (is_array($pmVar))
		{
			foreach ($pmVar as $lsVar => $lmValue)
			{
				$lsMethod = "set" . $lsVar;
				$lsMethod2 = "set" . substr($lsVar, 1);

				if (method_exists($this, $lsMethod))
				{
					$this->$lsMethod($lmValue);
				}
				elseif (method_exists($this, $lsMethod2))
				{
					$this->$lsMethod2($lmValue);
				}
				else
				{
					throw new \InvalidArgumentException('The property "' . $lsVar . '" do not exist into the object 1!');
				}
			}
		}
		else
		{
			$lsMethod = "set" . $pmVar;
			$lsMethod2 = "set" . substr($pmVar, 1);
			
			if (method_exists($this, $lsMethod))
			{
				$this->$lsMethod($pmValue);
			}
			elseif (method_exists($this, $lsMethod2))
			{
				$this->$lsMethod2($pmValue);
			}
			else
			{
				throw new \InvalidArgumentException('The property "' . $pmVar . '" do not exist into the object! 2');
			}
		}
		
		return $this;
	}

	
	/**
	 * Define if the application is enable to run
	 *
	 * @param bool $pbEnable        	
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setEnable (bool $pbEnable) : InterfaceGrid
	{
		if (is_bool($pbEnable))
		{
			$this->bEnable = $pbEnable;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "enable" property need to be a bool!');
		}
		
		return $this;
	}

	
	/**
	 * 
	 * @param object $poParent
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setParent (object $poParent = null) : InterfaceGrid
	{
		$this->oParent = $poParent;

		return $this;
	}
	
	
	/**
	 *
	 * @param string $psResource        
	 * @throws \InvalidArgumentException	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setResource (string $psResource) : InterfaceGrid
	{
		if (! empty($psResource))
		{
			$this->sResource = $psResource;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "resource" property should be a non empty string!');
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string $psId        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setId (string $psId) : InterfaceGrid
	{
		if (! empty($psId))
		{
			$this->sId = $psId;
			
			if (empty($this->sName) && !is_null($this->sName))
			{
				$this->setName($psId);
			}
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string|null $psName        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setName (?string $psName = null) : InterfaceGrid
	{
		if (! empty($psName))
		{
			$this->sName = $psName;
			
			if (empty($this->sId) && !is_null($this->sId))
			{
				$this->setId($psName);
			}
			
			if (empty($this->sTitle) && !is_null($this->sTitle))
			{
				$this->setTitle($psName);
			}
		}
		elseif ($psName === null)
		{
			$this->sName = null;
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string|null $psTitle        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setTitle (?string $psTitle = null) : InterfaceGrid
	{
		if (! empty($psTitle))
		{
			$this->sTitle = $psTitle;
			
			if (empty($this->sDescription) && !is_null($this->sDescription))
			{
				$this->setDescription($psTitle);
			}
		}
		elseif ($psTitle === null)
		{
			$this->sTitle = null;
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string|null $psDescription        	
	 * return \OnionGrid\AbstractGrid
	 */
	public function setDescription (?string $psDescription = null) : InterfaceGrid
	{
		if (! empty($psDescription))
		{
			$this->sDescription = $psDescription;
		}
		elseif ($psDescription === null)
		{
			$this->sDescription = null;
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string|null $psHelp        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setHelp (?string $psHelp = null) : InterfaceGrid
	{
		$this->sHelp = $psHelp;
		
		return $this;
	}

	
	/**
	 *
	 * @param string|null $psIcon        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setIcon (?string $psIcon = null) : InterfaceGrid
	{
		if (! empty($psIcon))
		{
			$this->sIcon = $psIcon;
		}
		elseif ($psIcon === null)
		{
			$this->sIcon = null;
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @param bool $pbPrepared
	 * @throws \InvalidArgumentException
	 * @return \OnionGrid\AbstractGrid
	 */
	public function setPrepared (bool $pbPrepared) : AbstractGrid
	{
		if (is_bool($pbPrepared))
		{
			$this->bPrepared = $pbPrepared;
		}
		else
		{
			throw new \InvalidArgumentException('The value of "prepared" property need to be a bool!');
		}
		
		return $this;
	}
	

	/**
	 * 
	 * @return string
	 */
	public function getId () : string
	{
		return $this->sId;
	}
	
	
	/**
	 *
	 * @param string $psVar        	
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public function getProperties (string $psVar)
	{
		if (property_exists($this, $psVar))
		{
			return $this->$psVar;
		}
		else
		{
			throw new \InvalidArgumentException('The property "' . $psVar . '" do not exist into the object!');
		}
	}

	
	/**
	 * 
	 * @return string|null
	 */
	public function getResource () : ?string
	{
		return $this->sResource;
	}	
	
	
	/**
	 * 
	 * @return object|null
	 */
	public function getParent () : ?object
	{
		return $this->oParent;
	}

	
	/**
	 * 
	 * @return bool
	 */
	public function isEnable () : bool
	{
		return $this->bEnable;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function isClone () : bool
	{
		return $this->bClone;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function isPrepared () : bool
	{
		return $this->bPrepared;
	}
	
	
	/**
	 * 
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function getClearProperties (bool $pbClearProperty = false) : array
	{
		$laProperties = get_object_vars($this);
		
		if ($pbClearProperty)
		{
			unset($laProperties['bEnable']);
			unset($laProperties['bClone']);
			unset($laProperties['bPrepared']);
			unset($laProperties['sResource']);
			unset($laProperties['oParent']);
		}
		
		return $laProperties;
	}
	
	
	/**
	 * Return the whole object and its children as an array
	 * 
	 * @param bool $pbJustEnable
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function toArray (bool $pbJustEnable = false, bool $pbClearProperty = false) : array
	{
		$laProperties = null;
		
		if (!$pbJustEnable || $this->bEnable)
		{
			$laProperties = $this->getClearProperties($pbClearProperty);
		}
		
		if (is_array($laProperties))
		{
			return $this->parseArray($laProperties, $pbJustEnable, $pbClearProperty);
		}
		
		return [];
	}
	
	
	/**
	 * 
	 * @param array $paData
	 * @param bool $pbJustEnable
	 * @param bool $pbClearProperty
	 * @return array
	 */
	public function parseArray (array $paData, bool $pbJustEnable = false, bool $pbClearProperty = false) : array
	{
		$laReturn = [];
		
		foreach ($paData as $lsKey => $lmValue)
		{
			if ($lsKey == 'oParent' && is_object($lmValue) && !$pbClearProperty)
			{
				$laReturn[$lsKey] = get_class($lmValue);
			}
			elseif ($lmValue instanceof InterfaceGrid)
			{
				if (!$pbJustEnable || $this->isEnable())
				{
					$laReturn[$lsKey] = $lmValue->toArray($pbJustEnable, $pbClearProperty);
				}
			}
			elseif (is_object($lmValue))
			{
				$laReturn[$lsKey] = get_object_vars($lmValue);
			}
			elseif (is_array($lmValue))
			{
				$laReturn[$lsKey] = $this->parseArray($lmValue, $pbJustEnable, $pbClearProperty);
			}
			else
			{
				$laReturn[$lsKey] = $lmValue;
			}
		}
		
		return $laReturn;
	}

	
	/**
	 * Return the value of the object and its children
	 *
	 * The method "get" could be used as:
	 * without pass any specific var, it will return the whole object
	 * ```
	 * get();
	 * ```
	 * or passing a property name, return just the object property required
	 * ```
	 * get('property_name');
	 * ```
	 * or passing an array whith the path to the deeped property.
	 * ```
	 * get(array('module'=>'value1','controller'=>'value2','button'=>'value3'));
	 * ```
	 * In this case it will return the button element identified by id = value3
	 *
	 * @param string|null $pmVar        	
	 * @return mixed
	 */
	public function get ($pmVar = null)
	{
		if (is_string($pmVar) && $pmVar !== null)
		{
			return $this->getProperties($pmVar);
		}
		elseif ($pmVar === null)
		{
			return $this->toArray();
		}
	}

	
	/**
	 * Return a clone of the object.
	 * The change maked in this object clone will not affect the original one.
	 * 
	 * @return \OnionGrid\AbstractGrid
	 */
	public function copy () : InterfaceGrid
	{
		$loClone = clone $this;
		$loClone->bClone = true;
		
		if (is_object($loClone))
		{
			foreach ($loClone as $lsVar => $lmValue)
			{
				if (is_array($lmValue) && count($lmValue) != 0)
				{
					foreach ($lmValue as $lsId => $lmObj)
					{
						if ($lmObj instanceof InterfaceGrid)
						{
							$loClone->{$lsVar}[$lsId] = $lmObj->copy();
						}
					}
				}
				elseif ($lsVar != 'oParent')
				{
					if ($lmValue instanceof InterfaceGrid)
					{
						$loClone->{$lsVar} = $lmValue->copy();
					}
				}
			}
		}
		
		return $loClone;
	}

	
	/**
	 *
	 * @param string $psItem        	
	 * @param array $paOrder        	
	 * @param \OnionGrid\AbstractGrid
	 */
	public function order (string $psItem, array $paOrder) : InterfaceGrid
	{
		$laProperty = $this->get($psItem);
		
		if (is_array($laProperty) && is_array($paOrder))
		{
			$laNew = [];
			
			foreach ($paOrder as $lsItem)
			{
				foreach ($laProperty as $lsKey => $lmItem)
				{
					if ($lmItem instanceof InterfaceGrid)
					{
						if ($lmItem->get('sId') == $lsItem)
						{
							$laNew[$lsKey] = $lmItem;
							continue;
						}
					}
					else
					{
						if ($lmItem == $lsItem)
						{
							$laNew[$lsKey] = $lmItem;
							continue;
						}
					}
				}
			}
			
			$psItem = "_" . $psItem;
			$this->$psItem = $laNew;
		}
		
		return $this;
	}

	
	/**
	 * Insere um item de array na posição desejada, deslocando os demais itens.
	 *
	 * @param array $paList	Array original onde será inserido o novo elemento.
	 * @param \OnionGrid\InterfaceGrid $poValue	Elemento a ser inserido no array original.
	 * @param string|null $psIndex Identificador do indice do elemento, podendo ser null quando for igual ao id do elemento.
	 * @param int|null $pnPosition Posição em que o elemento $poValue será inserido na nova lista.
	 * @param bool $pbNumericIndex Informa quando o indice do array for numérico.
	 * @return array Nova lista com o elemento inserido na posição desejada e com os demais elementos deslocados.
	 */
	public function position (array $paList, InterfaceGrid $poValue, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : array
	{
		// inicializa o contador de posição do array
		$lnPos = 0;
		
		// percorrendo lista original
		foreach ($paList as $lsKey => $loValue)
		{
			// se a posição atual é a posição desejada
			if ($lnPos == $pnPosition)
			{
				// verifica se o indice não é numérico
				if ($pbNumericIndex === false)
				{
					// verifica se não foi passado um indice específico
					if ($psIndex === null)
					{
						// carrega o id do elemento para servir de indice
						$lsIndex = $poValue->get('sId');
					}
					else
					{
						// utiliza o indice passado no parametro
						$lsIndex = $psIndex;
					}
				}
				else
				{
					// utiliza a posição numérica ordenada
					$lsIndex = $lnPos;
				}
				
				// insere o elemento na posição desejada
				$laNewList[$lsIndex] = $poValue;
				// incrementa a posição a ser verificada
				$lnPos ++;
			}
			
			// independente se o elemento achou sua posição ou não todos os
			// elementos devem ser transportados para o novo array. Sendo assim,
			// o array original deve ser percorrido até o fim.
			
			// verifica se o indice não é numérico
			if ($pbNumericIndex === false)
			{
				// utiliza o index já atual do elemanto
				$lsIndex = $lsKey;
			}
			else
			{
				// utiliza a posição numérica ordenada
				$lsIndex = $lnPos;
			}
			
			// mantem o elemento existente na sua posição ou desloca para baixo
			$laNewList[$lsKey] = $loValue;
			// incrementa a posição a ser verificada
			$lnPos ++;
		}
		
		// retornando a nova lista
		return $laNewList;
	}

	
	/**
	 * Create a new object into the array object
	 * and setting its id and name
	 *
	 * @param string $psArrayObj        	
	 * @param string $psElementId        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function create (string $psArrayObj, string $psElementId) : InterfaceGrid
	{
		return $this->{$psArrayObj}[$psElementId];
	}

	
	/**
	 * Add an existent object to the array object.
	 * If $pnPosition is int value, the object will be inserted in this array
	 * positon.
	 * Else, if $psIndex is given, it will be used to set the array key.
	 * Or by default the array key will be the object id property.
	 *
	 * @param string $psArrayObj        	
	 * @param \OnionGrid\InterfaceGrid $poElement        	
	 * @param string|null $psIndex        	
	 * @param int|null $pnPosition        	
	 * @param bool $pbNumericIndex        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function add (string $psArrayObj, InterfaceGrid $poElement, ?string $psIndex = null, ?int $pnPosition = null, bool $pbNumericIndex = false) : InterfaceGrid
	{
		if (is_int($pnPosition))
		{
			$this->$psArrayObj = $this->position($this->$psArrayObj, $poElement, $psIndex, $pnPosition, $pbNumericIndex);
		}
		else
		{
			if ($psIndex === null)
			{
				$lsIndex = $poElement->get('sId');
			}
			else
			{
				$lsIndex = $psIndex;
			}

			$this->{$psArrayObj}[$lsIndex] = $poElement;
		}
		
		return $this;
	}

	
	/**
	 * Remove the element from the array object
	 *
	 * @param string $psArrayObj        	
	 * @param string $psElementId        	
	 * @return \OnionGrid\AbstractGrid
	 */
	public function remove (string $psArrayObj, string $psElementId) : InterfaceGrid
	{
		if ($psElementId !== null && in_array($psElementId, $this->$psArrayObj))
		{
			unset($this->{$psArrayObj}[$psElementId]);
		}
		else
		{
			if (is_array($this->$psArrayObj))
			{
				foreach ($this->$psArrayObj as $lnKey => $loButton)
				{
					if ($loButton->get('sId') == $psElementId)
					{
						unset($this->{$psArrayObj}[$lnKey]);
					}
				}
			}
		}
		
		return $this;
	}

	
	/**
	 * Load the element from array object
	 * or the entire array if $psElementId = null
	 *
	 * @param string $psArrayObj        	
	 * @param string|null $psElementId        	
	 * @param bool $pbValid        	
	 * @throws \Exception
	 * @return \OnionGrid\AbstractGrid|array|null
	 */
	public function getElement (string $psArrayObj, ?string $psElementId = null, bool $pbValid = true)
	{
		if ($psElementId !== null && in_array($psElementId, $this->$psArrayObj))
		{
			return $this->{$psArrayObj}[$psElementId];
		}
		elseif ($psElementId !== null && is_string($psElementId))
		{
			if (is_array($this->$psArrayObj))
			{
				foreach ($this->$psArrayObj as $lnKey => $loObj)
				{
					if (is_object($loObj) && $loObj->sId == $psElementId)
					{
						return $loObj;
					}
				}
			}
			
			if ($pbValid)
			{
				throw new \Exception('The element "' . $psElementId . '" do not exist!');
			}
		}
		elseif ($pbValid && $psElementId !== null && is_string($psElementId))
		{
			throw new \Exception('The element "' . $psElementId . '" do not exist!');
		}
		else
		{
			return $this->$psArrayObj;
		}
		
		return null;
	}


	/**
	 * 
	 * @return object|null
	 */
	public function getGridObject () : ?object
	{
		$loParent = $this->oParent;

		if (!is_null($loParent) && $loParent::TYPE == 'grid')
		{
			return $loParent;
		}
		elseif (method_exists($loParent, 'getGridObject'))
		{
			return $loParent->getGridObject();
		}

		return null;
	}


	/**
	 * @param string $psSubject
	 * @param array $paPatternValue
	 * @return string
	 */
	public function parseTemplate (String $psSubject, array $paPatternValue) : string
	{
		foreach ($paPatternValue as $lsPattern => $lsValue)
		{
			$laPattern[] = "/#{{$lsPattern}}#/";
			$laValue[] = $lsValue;
		}

		return preg_replace($laPattern, $laValue, $psSubject);
	}
}